package fao.org.sesame.test;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.DirectReasoning;
import it.uniroma2.art.owlart.models.RDFSModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTLiteralIterator;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.vocabulary.OWL;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class LoadingStatistics {

	/**
	 * this method is required since some triple store implementations for OWL ART API sync with their data
	 * folders after a few seconds. For example, data sync in Sesame2 happens every 1000ms (1 second).
	 * 
	 */
	public static void pause() {
		try {
			System.out.println("press a key");
			System.in.read();
			System.out.println("key pressed!");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * @param args
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ModelCreationException
	 * @throws ModelUpdateException
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ModelAccessException
	 * @throws UnsupportedRDFFormatException
	 * @throws BadConfigurationException
	 */
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, ModelCreationException, ModelUpdateException, FileNotFoundException,
			IOException, ModelAccessException, UnsupportedRDFFormatException, BadConfigurationException {

		// argument resolution
		if (args.length < 2) {
			System.out.println("usage:\n" + LoadingStatistics.class + " config <modelClass> [rdf file]");
			return;
		}

		String modelClassPar = args[1];
		@SuppressWarnings("unchecked")
		Class<? extends RDFSModel> modelClass = (Class<RDFSModel>) Class.forName(modelClassPar);

		long time = System.currentTimeMillis();

		RDFSModel model = ModelLoader.loadModel(args[0], modelClass);

		long modelLoadedTime = System.currentTimeMillis();

		System.err.println("loading time: " + (modelLoadedTime - time));

		// OPTIONAL LOADING OF DATA FILE
		if (args.length > 2) {			
			String inputFileName = args[2];
			System.out.println("loading data from: " + inputFileName);
			// this should be used to load the file produced through the agrovoc-owl2skos conversion routine,
			// though it can be used for any RDF file
			File inputFile = new File(inputFileName);
			model.addRDF(inputFile, "http://aims.fao.org/aos/agrovoc/",
					RDFFormat.guessRDFFormatFromFile(inputFile));

			long dataLoadedTime = System.currentTimeMillis();
			System.err.println("time spent loading data from <" + inputFileName + ">: "
					+ (dataLoadedTime - modelLoadedTime));

		}
		
		pause();

	}
	
	
	
	public static void describePropertyPrettyPrint(SKOSXLModel skosXLModel, String prefix,
			String propLocalName) throws ModelAccessException {
		String qname = prefix + ":" + propLocalName;
		System.out.println("\nprefixed name (qname) for " + propLocalName + ": " + qname + "\n");

		String propertyName = skosXLModel.expandQName(qname);
		ARTURIResource property = skosXLModel.createURIResource(propertyName);

		describeProperty(skosXLModel, property);

		System.out.println("\n\n\n\nLIST OF STATEMENTS FOR THIS PROPERTY\n");

		ARTStatementIterator it = skosXLModel
				.listStatements(property, NodeFilters.ANY, NodeFilters.ANY, true);
		while (it.streamOpen()) {
			System.out.println(it.getNext());
		}

	}

	public static void describeProperty(SKOSXLModel skosXLModel, ARTURIResource prop)
			throws ModelAccessException {
		ARTResource res;
		System.out.printf("%-40s", prop.getLocalName());
		res = getPropertyDomain(skosXLModel, prop);
		System.out.printf("domain: ");
		if (res != null) {
			System.out.printf("%-70s", prettyPrintDomainRange(skosXLModel, res) + "\t");
		} else
			System.out.printf("%-70s", "");

		res = getPropertyRange(skosXLModel, prop);
		System.out.print("range: ");
		if (res != null) {
			System.out.printf("%-70s", prettyPrintDomainRange(skosXLModel, res) + "\t");
		} else
			System.out.printf("%-70s", "");
		System.out.println();
	}

	protected static String prettyPrintDomainRange(SKOSXLModel skosXLModel, ARTResource resource)
			throws ModelAccessException {
		if (resource.isBlank()) {
			if (skosXLModel.hasType(resource, OWL.Res.DATARANGE, true)) {
				StringBuffer buff = new StringBuffer("[");
				ARTLiteralIterator it = skosXLModel.getOWLModel().parseDataRange(resource);
				while (it.streamOpen()) {
					buff.append(it.getNext() + "|");
				}
				it.close();
				buff.append("]");
				return buff.toString();
			} else
				return resource.toString();
		} else {
			return skosXLModel.getQName(resource.asURIResource().getURI());
		}
	}

	/**
	 * this may not be precise, since it only goes up the first branching of the superproperties, so it does
	 * not cover multiple inheritance when discovering property domains from its superproperties
	 * 
	 * @param skosXLModel
	 * @param prop
	 * @return
	 * @throws ModelAccessException
	 */
	protected static ARTResource getPropertyDomain(SKOSXLModel skosXLModel, ARTURIResource prop)
			throws ModelAccessException {
		ARTResource result = null;
		ARTResourceIterator innerIT;
		innerIT = skosXLModel.getOWLModel().listPropertyDomains(prop, true, NodeFilters.MAINGRAPH);
		if (innerIT.streamOpen())
			result = innerIT.getNext();
		innerIT.close();
		if (result != null)
			return result;
		else {
			// System.out.println("trying to get property domain from superproperties");
			ARTURIResourceIterator innerURIIT = ((DirectReasoning) skosXLModel.getOWLModel())
					.listDirectSuperProperties(prop, NodeFilters.MAINGRAPH);
			ARTURIResource superProp = null;

			if (innerURIIT.streamOpen())
				superProp = innerURIIT.getNext();
			innerURIIT.close();
			if (superProp != null && (!superProp.equals(prop))) {
				// System.out.println("trying to get property domain for property: " + prop
				// + " from superproperties: " + superProp);
				return getPropertyDomain(skosXLModel, superProp);
			} else
				return null;
		}
	}

	/**
	 * this may not be precise, since it only goes up the first branching of the superproperties, so it does
	 * not cover multiple inheritance when discovering property ranges from its superproperties
	 * 
	 * @param skosXLModel
	 * @param prop
	 * @return
	 * @throws ModelAccessException
	 */
	protected static ARTResource getPropertyRange(SKOSXLModel skosXLModel, ARTURIResource prop)
			throws ModelAccessException {
		ARTResource result = null;
		ARTResourceIterator innerIT;
		innerIT = skosXLModel.getOWLModel().listPropertyRanges(prop, true, NodeFilters.MAINGRAPH);
		if (innerIT.streamOpen())
			result = innerIT.getNext();
		innerIT.close();
		if (result != null)
			return result;
		else {
			// System.out.println("trying to get property domain from superproperties");
			ARTURIResourceIterator innerURIIT = ((DirectReasoning) skosXLModel.getOWLModel())
					.listDirectSuperProperties(prop, NodeFilters.MAINGRAPH);
			ARTURIResource superProp = null;
			if (innerURIIT.streamOpen())
				superProp = innerURIIT.getNext();
			innerURIIT.close();
			if (superProp != null && (!superProp.equals(prop)))
				return getPropertyRange(skosXLModel, superProp);
			else
				return null;
		}
	}

}

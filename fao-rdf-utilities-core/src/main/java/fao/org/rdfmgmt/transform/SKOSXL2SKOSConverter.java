package fao.org.rdfmgmt.transform;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.impl.SKOSXLModelImpl;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;

/**
 * Simple utility class for lowering a SKOS-XL thesuarus to SKOS. <br/>
 * Note from <a href="mailto:stellato@info.uniroma2.it">Armando Stellato</a>: <em>the only issue with this
 * procedure is that it simply skips statements where the xlabel is the subject, but if the xlabel contains in
 * turn complex paths starting from it, then these are maintained in the target model. We should add a
 * deepDelete (with arbitrary paths to be specified for URIs which make sense only if applied to the xLabels),
 * or at least enable it as an option.</em>
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class SKOSXL2SKOSConverter extends RDFTransformer {

	/**
	 * this wrapping SKOSXL model is useful to use APIs of skosxl, without necessarily loading the
	 * OWL/SKOS/SKOSXL vocabulary files
	 */
	SKOSXLModel skosxlModel;

	public SKOSXL2SKOSConverter(RDFModel model) {
		super(model);
		skosxlModel = new SKOSXLModelImpl(model);
	}

	public SKOSXL2SKOSConverter(SKOSXLModel sourceModel, RDFModel targetModel) {
		super(sourceModel, targetModel);
		skosxlModel = new SKOSXLModelImpl(sourceModel);
	}

	public void transform(boolean keepSKOSXL, ARTResource...graphs) throws ModelAccessException, ModelUpdateException {
		ARTStatementIterator it = sourceModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
				NodeFilters.ANY, false, NodeFilters.MAINGRAPH);

		// this case should need a dedicated cleaner at the end, as we need to keep the info SKOSXL until the
		// end, and finally invoke a cleaner for removing the SKOSXL info after all the plain SKOS labels have
		// been written. If target is different from source, we just don't output them
		if (!keepSKOSXL && sourceAndTargetModelAreTheSame) {
			throw new IllegalStateException(
					"the case in which the source and target model are the same, and you want to remove the SKOSXL information has not been implemented");
		}

		while (it.streamOpen()) {
			ARTStatement stmt = it.next();

			ARTResource subj = stmt.getSubject();
			ARTURIResource pred = stmt.getPredicate();
			ARTNode obj = stmt.getObject();

			// if the subject of the statement is an xlabel, simply go to next statement
			// the conversion will be handled later when meeting the xlabel as an object
			if (skosxlModel.isXLabel(subj)) {
				// add it to the target if you want to keep skosxl info
				if (keepSKOSXL)
					addTriple(subj, pred, obj, NodeFilters.MAINGRAPH);
				continue;
			}

			// if the object of the statement is an xlabel, do the conversion, then go
			// to next statement (continue)
			if (obj.isResource() && skosxlModel.isXLabel(obj.asResource())) {

				// add it to the target if you want to keep skosxl info
				if (keepSKOSXL)
					addTriple(subj, pred, obj, NodeFilters.MAINGRAPH);

				ARTURIResource inferredPred = null;
				if (pred.equals(SKOSXL.Res.PREFLABEL)) {
					inferredPred = SKOS.Res.PREFLABEL;
				} else if (pred.equals(SKOSXL.Res.ALTLABEL)) {
					inferredPred = SKOS.Res.ALTLABEL;
				} else if (pred.equals(SKOSXL.Res.HIDDENLABEL)) {
					inferredPred = SKOS.Res.HIDDENLABEL;
				}

				ARTLiteral inferredObj = skosxlModel.getLiteralForm(obj.asResource());

				// System.err.println("@@" + inferredObj);
				if (inferredPred != null) {
					// System.err.println("@@" + stmt.getSubject() + " " + inferredPred + " " + inferredObj);
					addTriple(subj, inferredPred, inferredObj, graphs);
				} else {
					continue;
				}
				continue;
			}

			// if nothing of the above, add the triple in any case
			addTriple(subj, pred, obj, NodeFilters.MAINGRAPH);
		}
		it.close();

	}

}

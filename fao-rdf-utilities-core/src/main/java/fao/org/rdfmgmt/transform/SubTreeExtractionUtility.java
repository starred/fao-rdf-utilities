package fao.org.rdfmgmt.transform;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.utilities.transform.SubTreeExtractor;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SubTreeExtractionUtility {

	protected static Logger logger = LoggerFactory.getLogger(SubTreeExtractionUtility.class);
	
	public static void main(String[] args) throws IOException, ModelCreationException, BadConfigurationException, ModelAccessException, ModelUpdateException, UnsupportedRDFFormatException {
		if (args.length < 4) {
			System.out.println("usage:\njava " + SubTreeExtractionUtility.class.getCanonicalName()
					+ " <configfile> <thesaurusfile> <mappingsfile> <rootConcept>\n"
					+ "\t<configfile>    : config file for the chosen RDFModel Implementation \n"
					+ "\t<sourcefile> : source thesaurus file  \n"
					+ "\t<targetfile>  : target thesaurus file  \n"
					+ "\t<rootConcept>  : rootConcept to use for creating the subtree  \n");
			;
			return;
		}
		
		String configFile = args[0];
		File sourceFile = new File(args[1]);
		File targetFile = new File(args[2]);		
		
		SKOSXLModel sourceModel = ModelLoader.loadModel(configFile, SKOSXLModel.class);
		SKOSXLModel targetModel = ModelLoader.loadModel(configFile, SKOSXLModel.class);
		
		logger.info("loading the input dataset");
		sourceModel.addRDF(sourceFile, null, RDFFormat.guessRDFFormatFromFile(sourceFile));
				
		ARTURIResource rootConcept = targetModel.createURIResource(args[3]);
				
		SubTreeExtractor<SKOSXLModel> subTreer = new SubTreeExtractor<SKOSXLModel>();
		
		logger.info("starting the subtreee extraction");
		subTreer.doExtract(sourceModel, targetModel, rootConcept);
		
		targetModel.writeRDF(targetFile, RDFFormat.guessRDFFormatFromFile(targetFile), NodeFilters.MAINGRAPH);
	}
		
}

package fao.org.rdfmgmt.transform.routine;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.query.GraphQuery;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.Query;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.query.io.TupleBindingsWriterException;
import it.uniroma2.art.owlart.query.io.TupleBindingsWritingFormat;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.vocabulary.OWL;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.RDFS;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.openrdf.sail.nativerdf.datastore.DataFile;

/**
 * This class just loads a file and reoutputs its content
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 *
 */
public class Identity {

	public static void main(String[] args) throws IOException, ModelCreationException,
			BadConfigurationException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException, QueryEvaluationException, TupleBindingsWriterException, UnsupportedQueryLanguageException, MalformedQueryException {

		if (args.length < 3) {
			System.out
					.println("usage:\n"
							+ "java " + Identity.class.toString() + "\n"
							+ "\t<configfile>  : config file for the chosen RDFModel Implementation \n"
							+ "\t<inputFile>  : the file to be tested \n"
							+ "\t<outputfile>  : the output file, which should be identical to the input one \n"							
							);
			return;
		}
		
		String configFile = args[0];
		File inputFile = new File(args[1]);
		File outputFile = new File(args[2]);

		RDFModel sourceModel = ModelLoader.loadModel(configFile, RDFModel.class);
		sourceModel.addRDF(inputFile, null, RDFFormat.guessRDFFormatFromFile(inputFile));
		

		
		ARTResource[] graphFilter = null;
		RDFFormat outputFormat = RDFFormat.guessRDFFormatFromFile(outputFile);
		if ( (outputFormat == RDFFormat.NTRIPLES) || (outputFormat == RDFFormat.RDFXML) || (outputFormat == RDFFormat.RDFXML_ABBREV) )
			graphFilter = new ARTResource[] { NodeFilters.MAINGRAPH };
		if ( (outputFormat == RDFFormat.NQUADS) ) {
			Collection<ARTResource> ngs = RDFIterators.getCollectionFromIterator(sourceModel.listNamedGraphs());
			System.out.println("named graphs: " + ngs);
			ngs.add(NodeFilters.MAINGRAPH);
			ngs.remove(RDFS.Res.URI);
			ngs.remove(OWL.Res.URI);
			ngs.remove(SKOS.Res.URI);
			ngs.remove(SKOSXL.Res.URI);
			graphFilter = new ARTResource[ngs.size()];
			ngs.toArray(graphFilter);	
			System.out.println("named graphs: " + graphFilter + " after cleanup of model vocabularies");
		}
		if (graphFilter == null) {
			System.err.println("output format not recognized, add this to either the quad or triple format lists in this routine");			
		}
					
		sourceModel.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), graphFilter);

	}
}

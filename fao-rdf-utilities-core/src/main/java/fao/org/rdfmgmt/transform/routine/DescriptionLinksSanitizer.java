package fao.org.rdfmgmt.transform.routine;

import fao.org.rdfmgmt.owlart.ModelLoader;
import fao.org.rdfmgmt.vocabularies.DCMIMetadataTerms;
import fao.org.rdfmgmt.vocabularies.VOCBENCH;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.XmlSchema;

import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;

public class DescriptionLinksSanitizer {

	public static ARTStatement getStatement(RDFModel model, ARTResource subj, ARTURIResource pred,
			ARTNode obj, boolean inferred, ARTResource graphs) throws ModelAccessException {
		ARTStatementIterator it = model.listStatements(subj, pred, obj, inferred, graphs);
		ARTStatement stat = null;
		if (it.hasNext())
			stat = it.getNext();
		it.close();
		return stat;
	}

	/**
	 * this main method takes an input file (first argument) in some RDF serialization format, loads it by
	 * guessing the format from the file extension, and then converts it into another serialization format
	 * writing it onto an output file (second argument). The third optional parameter specifies the class
	 * implementing OWL ART API (the ModelFactory implementation class). By default, if third argument is not
	 * specified, Sesame2 implementation is being used.
	 * 
	 * @param args
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ModelCreationException
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 * @throws UnsupportedRDFFormatException
	 * @throws BadConfigurationException
	 * @throws QueryEvaluationException
	 * @throws MalformedQueryException
	 * @throws UnsupportedQueryLanguageException
	 */
	public static void main(String[] args) throws IOException, ModelCreationException,
			BadConfigurationException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException {
		if (args.length < 3) {
			System.out
					.println("usage:\njava "
							+ DescriptionLinksSanitizer.class.getCanonicalName()
							+ " <sourceModelConfigfile> <datasetfile> <outputDatasetFile> [targetModelConfigfile] \n"

							+ "\t<sourceModelConfigfile>    : config file for the source model chosen RDFModel Implementation \n"
							+ "\t<datasetfile>    : the file of the dataset to be transformed \n"
							+ "\t<outputDatasetFile>    : the file of the transformed dataset \n"
							+ "\t[targetModelConfigfile]    : config file for the target model chosen RDFModel Implementation. If not specified, the sourceModelConfigFile is used for the target too \n");
			;
			return;
		}

		String srcModelConfigFile = args[0];
		String tgtModelConfigFile;
		File inputFile = new File(args[1]);
		File targetFile = new File(args[2]);

		if (args.length < 4) {
			tgtModelConfigFile = srcModelConfigFile;
		} else {
			tgtModelConfigFile = args[3];
		}

		// OWL ART SKOSXLMODEL LOADING
		RDFModel srcModel = ModelLoader.loadRDFModel(srcModelConfigFile);
		RDFModel tgtModel = ModelLoader.loadRDFModel(tgtModelConfigFile);

		// LOADING PRODUCED DATA IN SOURCE SERIALIZATION FORMAT

		srcModel.addRDF(inputFile, srcModel.getBaseURI(), RDFFormat.guessRDFFormatFromFile(inputFile));

		// LOADING OTHER RESOURCES

		// PROCESS
		ARTStatementIterator it = srcModel.listStatements(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY,
				false, NodeFilters.MAINGRAPH);
		while (it.streamOpen()) {
			ARTStatement stat = it.getNext();

			ARTURIResource pred = stat.getPredicate();

			if (pred.equals(VOCBENCH.Res.hasLink)) {
				// LINK ELEMENTS
				ARTURIResource subjDefImg = stat.getSubject().asURIResource();
				ARTLiteral linkValue = stat.getObject().asLiteral();
				String linkLabel = linkValue.getLabel();

				// CONCEPT ELEMENTS
				ARTStatement conceptStatement = getStatement(srcModel, NodeFilters.ANY, NodeFilters.ANY,
						subjDefImg, false, NodeFilters.MAINGRAPH);
				ARTURIResource concept = conceptStatement.getSubject().asURIResource();
				ARTURIResource propDefImg = conceptStatement.getPredicate();

				// SOURCE ELEMENTS
				ARTStatement statHasSource = getStatement(srcModel, subjDefImg, VOCBENCH.Res.hasSource,
						NodeFilters.ANY, false, NodeFilters.MAINGRAPH);
				ARTLiteral sourceValue = null;
				String sourceLabel = null;
				if (statHasSource != null) {
					sourceValue = statHasSource.getObject().asLiteral();
					sourceLabel = sourceValue.getLabel();
				}

				// case of descriptions of books
				if (propDefImg.equals(SKOS.Res.DEFINITION) && sourceLabel != null
						&& sourceLabel.equals("Book")) {
					ARTLiteral newSource = null;
					newSource = tgtModel.createLiteral("Book: " + linkLabel, XmlSchema.Res.STRING);
					tgtModel.addTriple(subjDefImg, VOCBENCH.Res.hasSource, newSource, NodeFilters.MAINGRAPH);

				} else { // case of link with no Book, the source is dropped as the link is
							// enough
					try {
						tgtModel.addTriple(subjDefImg, DCMIMetadataTerms.Res.SOURCE,
								tgtModel.createURIResource(linkValue.getLabel()), NodeFilters.MAINGRAPH);
					} catch (Exception e) {
						try {
							ARTURIResource newSource = tgtModel.createURIResource("http://" + linkLabel);
							tgtModel.addTriple(subjDefImg, DCMIMetadataTerms.Res.SOURCE, newSource,
									NodeFilters.MAINGRAPH);
							System.err.println("Fixed link of definition " + subjDefImg.getLocalName()
									+ "of concept: " + concept.getLocalName()
									+ " by adding the \"http://\" preamble: " + newSource);
						} catch (Exception e1) {
							System.err.println("Unable to process link of definition"
									+ subjDefImg.getLocalName() + "of concept: " + concept.getLocalName()
									+ " : " + linkLabel);
						}
					}
				}

			} else if (pred.equals(VOCBENCH.Res.hasSource)) {
				// in theory, do nothing, sources have been managed in the hasLink handler here above
				// @TODO in practice, I have to take into account the case where there's a source
				// without a link

				// in the end I did not add them, as they were very few, and I preferred to just report them
				// for manual fix by the users

				// SOURCE ELEMENTS
				ARTURIResource subjDefImg = stat.getSubject().asURIResource();
				ARTLiteral sourceValue = stat.getObject().asLiteral();
				String sourceLabel = sourceValue.getLabel();

				// CONCEPT ELEMENTS
				ARTStatement conceptStatement = getStatement(srcModel, NodeFilters.ANY, NodeFilters.ANY,
						subjDefImg, false, NodeFilters.MAINGRAPH);
				ARTURIResource concept = conceptStatement.getSubject().asURIResource();

				// LINK ELEMENTS
				ARTStatement statHasLink = getStatement(srcModel, subjDefImg, VOCBENCH.Res.hasLink,
						NodeFilters.ANY, false, NodeFilters.MAINGRAPH);

				if (statHasLink == null) {
					System.err.println("we have a source alone: " + sourceLabel + " for definition: "
							+ subjDefImg + " of concept: " + concept);
				}

			} else {
				tgtModel.addStatement(stat, NodeFilters.MAINGRAPH);
			}

		}
		it.close();

		/*
		 * NOTES: hasSource triples need to be always removed, unless there's a link so the effects are the
		 * same of the algorithm in https://aims-fao.atlassian.net/browse/AGROVOC-222 just the procedure is
		 * different: all statements are copied except hasLink and hasSource (exclude them) in hasLink
		 * management, there's the procedure for chekcing if and how to keep/convert both the link and the
		 * hasSource
		 */

		//@formatter:off
		/*
			For each def/img {
			    if (obj(vb:hasSource) != null AND ( (obj(vb:hasSource) == "Book") OR (obj(vb:hasSource) == "Other") ) {
			        if (obj(vb:hasLink) != null)
			            convert(vb:hasLink, vb:hasSource, mod:noURIconversionJustPlainString); //but preempt: "Book: " to the book title
			        else
			            delete both hasSource and hasLink triples and do not replace them with anything else (an indicator "book" with no title, or a other with no link, makes no sense)
			        continue;
			    }     
			
			    // we are not in Book nor in Other case, and there's a URL
			    if (obj(vb:hasLink) != null) {
			        convert(vb:hasLink, dct:source, mod:convertToURI)
			        deleteTriple(vb:hasSource) //no need to use the source string if we have a URL
			    }
			
			    // we are not in Book nor in Other case, and there's no URL
			    else {
			        convert(vb:hasSource, dct:source, mod:lookOutTheGeneralURIForEachNotableSourceType) //e.g. I have source=Wikipedia, and no link. I replace this with dct:source = the URL of wikipedia
			    }
			  
			}		
		  }
		*/
		//@formatter:on

		tgtModel.writeRDF(targetFile, RDFFormat.guessRDFFormatFromFile(inputFile), NodeFilters.MAINGRAPH);

	}
}

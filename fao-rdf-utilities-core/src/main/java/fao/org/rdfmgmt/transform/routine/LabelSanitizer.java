package fao.org.rdfmgmt.transform.routine;

import fao.org.rdfmgmt.owlart.ModelLoader;
import fao.org.rdfmgmt.release.FAORDFDatasetReleaser;
import fao.org.rdfmgmt.vocabularies.AGRONTOLOGY;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.RDFSModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTNodeIterator;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.query.BooleanQuery;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.HashSet;

public class LabelSanitizer {

	private static HashSet<String> stopWords = new HashSet();

	private static String toTitleCase(String label) throws ModelUpdateException {
		String[] words = label.split(" ");
		StringBuffer sanitizedLabel = new StringBuffer("");
		for (String word : words) {
			// case for hyphen separated words: here I don't look for stopwords
			if (word.contains("-")) {
				String[] subwords = word.split("-");
				StringBuffer sanitizedHyphenedTerm = new StringBuffer("");
				for (String subword : subwords) {
					String lowerCaseSubWord = subword.toLowerCase();
					sanitizedHyphenedTerm.append(Character.toUpperCase(lowerCaseSubWord.charAt(0)));
					sanitizedHyphenedTerm.append(lowerCaseSubWord.substring(1, lowerCaseSubWord.length()));
					sanitizedHyphenedTerm.append("-");
				}
				sanitizedHyphenedTerm.deleteCharAt(sanitizedHyphenedTerm.length() - 1);
				sanitizedLabel.append(sanitizedHyphenedTerm);

			} else {
				String lowerCaseWord = word.toLowerCase();
				if (stopWords.contains(lowerCaseWord))
					sanitizedLabel.append(lowerCaseWord);
				else {
					sanitizedLabel.append(Character.toUpperCase(lowerCaseWord.charAt(0)));
					sanitizedLabel.append(lowerCaseWord.substring(1, lowerCaseWord.length()));
				}

			}

			sanitizedLabel.append(" ");
		}
		return sanitizedLabel.toString().trim();
	}

	private static String toSentenceCase(String label) throws ModelUpdateException {

		String lowerCaseLabel = label.toLowerCase();
		return lowerCaseLabel.substring(0, 1).toUpperCase()
				.concat(lowerCaseLabel.substring(1, lowerCaseLabel.length()));
	}

	private static boolean isTaxonomiciOrScientific(RDFModel srcModel, ARTResource label)
			throws ModelAccessException {

		if (srcModel.hasTriple(NodeFilters.ANY, AGRONTOLOGY.Res.hasScientificName, label, false))
			return true;
		ARTNodeIterator it = srcModel.listValuesOfSubjPredPair(label, AGRONTOLOGY.Res.hasTermType, false);
		while (it.streamOpen()) {
			String termType = it.getNext().asLiteral().getLabel();
			if (termType.toLowerCase().startsWith("taxonomic"))
				return true;
		}

		return false;
	}

	private static boolean isAcronymOrAbbreviation(RDFModel srcModel, ARTResource label)
			throws ModelAccessException {

		// b. objects of (literal of) agrontology:hasAcronym
		if (srcModel.hasTriple(NodeFilters.ANY, AGRONTOLOGY.Res.hasAcronym, label, false))
			return true;

		// c. object of agrontology:hasAbbreviation (inverse inferred - isAbbreviationOf)
		// (Example: Nuclear Magnetic Resonance Spectroscopy hasAbbreviation NMR spectroscopy)
		if (srcModel.hasTriple(NodeFilters.ANY, AGRONTOLOGY.Res.hasAbbreviation, label, false))
			return true;

		// d. object of agrontology:hasChemicalFormula (inverse inferred - IsChemicalFormulaOf)
		if (srcModel.hasTriple(NodeFilters.ANY, AGRONTOLOGY.Res.hasChemicalFormula, label, false))
			return true;

		// e. object of agrontology:hasSymbol (inverse inferred - isSymbolOf ).
		// (Example: oxygen hasSymbol O (symbole))
		if (srcModel.hasTriple(NodeFilters.ANY, AGRONTOLOGY.Res.hasSymbol, label, false))
			return true;

		// a. (literals of) labels with property agrontology:hasTermType "Acronym"
		ARTNodeIterator it = srcModel.listValuesOfSubjPredPair(label, AGRONTOLOGY.Res.hasTermType, false);
		while (it.streamOpen()) {
			String termType = it.getNext().asLiteral().getLabel();
			if (termType.toLowerCase().startsWith("Acronym"))
				return true;
		}

		return false;
	}

	private static String processGermanLabel(RDFModel srcModel, ARTResource subj, String label)
			throws ModelAccessException, ModelUpdateException {
		if (isTaxonomiciOrScientific(srcModel, subj.asResource()))
			return toSentenceCase(label);
		if (isAcronymOrAbbreviation(srcModel, subj.asResource()))
			return label.toUpperCase();

		// post processing
		String titleCasedString = toTitleCase(label);
		String USAedString = titleCasedString.replace("(usa)", "(USA)");
		String adjLowerCasedString = USAedString.replace("Übertragene", "übertragene")
				.replace("Uebertragene", "uebertragene").replace("Weibliches", "weibliches")
				.replace("Modifizierter", "modifizierter").replace("Veraenderte", "veraenderte");
		return adjLowerCasedString;
		
	}

	private static boolean isEnglishException(RDFModel model, ARTURIResource xLabel)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException,
			QueryEvaluationException {

		/* @formatter:off */
		// see https://aims-fao.atlassian.net/browse/AGROVOCGL-1 for a description of the query
		final String queryTemplate = 
						"prefix skos: <http://www.w3.org/2004/02/skos/core#>                                                                       \n" +
						"prefix skosxl: <http://www.w3.org/2008/05/skos-xl#>                                                                       \n" +
						"                                                                                                                          \n" +
						"ASK {                                                                                                                     \n" +
						"   { _xlabel_ <http://aims.fao.org/aos/agrontology#hasTermType> _:a .  }                                                  \n" +
						"   UNION                                                                                                                  \n" +
						"   { _xlabel_ <http://aims.fao.org/aos/agrontology#hasTaxonomicLevel> _:b .  }                                            \n" +
						"   UNION                                                                                                                  \n" +
						"   { _:c <http://aims.fao.org/aos/agrontology#hasChemicalFormula> _xlabel_ . }                                            \n" +
						"   UNION                                                                                                                  \n" +
						"   { _:ds <http://aims.fao.org/aos/agrontology#hasAbbreviation> _xlabel_ . }                                              \n" +
						"   UNION                                                                                                                  \n" +
						"   { _xlabel_ <http://aims.fao.org/aos/agrontology#hasAbbreviation>  _:do . }                                             \n" +
						"   UNION                                                                                                                  \n" +
						"   { _:es <http://aims.fao.org/aos/agrontology#hasAcronym> _xlabel_ . }                                                   \n" +
						"   UNION                                                                                                                  \n" +
						"   {  _xlabel_ <http://aims.fao.org/aos/agrontology#hasAcronym> _:eo . }                                                  \n" +
						"   UNION                                                                                                                  \n" +
						"   { _:f <http://aims.fao.org/aos/agrontology#hasScientificName> _xlabel_ . }                                             \n" +
						"   UNION                                                                                                                  \n" +
						"   { _:g <http://aims.fao.org/aos/agrontology#hasSymbol> _xlabel_ . }                                                     \n" +
						"                                                                                                                          \n" +
						"   UNION                                                                                                                  \n" +
						"   {                                                                                                                      \n" +
						"      {                                                                                                                   \n" +
						"         {?c skosxl:prefLabel _xlabel_ .}                                                                                 \n" +
						"         UNION                                                                                                            \n" +
						"         {?c skosxl:altLabel _xlabel_ .}                                                                                  \n" +
						"      }                                                                                                                   \n" +
						"      {                                                                                                                   \n" +
						"         { ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_3918> .	  }  #international organizations                  \n" +
						"         UNION                                                                                                            \n" +
						"         { ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_8069> .	  }  #UN                                           \n" +
						"                                                                                                                          \n" +
						"         #all geo names                                                                                                   \n" +
						"                                                                                                                          \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_24920> .	   }                                             \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_330997> .	   }                                           \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_29698> .	   }                                             \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_4160> .	   }                                             \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_6617> .	   }                                             \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_4607> .	   }                                             \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_49820> .	   }                                             \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_330904> .	   }                                             \n" +
						"                                                                                                                          \n" +
						"         #all scientific names                                                                                            \n" +
						"                                                                                                                          \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_330957> .	   }                                           \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_330944> .	   }                                           \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_4807> .	   }                                             \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_4906> .	   }                                             \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_330939> .	   }                                           \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_14083> .	   }                                             \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_7280> .	   }                                             \n" +
						"         UNION                                                                                                            \n" +
						"         {  ?c skos:broader+ <http://aims.fao.org/aos/agrovoc/c_5023> .	   }                                             \n" +
						"      }                                                                                                                   \n" +
						"   }                                                                                                                      \n" +
						"}";
		/* @formatter:on */

		String queryString = queryTemplate.replace("_xlabel_", "<" + xLabel.getURI() + ">");
		BooleanQuery query = model.createBooleanQuery(queryString);
		return query.evaluate(false);
	}

	private static String processEnglishLabel(RDFModel srcModel, ARTResource xLabel, String label)
			throws ModelAccessException, ModelUpdateException, UnsupportedQueryLanguageException,
			MalformedQueryException, QueryEvaluationException {
		// if (isAcronymOrAbbreviation(srcModel, xLabel))
		// return label.toUpperCase();
		if (!isEnglishException(srcModel, xLabel.asURIResource()))
			return label.toLowerCase();

		return label;
	}

	/**
	 * this main method takes an input file (first argument) in some RDF serialization format, loads it by
	 * guessing the format from the file extension, and then converts it into another serialization format
	 * writing it onto an output file (second argument). The third optional parameter specifies the class
	 * implementing OWL ART API (the ModelFactory implementation class). By default, if third argument is not
	 * specified, Sesame2 implementation is being used.
	 * 
	 * @param args
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ModelCreationException
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 * @throws UnsupportedRDFFormatException
	 * @throws BadConfigurationException
	 * @throws QueryEvaluationException
	 * @throws MalformedQueryException
	 * @throws UnsupportedQueryLanguageException
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException, ClassNotFoundException,
			InstantiationException, IllegalAccessException, ModelCreationException, ModelUpdateException,
			ModelAccessException, UnsupportedRDFFormatException, BadConfigurationException,
			UnsupportedQueryLanguageException, MalformedQueryException, QueryEvaluationException {
		if (args.length < 4) {
			System.out
					.println("usage:\njava "
							+ FAORDFDatasetReleaser.class.getCanonicalName()
							+ " <sourceModelConfigfile> <datasetfile> <language> <outputDatasetFile> [targetModelConfigfile] \n"

							+ "\t<sourceModelConfigfile>    : config file for the source model chosen RDFModel Implementation \n"
							+ "\t<datasetfile>    : the file of the dataset to be transformed \n"
							+ "\t<language>    : the language of the labels to be sanitized \n"
							+ "\t<outputDatasetFile>    : the file of the transformed dataset \n"
							+ "\t[targetModelConfigfile]    : config file for the target model chosen RDFModel Implementation. If not specified, the sourceModelConfigFile is used for the target too \n");
			;
			return;
		}

		String srcModelConfigFile = args[0];
		String tgtModelConfigFile;
		File inputFile = new File(args[1]);
		File targetFile = new File(args[2]);
		String language = args[3];

		if (args.length < 5) {
			tgtModelConfigFile = srcModelConfigFile;
		} else {
			tgtModelConfigFile = args[4];
		}

		// OWL ART SKOSXLMODEL LOADING
		RDFModel srcModel = ModelLoader.loadRDFModel(srcModelConfigFile);
		RDFModel tgtModel = ModelLoader.loadRDFModel(tgtModelConfigFile);

		// LOADING PRODUCED DATA IN SOURCE SERIALIZATION FORMAT

		srcModel.addRDF(inputFile, srcModel.getBaseURI(), RDFFormat.guessRDFFormatFromFile(inputFile));

		// LOADING OTHER RESOURCES

		String stopWordsFile;

		switch (language) {
		case "de":
			stopWordsFile = "/germanstopwords.txt";
			break;
		default:
			stopWordsFile = null;
		}

		if (stopWordsFile != null) {
			InputStream in = LabelSanitizer.class.getResourceAsStream(stopWordsFile);
			Reader r = new InputStreamReader(in, "utf-8");
			BufferedReader bf = new BufferedReader(r);
			String line;
			while ((line = bf.readLine()) != null) {
				stopWords.add(line.trim());
			}
		}

		System.out.println(stopWords);

		// PROCESS
		ARTStatementIterator it = srcModel.listStatements(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY,
				false, NodeFilters.MAINGRAPH);
		while (it.streamOpen()) {
			ARTStatement stat = it.getNext();
			// System.out.println(stat);
			ARTResource subj = stat.getSubject();
			ARTURIResource pred = stat.getPredicate();
			ARTNode obj = stat.getObject();

			boolean modified = false;

			if (pred.equals(SKOSXL.Res.LITERALFORM) && obj.isLiteral()) {
				String lang = obj.asLiteral().getLanguage().toLowerCase();
				String label = obj.asLiteral().getLabel();
				String sanitizedLabel;

				try {

					if (lang.equals(language)) {
						modified = true;
						if (lang.equals("de"))
							sanitizedLabel = processGermanLabel(srcModel, subj, label);
						else if (lang.equals("en"))
							sanitizedLabel = processEnglishLabel(srcModel, subj, label);
						else
							continue;

						ARTLiteral sanitizedLiteral = tgtModel.createLiteral(sanitizedLabel, language);
						tgtModel.addTriple(subj, pred, sanitizedLiteral);
						if (!sanitizedLabel.equals(label)) {
							System.out.println(String.format("%-50s-->     %-30s", obj.asLiteral(),
									sanitizedLiteral));
						}
					}

				} catch (Exception e) {
					System.err.println("error while processing label: " + label);
					e.printStackTrace();
					System.exit(0);
				}
			}

			if (!modified)
				tgtModel.addStatement(stat, NodeFilters.MAINGRAPH);
		}
		it.close();

		tgtModel.writeRDF(targetFile, RDFFormat.guessRDFFormatFromFile(inputFile), NodeFilters.MAINGRAPH);

	}
}

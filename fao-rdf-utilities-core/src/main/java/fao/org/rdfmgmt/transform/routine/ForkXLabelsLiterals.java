package fao.org.rdfmgmt.transform.routine;

import fao.org.rdfmgmt.owlart.ModelLoader;
import fao.org.rdfmgmt.vocabularies.AGRONTOLOGY;
import fao.org.rdfmgmt.vocabularies.DCMIMetadataTerms;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;
import it.uniroma2.art.owlart.vocabulary.XmlSchema;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * this is more a routine for a specific task than a general tool, in that it covers the issues listed here:
 * https://aims-fao.atlassian.net/browse/AGROVOC-42, by generating new skosxl:Labels in place of the literals
 * attached to existing skosxl:Labels by means of various properties, such as agrontology:hasPlural,
 * agrontology:hasSpellingVariant and then like.<br/>
 * The (multiple) issue is that skosxl:Labels should have only one literalForm and not conveying any other
 * related label<br/>
 * 
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 *
 */
public class ForkXLabelsLiterals {

	private static String createTruncatedUUID() {
		return UUID.randomUUID().toString().split("-")[0];
	}

	public static ARTURIResource createXLabelAndAddAsAltLabel(SKOSXLModel model, ARTURIResource subject,
			String label, String language) throws ModelUpdateException, ModelAccessException {
		ARTURIResource xLabel = model.addXLabel(model.getDefaultNamespace() + "xl_" + language + "_"
				+ createTruncatedUUID(), label, language);
		model.addAltXLabel(subject, xLabel);
		return xLabel;
	}

	// <http://aims.fao.org/aos/agrovoc/xl_en_1299502683449>
	// <http://aims.fao.org/aos/agrontology#hasScientificName>
	// <http://aims.fao.org/aos/agrovoc/xl_en_1299502680191> .
	// <http://aims.fao.org/aos/agrovoc/xl_en_1299502683449> <http://aims.fao.org/aos/agrontology#hasTermType>
	// "Common name for plants"^^<http://www.w3.org/2001/XMLSchema#string> .
	// <http://aims.fao.org/aos/agrovoc/xl_en_1299502683449>
	// <http://art.uniroma2.it/ontologies/vocbench#hasStatus>
	// "Published"^^<http://www.w3.org/2001/XMLSchema#string> .
	// <http://aims.fao.org/aos/agrovoc/xl_en_1299502683449> <http://purl.org/dc/terms/created>
	// "1998-11-26T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> .
	// <http://aims.fao.org/aos/agrovoc/xl_en_1299502683449> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>
	// <http://www.w3.org/2008/05/skos-xl#Label> .
	// <http://aims.fao.org/aos/agrovoc/xl_en_1299502683449> <http://www.w3.org/2004/02/skos/core#notation>
	// "37689"^^<http://aims.fao.org/aos/agrovoc/AgrovocCode> .
	// <http://aims.fao.org/aos/agrovoc/xl_en_1299502683449> <http://www.w3.org/2008/05/skos-xl#literalForm>
	// "firsz"@en .

	public static void main(String[] args) throws IOException, ModelCreationException,
			BadConfigurationException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException {

		String configFile = args[0];
		File dataFile = new File(args[1]);
		File outputFile = new File(args[2]);

		SKOSXLModel model = ModelLoader.loadModel(configFile, SKOSXLModel.class);
		model.addRDF(dataFile, null, RDFFormat.guessRDFFormatFromFile(dataFile));

		ARTURIResource hasStatusProp = model
				.createURIResource("http://art.uniroma2.it/ontologies/vocbench#hasStatus");
		ARTLiteral statusPublishedValue = model.createLiteral("Published", XmlSchema.Res.STRING);
		ARTURIResource agrovocCodeDataType = model
				.createURIResource("http://aims.fao.org/aos/agrovoc/AgrovocCode");

		ARTStatementIterator it = model.listStatements(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY,
				false, NodeFilters.MAINGRAPH);
		while (it.hasNext()) {
			ARTStatement st = it.getNext();

			ARTURIResource xLabel;

			if (st.getPredicate().equals(AGRONTOLOGY.Res.hasSpellingVariant)) {

				xLabel = st.getSubject().asURIResource();

				// look for the concept to which the xLabel containing the variant is attached
				ARTStatementIterator innerIt = model.listStatements(NodeFilters.ANY, NodeFilters.ANY, xLabel,
						false, NodeFilters.MAINGRAPH);
				int count = 0;
				ARTURIResource concept = null;
				while (innerIt.hasNext()) {
					ARTStatement innerST = innerIt.getNext();
					ARTURIResource maybeLabelPred = innerST.getPredicate();
					if (maybeLabelPred.equals(SKOSXL.Res.ALTLABEL)
							|| maybeLabelPred.equals(SKOSXL.Res.PREFLABEL)) {
						// perform a check, a label should be attached to one concept only, otherwise
						// something is at least strange!
						count++;
						if (count > 1)
							throw new RuntimeException("there are two concepts bound to xLabel: " + xLabel
									+ ", last one is: " + innerST.getPredicate() + ", and previous one is: "
									+ concept);
						concept = innerST.getSubject().asURIResource();
					}

				}
				innerIt.close();
				if (concept == null || count == 0)
					throw new RuntimeException("no concept is bound to xLabel: " + xLabel);
				model.deleteStatement(st);
				ARTLiteral spellingVariant = st.getObject().asLiteral();
				ARTURIResource newXLabel = createXLabelAndAddAsAltLabel(model, concept,
						spellingVariant.getLabel(), spellingVariant.getLanguage());

				model.addTriple(newXLabel, hasStatusProp, statusPublishedValue);
				model.addTriple(newXLabel, DCMIMetadataTerms.Res.CREATED,
						model.createLiteral(XmlSchema.formatCurrentUTCDateTime(), XmlSchema.Res.DATETIME));

				ARTStatementIterator notationIt = model.listStatements(xLabel, SKOS.Res.NOTATION,
						NodeFilters.ANY, false);
				String notation;
				if (notationIt.hasNext())
					notation = notationIt.getNext().getObject().asLiteral().getLabel();
				else
					throw new RuntimeException("no notation for xLabel: " + xLabel);
				notationIt.close();

				model.addTriple(newXLabel, SKOS.Res.NOTATION,
						model.createLiteral(notation + createTruncatedUUID(), agrovocCodeDataType));
				model.addTriple(xLabel, AGRONTOLOGY.Res.hasSpellingVariant, newXLabel);

			}

		}

		it.close();

		model.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);

	}
}

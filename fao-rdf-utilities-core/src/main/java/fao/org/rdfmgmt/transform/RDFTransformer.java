package fao.org.rdfmgmt.transform;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;

import java.io.IOException;

/**
 * simple base abstract class for supporting transformation of RDF data in an efficient way, by taking into
 * consideration two cases:
 * <ul>
 * <li>a model is being loaded and processed by the transformation</li>
 * <li>a model is being loaded and the transformed triples are copied into the target model</li>
 * </ul>
 * 
 * and relieving the developer from managing the deletions and additions of triples depending on the two cases
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 */
public abstract class RDFTransformer {

	boolean sourceAndTargetModelAreTheSame;

	protected RDFModel sourceModel;
	protected RDFModel targetModel;

	public RDFTransformer(RDFModel sourceModel, RDFModel targetModel) {
		this.sourceModel = sourceModel;
		this.targetModel = targetModel;
		sourceAndTargetModelAreTheSame = false;
		if (sourceModel == targetModel)
			throw new IllegalStateException(
					"source and target model must be different, use the constructor with the single model if you want them to be the same");
	}

	public RDFTransformer(RDFModel model) {
		this.sourceModel = model;
		this.targetModel = model;
		sourceAndTargetModelAreTheSame = true;
	}
	
	/**
	 * if <code>source</code> and
	 * <code>target/code> models are the same, the triple is simply maintained in the processed model
	 * 
	 * @param subject
	 * @param predicate
	 * @param object
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public void keepTriple(ARTResource subject, ARTURIResource predicate, ARTNode object,
			ARTResource... graphs) throws ModelUpdateException {
		if (!sourceAndTargetModelAreTheSame)
			targetModel.addTriple(subject, predicate, object, graphs);
	}

	/**
	 * this should be used only when really willing to add a <em>new</em> triple (no matter if the target
	 * model is the same or different)
	 * 
	 * @param subject
	 * @param predicate
	 * @param object
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public void addTriple(ARTResource subject, ARTURIResource predicate, ARTNode object,
			ARTResource... graphs) throws ModelUpdateException {
		targetModel.addTriple(subject, predicate, object, graphs);
	}

	/**
	 * if <code>source</code> and
	 * <code>target/code> models are not the same, then the triple does not need to be deleted, it is simply 
	 * not transferred to the the target model
	 * 
	 * @param subject
	 * @param predicate
	 * @param object
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public void deleteTriple(ARTResource subject, ARTURIResource predicate, ARTNode object,
			ARTResource... graphs) throws ModelUpdateException {
		if (sourceAndTargetModelAreTheSame)
			targetModel.deleteTriple(subject, predicate, object, graphs);
	}
}

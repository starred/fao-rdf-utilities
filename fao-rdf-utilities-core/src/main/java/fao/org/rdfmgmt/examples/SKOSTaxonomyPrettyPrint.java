package fao.org.rdfmgmt.examples;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;

import java.io.File;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fao.org.rdfmgmt.owlart.ModelLoader;

/**
 * This class represents a simple example of use of OWL ART API for managing a SKOS-XL RDF repository.<br/>
 * The main method of this class requires one argument; this can be the name of an input file containing the
 * SKOS data to be loaded, or have the value: "preloaded", meaning that the data has already been loaded on a
 * previous run of the class. The program stops by asking the user to press a key. This is necessary since
 * some triple store implementations may wait a while before saving their managed data. For example,
 * Sesame2Implementation of OWL ART API is configured to wait 1 second before saving data on the persistence
 * directory.<br/>
 * Note when using Sesame2: remember to press the key when program stops, otherwise forcing the program to end
 * will leave a lock on the ModelData folder, which needs to be manually removed
 * 
 * @author Sachit Rajbhandari <Sachit.Rajbhandari@fao.org>
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class SKOSTaxonomyPrettyPrint {

	protected static Logger logger = LoggerFactory.getLogger(SKOSTaxonomyPrettyPrint.class);

	public static void main(String args[]) {

		if (args.length < 2) {
			System.out.println("usage:\n"
					+ "java fao.org.owl2skos.examples.TaxonomyPrettyPrint <inputfilename|\"preloaded\"> configFile ");
			return;
		}

		try {
			SKOSTaxonomyPrettyPrint skosTest = new SKOSTaxonomyPrettyPrint();
			SKOSXLModel skosXLModel;
			
			skosXLModel = ModelLoader.loadModel(args[1], SKOSXLModel.class);
			
			if (args[0].equals("preloaded")) {
				logger.info("standard model loading");
			} else {
				logger.info("first time loading the model: taking care of importing data from file: "
						+ args[0]);
				File inputFile = new File(args[0]);
				skosXLModel.addRDF(inputFile, skosXLModel.getBaseURI(), RDFFormat
						.guessRDFFormatFromFile(inputFile));
			}
			ArrayList<ARTURIResource> topConcepts = skosTest.loadTopConcepts(skosXLModel);
			int level = -1;
			for (ARTURIResource skosConcept : topConcepts) {
				skosTest.printConcept(skosXLModel, skosConcept, level);
				ArrayList<ARTURIResource> narrowerConcepts = skosTest.getNarrowerConcept(skosXLModel,
						skosConcept);
				System.out.println(level + " : " + narrowerConcepts.size());
				skosTest.printChildConcept(skosXLModel, narrowerConcepts, level);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void printChildConcept(SKOSXLModel skosXLModel, ArrayList<ARTURIResource> skosConcepts, int level)
			throws ModelAccessException {
		level++;
		for (ARTURIResource skosConcept : skosConcepts) {
			printConcept(skosXLModel, skosConcept, level);
			ArrayList<ARTURIResource> narrowerConcepts = getNarrowerConcept(skosXLModel, skosConcept);
			printChildConcept(skosXLModel, narrowerConcepts, level);
		}
	}


	public ArrayList<ARTURIResource> loadTopConcepts(SKOSXLModel skosXLModel) throws ModelAccessException {
		// ARTURIResourceIterator itr = skosXLModel.listTopConceptsInScheme(skosXLModel.getDefaultSchema());
		ARTURIResourceIterator itr = skosXLModel.listConceptsInScheme(skosXLModel.getDefaultSchema());
		logger.info("defaultScheme: " + skosXLModel.getDefaultSchema());
		ArrayList<ARTURIResource> topConcepts = new ArrayList<ARTURIResource>();
		while (itr.hasNext()) {			
			ARTURIResource skosConcept = itr.next();
			if (skosXLModel.isTopConcept(skosConcept, skosXLModel.getDefaultSchema()))
				topConcepts.add(skosConcept);

		}
		return topConcepts;
	}

	public ArrayList<ARTURIResource> getBroaderConcept(SKOSXLModel skosXLModel, ARTURIResource skosConcept)
			throws ModelAccessException {
		ArrayList<ARTURIResource> broaderConcepts = new ArrayList<ARTURIResource>();
		// now we have two booleans: transitive is set to false as before, to get only direct relationships,
		// while inference is true, so that both broader and inverse narrower relationships are considered
		ARTURIResourceIterator itr = skosXLModel.listNarrowerConcepts(skosConcept, false, true);
		while (itr.hasNext()) {
			ARTURIResource broaderConcept = itr.next();
			if (skosXLModel.isInScheme(skosConcept, skosXLModel.getDefaultSchema()))
				broaderConcepts.add(broaderConcept);

		}
		return broaderConcepts;
	}

	public ArrayList<ARTURIResource> getNarrowerConcept(SKOSXLModel skosXLModel, ARTURIResource skosConcept)
			throws ModelAccessException {
		ArrayList<ARTURIResource> narrowerConcepts = new ArrayList<ARTURIResource>();
		// now we have two booleans: transitive is set to false as before, to get only direct relationships,
		// while inference is true, so that both narrower and inverse broader relationships are considered
		ARTURIResourceIterator itr = skosXLModel.listNarrowerConcepts(skosConcept, false, true);
		while (itr.hasNext()) {
			ARTURIResource narrowerConcept = itr.next();
			if (skosXLModel.isInScheme(skosConcept, skosXLModel.getDefaultSchema()))
				narrowerConcepts.add(narrowerConcept);
		}
		return narrowerConcepts;
	}

	public String getTabPosition(int level) {
		String tab = "";
		for (int i = 0; i < level; i++) {
			tab = tab + "\t";
		}
		return tab;
	}

	public void printConcept(SKOSXLModel skosXLModel, ARTURIResource skosConcept, int level)
			throws ModelAccessException {
		ARTResource xLabel = skosXLModel.getPrefXLabel(skosConcept, "en");
		ARTLiteral label = skosXLModel.getLiteralForm(xLabel);
		System.out.println(getTabPosition(level) + skosConcept + " : " + label);
	}
}

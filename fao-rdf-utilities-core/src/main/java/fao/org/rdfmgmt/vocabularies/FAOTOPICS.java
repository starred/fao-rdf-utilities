package fao.org.rdfmgmt.vocabularies;

public class FAOTOPICS extends SKOSVocabulary {

	public static final String name = "FAOTOPICS";
	public static final String baseURI = "http://aims.fao.org/aos/agrovoc";
	public static final String defNamespace = baseURI + "/";
	// the defaultScheme has changed. Last version to use the old defaultScheme is agrovoc_wb_20110223_v_1_3
	// (telugu terms edition)
	public static final String defaultScheme = baseURI;

	FAOTOPICS() {
		super(name, baseURI, defNamespace, defaultScheme);
	}
}

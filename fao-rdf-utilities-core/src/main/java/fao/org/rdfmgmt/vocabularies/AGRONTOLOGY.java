/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package fao.org.rdfmgmt.vocabularies;

import java.util.HashSet;
import java.util.Set;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.vocabulary.VocabUtilities;

/**
 * Vocabulary file for the Agrontology, driving AGROVOC SKOS vocabulary
 * 
 * @author Armando Stellato
 * 
 */
public class AGRONTOLOGY {

	/** http://aims.fao.org/aos/agrontology# **/
	public static final String NAMESPACE = "http://aims.fao.org/aos/agrontology#";

	// properties

	
	public static final String hasAbbreviation = NAMESPACE + "hasAbbreviation";
	
	public static final String hasAcronym = NAMESPACE + "hasAcronym";
	
	/** http://aims.fao.org/aos/agrontology#hasDisorder **/
	public static final String hasDisorder = NAMESPACE + "hasDisorder";
	
	/** http://aims.fao.org/aos/agrontology#hasPostProductionPractice **/
	public static final String hasPostProductionPractice = NAMESPACE + "hasPostProductionPractice";
	
	public static final String hasScientificName = NAMESPACE + "hasScientificName";	
	
	public static final String hasSpellingVariant = NAMESPACE + "hasSpellingVariant";
	
	
	public static final String hasSymbol = NAMESPACE + "hasSymbol";
	
	public static final String hasTermType = NAMESPACE + "hasTermType";
	
	public static final String hasChemicalFormula = NAMESPACE + "hasChemicalFormula";
	
		

	

	public static class Res {

		public static HashSet<ARTURIResource> classes = new HashSet<ARTURIResource>();

		// properties

		public static ARTURIResource hasAbbreviation;
		public static ARTURIResource hasAcronym;
		public static ARTURIResource hasChemicalFormula;
		public static ARTURIResource hasDisorder;
		public static ARTURIResource hasPostProductionPractice;
		public static ARTURIResource hasScientificName;	
		public static ARTURIResource hasSpellingVariant;
		public static ARTURIResource hasSymbol;
		public static ARTURIResource hasTermType;
		
		
			

		static {
			try {
				initialize(VocabUtilities.nodeFactory);
			} catch (VocabularyInitializationException e) {
				e.printStackTrace(System.err);
			}
		}

		public static void initialize(ARTNodeFactory nodeFact) throws VocabularyInitializationException {



			// properties
			hasAbbreviation = nodeFact.createURIResource(AGRONTOLOGY.hasAbbreviation);
			hasAcronym = nodeFact.createURIResource(AGRONTOLOGY.hasAcronym);
			hasChemicalFormula = nodeFact.createURIResource(AGRONTOLOGY.hasChemicalFormula);
			hasDisorder = nodeFact.createURIResource(AGRONTOLOGY.hasDisorder);
			hasPostProductionPractice = nodeFact.createURIResource(AGRONTOLOGY.hasPostProductionPractice);
			hasScientificName = nodeFact.createURIResource(AGRONTOLOGY.hasScientificName);
			hasSpellingVariant = nodeFact.createURIResource(AGRONTOLOGY.hasSpellingVariant);
			hasSymbol = nodeFact.createURIResource(AGRONTOLOGY.hasSymbol);
			hasTermType = nodeFact.createURIResource(AGRONTOLOGY.hasTermType);

		}

		public static Set<ARTURIResource> getClasses() {
			return classes;
		}

		public static void printVocabulary() {
			System.err.println("hasDisorder                    : " + hasDisorder);
			System.err.println("hasPostProductionPractice	   : " + hasPostProductionPractice);
			// ...
		}

	}

}

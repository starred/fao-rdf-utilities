package fao.org.rdfmgmt.vocabularies;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.vocabulary.VocabUtilities;

public class UAT extends SKOSVocabulary {

	public static final String name = "UAT";
	public static final String baseURI = "http://purl.org/astronomy/uat";
	public static final String defNamespace = baseURI + "#";
	public static final String defaultScheme = baseURI;

	UAT() {
		super(name, baseURI, defNamespace, defaultScheme);
	}

	public static class Res {
		
		static {
			try {
				initialize(VocabUtilities.nodeFactory);
			} catch (VocabularyInitializationException e) {
				e.printStackTrace(System.err);
			}
		}

		public static void initialize(ARTNodeFactory nodeFact) throws VocabularyInitializationException {

		}

	}

}

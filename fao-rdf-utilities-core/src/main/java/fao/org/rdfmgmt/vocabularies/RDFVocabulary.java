package fao.org.rdfmgmt.vocabularies;

public abstract class RDFVocabulary {

	protected String name;
	protected String baseURI;
	protected String defNamespace;

	protected RDFVocabulary(String name, String baseURI, String defNamespace) {
		this.name = name;
		this.baseURI = baseURI;
		this.defNamespace = defNamespace;
	}

	public String getName() {
		return name;
	}
	
	public String getBaseURI() {
		return baseURI;
	}

	public String getDefNamespace() {
		return defNamespace;
	}

	public String toString() {
		return name+" | "+baseURI+" | "+defNamespace;
	}
}

package fao.org.rdfmgmt.vocabularies;

public class AUTHCON_SERIES extends SKOSVocabulary {

	public static final String name = "AUTHCON_SERIES";
	public static final String baseURI = "http://aims.fao.org/aos/series";
	public static final String defNamespace = baseURI + "/";
	public static final String defaultScheme = defNamespace + "mainScheme";
	
	AUTHCON_SERIES() {
		super(name, baseURI, defNamespace, defaultScheme);
	}
}

package fao.org.rdfmgmt.vocabularies;

public class AUTHCON_PROJECTS extends SKOSVocabulary {

	public static final String name = "AUTHCON_PROJECTS";
	public static final String baseURI = "http://aims.fao.org/aos/fao_project";
	public static final String defNamespace = baseURI + "/";
	public static final String defaultScheme = defNamespace + "mainScheme";
	
	AUTHCON_PROJECTS() {
		super(name, baseURI, defNamespace, defaultScheme);
	}
}

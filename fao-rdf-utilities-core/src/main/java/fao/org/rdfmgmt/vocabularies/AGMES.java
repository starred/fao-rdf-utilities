/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package fao.org.rdfmgmt.vocabularies;

import java.util.HashSet;
import java.util.Set;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.vocabulary.VocabUtilities;

/**
 * Vocabulary file for the DCMI Metadata Terms IMCOMPLETE!!!
 * 
 * @author Armando Stellato
 * 
 */
public class AGMES {

	/** http://www.purl.org/agmes/1.1/ **/
	public static final String NAMESPACE = "http://www.purl.org/agmes/1.1/";

	// classes

	// properties

	/** http://www.purl.org/agmes/1.1/availabilityNumber **/
	public static final String AVAILABILITYNUMBER = NAMESPACE + "availabilityNumber";

	/** http://www.purl.org/agmes/1.1/availabilityLocation **/
	public static final String AVAILABILITYLOCATION = NAMESPACE + "availabilityLocation";

	public static class Res {

		public static HashSet<ARTURIResource> classes = new HashSet<ARTURIResource>();

		// classes

		// properties

		public static ARTURIResource AVAILABILITYNUMBER;
		public static ARTURIResource AVAILABILITYLOCATION;

		static {
			try {
				initialize(VocabUtilities.nodeFactory);
			} catch (VocabularyInitializationException e) {
				e.printStackTrace(System.err);
			}
		}

		public static void initialize(ARTNodeFactory nodeFact) throws VocabularyInitializationException {

			// properties
			AVAILABILITYNUMBER = nodeFact.createURIResource(AGMES.AVAILABILITYNUMBER);
			AVAILABILITYLOCATION = nodeFact.createURIResource(AGMES.AVAILABILITYLOCATION);

			// classes list initialization
			// classes.add(AGENT);

		}

		public static Set<ARTURIResource> getClasses() {
			return classes;
		}

		public static void printVocabulary() {
			System.err.println("AVAILABILITYNUMBER                      : " + AVAILABILITYNUMBER);
			// ...
		}

	}

}

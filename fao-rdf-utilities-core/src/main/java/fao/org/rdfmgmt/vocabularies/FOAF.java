package fao.org.rdfmgmt.vocabularies;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.vocabulary.VocabUtilities;

import java.util.HashSet;
import java.util.Set;

public class FOAF {

	/** http://xmlns.com/foaf/0.1/ **/
	public static final String NAMESPACE = "http://xmlns.com/foaf/0.1/";

	// classes

	/** http://xmlns.com/foaf/0.1/Organization **/
	public static final String ORGANIZATION = NAMESPACE + "Organization";	
	
	// properties

	/** http://xmlns.com/foaf/0.1/depiction **/
	public static final String DEPICTION = NAMESPACE + "depiction";	
	
	/** http://xmlns.com/foaf/0.1/img **/
	public static final String IMG = NAMESPACE + "img";	

	/** http://xmlns.com/foaf/0.1/Image **/
	public static final String IMAGE = NAMESPACE + "Image";	
	
	public static class Res {

		public static HashSet<ARTURIResource> classes = new HashSet<ARTURIResource>();

		// classes

		public static ARTURIResource ORGANIZATION;
		
		// properties

		public static ARTURIResource DEPICTION;
		public static ARTURIResource IMG;
		public static ARTURIResource IMAGE;


		static {
			try {
				initialize(VocabUtilities.nodeFactory);
			} catch (VocabularyInitializationException e) {
				e.printStackTrace(System.err);
			}
		}

		public static void initialize(ARTNodeFactory nodeFact) throws VocabularyInitializationException {

			// properties
			ORGANIZATION = nodeFact.createURIResource(FOAF.ORGANIZATION);
			DEPICTION = nodeFact.createURIResource(FOAF.DEPICTION);
			IMG = nodeFact.createURIResource(FOAF.IMG);
			IMAGE = nodeFact.createURIResource(FOAF.IMAGE);

			// classes list initialization
			// classes.add(AGENT);

		}

		public static Set<ARTURIResource> getClasses() {
			return classes;
		}

		public static void printVocabulary() {
			System.err.println("ORGANIZATION                      : " + ORGANIZATION);
			// ...
		}

	}
	
}

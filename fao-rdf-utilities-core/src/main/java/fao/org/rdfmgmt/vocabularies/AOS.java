package fao.org.rdfmgmt.vocabularies;

public class AOS extends RDFVocabulary {

	public static final String name = "AOS";
	public static final String baseURI = "http://aims.fao.org/aos/common";
	public static final String defNamespace = baseURI + "/";
	
	AOS() {
		super(name, baseURI, defNamespace);
	}
	
}

package fao.org.rdfmgmt.vocabularies;

public class JAD extends RDFVocabulary {

	public static final String name = "JAD";
	public static final String baseURI = "http://aims.fao.org/serials";
	public static final String defNamespace = baseURI + "/";
	
	JAD() {
		super(name, baseURI, defNamespace);
	}
}

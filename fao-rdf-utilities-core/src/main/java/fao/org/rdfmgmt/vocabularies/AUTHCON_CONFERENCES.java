package fao.org.rdfmgmt.vocabularies;

public class AUTHCON_CONFERENCES extends SKOSVocabulary {

	public static final String name = "AUTHCON_CONFERENCES";
	public static final String baseURI = "http://aims.fao.org/aos/conference";
	public static final String defNamespace = baseURI + "/";
	public static final String defaultScheme = defNamespace + "mainScheme";
	
	AUTHCON_CONFERENCES() {
		super(name, baseURI, defNamespace, defaultScheme);
	}
}

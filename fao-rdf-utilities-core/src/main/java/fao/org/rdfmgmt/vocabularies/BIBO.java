/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package fao.org.rdfmgmt.vocabularies;

import java.util.HashSet;
import java.util.Set;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.vocabulary.VocabUtilities;

/**
 * Vocabulary file for the Bibliographic Ontology
 * 
 * @author Armando Stellato
 * 
 */
public class BIBO {

	/** http://purl.org/ontology/bibo/ **/
	public static final String NAMESPACE = "http://purl.org/ontology/bibo/";

	// classes

	/** http://purl.org/ontology/bibo/Journal **/
	public static final String JOURNAL = NAMESPACE + "Journal";

	/** http://purl.org/ontology/bibo/Series **/
	public static final String SERIES = NAMESPACE + "Series";

	// properties

	/** http://purl.org/ontology/bibo/shortDescription **/
	public static final String SHORTDESCRIPTION = NAMESPACE + "shortDescription";
		
	/** http://purl.org/ontology/bibo/issn **/
	public static final String ISSN = NAMESPACE + "issn";
	
	/** http://purl.org/ontology/bibo/eissn **/
	public static final String EISSN = NAMESPACE + "eissn";
		
	

	public static class Res {

		public static HashSet<ARTURIResource> classes = new HashSet<ARTURIResource>();

		// classes

		public static ARTURIResource JOURNAL;
		public static ARTURIResource SERIES;

		// properties

		public static ARTURIResource SHORTDESCRIPTION;
		public static ARTURIResource ISSN;
		public static ARTURIResource EISSN;

		static {
			try {
				initialize(VocabUtilities.nodeFactory);
			} catch (VocabularyInitializationException e) {
				e.printStackTrace(System.err);
			}
		}

		public static void initialize(ARTNodeFactory nodeFact) throws VocabularyInitializationException {

			JOURNAL = nodeFact.createURIResource(BIBO.JOURNAL);
			SERIES = nodeFact.createURIResource(BIBO.SERIES);

			// properties
			SHORTDESCRIPTION = nodeFact.createURIResource(BIBO.SHORTDESCRIPTION);
			ISSN = nodeFact.createURIResource(BIBO.ISSN);
			EISSN = nodeFact.createURIResource(BIBO.EISSN);
			
			
			// classes list initialization
			classes.add(JOURNAL);
			classes.add(SERIES);

		}

		public static Set<ARTURIResource> getClasses() {
			return classes;
		}

		public static void printVocabulary() {
			System.err.println("JOURNAL                    : " + JOURNAL);
			System.err.println("SERIES		               : " + SERIES);
			// ...
		}

	}

}

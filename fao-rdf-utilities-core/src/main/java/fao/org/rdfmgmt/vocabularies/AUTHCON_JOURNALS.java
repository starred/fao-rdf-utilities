package fao.org.rdfmgmt.vocabularies;

public class AUTHCON_JOURNALS extends SKOSVocabulary {

	public static final String name = "AUTHCON_JOURNALS";
	public static final String baseURI = "http://aims.fao.org/aos/journal";
	public static final String defNamespace = baseURI + "/";
	public static final String defaultScheme = defNamespace + "mainScheme";
	
	AUTHCON_JOURNALS() {
		super(name, baseURI, defNamespace, defaultScheme);
	}
}

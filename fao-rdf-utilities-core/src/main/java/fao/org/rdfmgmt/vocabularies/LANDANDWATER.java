package fao.org.rdfmgmt.vocabularies;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.vocabulary.VocabUtilities;

public class LANDANDWATER extends SKOSVocabulary {

	public static final String name = "LANDANDWATER";
	public static final String baseURI = "http://www.fao.org/landandwater";
	public static final String defNamespace = baseURI + "/";
	// the defaultScheme has changed. Last version to use the old defaultScheme is agrovoc_wb_20110223_v_1_3
	// (telugu terms edition)
	public static final String defaultScheme = baseURI;

	// public static final String AGROVOC_CODE = defNamespace + "AgrovocCode";

	LANDANDWATER() {
		super(name, baseURI, defNamespace, defaultScheme);
	}

	public static class Res {

		// datatypes
		// public static ARTURIResource AGROVOC_CODE;
		
		static {
			try {
				initialize(VocabUtilities.nodeFactory);
			} catch (VocabularyInitializationException e) {
				e.printStackTrace(System.err);
			}
		}

		public static void initialize(ARTNodeFactory nodeFact) throws VocabularyInitializationException {

			// properties
			// AGROVOC_CODE = nodeFact.createURIResource(LANDANDWATER.AGROVOC_CODE);
		}

	}

}

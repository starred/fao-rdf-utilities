package fao.org.rdfmgmt.vocabularies;

import java.util.HashMap;

/**
 * Available vocabularies both contain static and instance data. Static one is useful when directly accessin
 * it in static code, while instance-methods are useful when accessing dynamically a vocabulary loaded
 * through the {@link #getVocabulary(String) method}
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class VocabularyFactory {

	static HashMap<String, RDFVocabulary> vocabularies;
	
	static {
		vocabularies = new HashMap<String, RDFVocabulary>();
		register(new ASFA());
		register(new AGROVOC());
		register(new LCSH());
		register(new AUTHCON_CONFERENCES());
		register(new AUTHCON_CORPORATEBODIES());
		register(new AUTHCON_JOURNALS());
		register(new AUTHCON_PROJECTS());
		register(new AUTHCON_SERIES());
		register(new BIOTECH());
		register(new JAD());
		register(new EUROVOC());
		register(new FAOTOPICS());
		register(new LANDANDWATER());
		register(new UAT());
	}
	
	public static void printAllVocabularies() {
		for (RDFVocabulary vocabulary : vocabularies.values()) {
			System.out.println(vocabulary);
		}
	}
	
	public static void register(RDFVocabulary voc) {
		vocabularies.put(voc.getName(), voc);
	}
	
	public static RDFVocabulary getVocabulary(String vocabularyName) {
		return vocabularies.get(vocabularyName);
	}
	
	public static void main(String[] args) {
		VocabularyFactory.printAllVocabularies();
	}
	
}

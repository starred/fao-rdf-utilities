package fao.org.rdfmgmt.vocabularies;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.vocabulary.VocabUtilities;

import java.util.HashSet;
import java.util.Set;

public class FAORGANIZATION extends RDFVocabulary {

	public static final String name = "FAORGANIZATION";
	public static final String baseURI = "http://aims.fao.org/ontologies/faorganization";
	public static final String defNamespace = baseURI + "#";

	// Classes 
	
	/** http://aims.fao.org/faorganization#Owner **/
	public static final String OWNER = defNamespace + "Owner";
	
	// properties	

	// xsd:string typed literals (fixed amount of values)
	public static final String SOURCE = FAORGANIZATION.defNamespace + "source";
	// plain literals in en/es/fr (but fixed amount of values)
	public static final String UNIT = FAORGANIZATION.defNamespace + "unit";
	// plain literals in en/es/fr (but fixed amount of values)
	public static final String LINKLAB = FAORGANIZATION.defNamespace + "linklab";
	// object property pointing to URIs of OWNERs
	public static final String OWNER_PROP = FAORGANIZATION.defNamespace + "owner";
	
	FAORGANIZATION() {
		super(name, baseURI, defNamespace);
	}
	
	public static class Res {

		public static HashSet<ARTURIResource> classes = new HashSet<ARTURIResource>();
	
		public static ARTURIResource OWNER;
		
		public static ARTURIResource SOURCE;
		public static ARTURIResource UNIT;
		public static ARTURIResource LINKLAB;
		public static ARTURIResource OWNER_PROP;
		
		static {
			try {
				initialize(VocabUtilities.nodeFactory);
			} catch (VocabularyInitializationException e) {
				e.printStackTrace(System.err);
			}
		}

		public static void initialize(ARTNodeFactory nodeFact) throws VocabularyInitializationException {

			// Classes

			OWNER = nodeFact.createURIResource(FAORGANIZATION.OWNER);

			classes.add(OWNER);
			
			// properties
			
			SOURCE = nodeFact.createURIResource(FAORGANIZATION.SOURCE);
			UNIT = nodeFact.createURIResource(FAORGANIZATION.UNIT);
			LINKLAB = nodeFact.createURIResource(FAORGANIZATION.LINKLAB);
			OWNER_PROP = nodeFact.createURIResource(FAORGANIZATION.OWNER_PROP);

		}		
		
		
		public static Set<ARTURIResource> getClasses() {
			return classes;
		}

		public static void printVocabulary() {
			// TODO
		}

	}
}

package fao.org.rdfmgmt.vocabularies;

public class LCSH extends SKOSVocabulary {

	public static final String name = "LCSH";
	// though concept URIs are in the form: <baseURI> + "/" + <conceptcode> + "#concept"
	public static final String baseURI = "http://id.loc.gov/authorities";
	public static final String defNamespace = baseURI + "#";
	public static final String defaultScheme = defNamespace + "conceptScheme";
	
	LCSH() {
		super(name, baseURI, defNamespace, defaultScheme);
	}
}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package fao.org.rdfmgmt.vocabularies;

import java.util.HashSet;
import java.util.Set;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.vocabulary.VocabUtilities;

/**
 * Vocabulary file for the DCMI Metadata Terms IMCOMPLETE!!!
 * 
 * @author Armando Stellato
 * 
 */
public class DCMIMetadataTerms {

	/** http://purl.org/dc/terms/ **/
	public static final String NAMESPACE = "http://purl.org/dc/terms/";

	// CLASSES

	/** http://purl.org/dc/terms/Agent **/
	public static final String AGENT = NAMESPACE + "Agent";

	// PROPERTIES

	// start of date properties
	
	/** http://purl.org/dc/terms/date **/
	public static final String DATE = NAMESPACE + "date";
	
	/** http://purl.org/dc/terms/created **/
	public static final String CREATED = NAMESPACE + "created";
	
	/** http://purl.org/dc/terms/modified **/
	public static final String MODIFIED = NAMESPACE + "modified";
	
	/** http://purl.org/dc/terms/issued **/
	public static final String ISSUED = NAMESPACE + "issued";

	// end of date properties
	
	/** http://purl.org/dc/terms/title **/
	public static final String TITLE = NAMESPACE + "title";

	/** http://purl.org/dc/terms/publisher **/
	public static final String PUBLISHER = NAMESPACE + "publisher";
	
	public static final String SOURCE = NAMESPACE + "source";

	public static class Res {

		public static HashSet<ARTURIResource> classes = new HashSet<ARTURIResource>();

		// classes

		public static ARTURIResource AGENT;

		// properties

		public static ARTURIResource TITLE;
		public static ARTURIResource PUBLISHER;
		public static ARTURIResource DATE;
		public static ARTURIResource CREATED;
		public static ARTURIResource MODIFIED;
		public static ARTURIResource ISSUED;
		public static ARTURIResource SOURCE;

		static {
			try {
				initialize(VocabUtilities.nodeFactory);
			} catch (VocabularyInitializationException e) {
				e.printStackTrace(System.err);
			}
		}

		public static void initialize(ARTNodeFactory nodeFact) throws VocabularyInitializationException {

			AGENT = nodeFact.createURIResource(DCMIMetadataTerms.AGENT);

			// properties
			TITLE = nodeFact.createURIResource(DCMIMetadataTerms.TITLE);
			PUBLISHER = nodeFact.createURIResource(DCMIMetadataTerms.PUBLISHER);
			DATE = nodeFact.createURIResource(DCMIMetadataTerms.DATE);
			CREATED = nodeFact.createURIResource(DCMIMetadataTerms.CREATED);
			MODIFIED = nodeFact.createURIResource(DCMIMetadataTerms.MODIFIED);
			
			ISSUED = nodeFact.createURIResource(DCMIMetadataTerms.ISSUED);
			
			SOURCE = nodeFact.createURIResource(DCMIMetadataTerms.SOURCE);

			// classes list initialization
			classes.add(AGENT);

		}

		public static Set<ARTURIResource> getClasses() {
			return classes;
		}

		public static void printVocabulary() {
			System.err.println("AGENT                      : " + AGENT);
			// ...
		}

	}

}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package fao.org.rdfmgmt.vocabularies;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.vocabulary.VocabUtilities;

import java.util.HashSet;
import java.util.Set;

/**
 * Vocabulary file for the Agrontology, driving AGROVOC SKOS vocabulary
 * 
 * @author Armando Stellato
 * 
 */
public class VOCBENCH {

	/** http://aims.fao.org/aos/agrontology# **/
	public static final String NAMESPACE = "http://art.uniroma2.it/ontologies/vocbench#";
	
	// properties

	/** #hasStatus **/
	public static final String hasStatus = NAMESPACE + "hasStatus";

	public static final String hasLink = NAMESPACE + "hasLink";
	
	public static final String hasSource = NAMESPACE + "hasSource";


	public static class Res {

		public static HashSet<ARTURIResource> classes = new HashSet<ARTURIResource>();

		// properties

		public static ARTURIResource hasStatus;
		public static ARTURIResource hasLink;
		public static ARTURIResource hasSource;

		
		static {
			try {
				initialize(VocabUtilities.nodeFactory);
			} catch (VocabularyInitializationException e) {
				e.printStackTrace(System.err);
			}
		}

		public static void initialize(ARTNodeFactory nodeFact) throws VocabularyInitializationException {



			// properties
			hasStatus = nodeFact.createURIResource(VOCBENCH.hasStatus);
			hasLink = nodeFact.createURIResource(VOCBENCH.hasLink);	
			hasSource = nodeFact.createURIResource(VOCBENCH.hasSource);	


		}

		public static Set<ARTURIResource> getClasses() {
			return classes;
		}

		public static void printVocabulary() {
			System.err.println("hasStatus                    : " + hasStatus);
			// ...
		}

	}

}

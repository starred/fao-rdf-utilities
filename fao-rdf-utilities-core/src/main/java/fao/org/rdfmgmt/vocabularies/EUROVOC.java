package fao.org.rdfmgmt.vocabularies;

public class EUROVOC extends SKOSVocabulary {

	public static final String name = "EUROVOC";
	public static final String baseURI = "http://eurovoc.europa.eu";
	public static final String defNamespace = baseURI + "/";
	// the defaultScheme has changed. Last version to use the old defaultScheme is agrovoc_wb_20110223_v_1_3
	// (telugu terms edition)
	public static final String defaultScheme = baseURI;

	EUROVOC() {
		super(name, baseURI, defNamespace, defaultScheme);
	}
}

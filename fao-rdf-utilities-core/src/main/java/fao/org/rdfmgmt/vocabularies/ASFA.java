package fao.org.rdfmgmt.vocabularies;

public class ASFA extends SKOSVocabulary {

	public static final String name = "ASFA";
	public static final String baseURI = "http://aims.fao.org/aos/asfa";
	public static final String defNamespace = baseURI + "/";
	public static final String defaultScheme = baseURI;
	
	ASFA() {
		super(name, baseURI, defNamespace, defaultScheme);
	}
}

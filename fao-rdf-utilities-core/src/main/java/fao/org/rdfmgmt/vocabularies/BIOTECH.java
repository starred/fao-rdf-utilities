package fao.org.rdfmgmt.vocabularies;

public class BIOTECH extends SKOSVocabulary {

	public static final String name = "BIOTECH";
	public static final String baseURI = "http://aims.fao.org/aos/biotechglossary";
	public static final String defNamespace = baseURI + "/";
	public static final String defaultScheme = defNamespace + "mainScheme";
	
	BIOTECH() {
		super(name, baseURI, defNamespace, defaultScheme);
	}
}

package fao.org.rdfmgmt.vocabularies;

public abstract class SKOSVocabulary extends RDFVocabulary{

	protected String defaultScheme;
	
	protected SKOSVocabulary(String name, String baseURI, String defNamespace, String defaultScheme) {
		super(name, baseURI, defNamespace);
		this.defaultScheme = defaultScheme;
	}

	public String getDefaultScheme() {
		return defaultScheme;
	}
	
	public String toString() {
		return super.toString()+" | " + defaultScheme;
	}
}

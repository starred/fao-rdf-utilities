package fao.org.rdfmgmt.vocabularies;

public class AUTHCON_CORPORATEBODIES extends SKOSVocabulary {

	public static final String name = "AUTHCON_CORPORATEBODIES";
	public static final String baseURI = "http://aims.fao.org/aos/corporate";
	public static final String defNamespace = baseURI + "/";
	public static final String defaultScheme = defNamespace + "mainScheme";
	
	AUTHCON_CORPORATEBODIES() {
		super(name, baseURI, defNamespace, defaultScheme);
	}
}

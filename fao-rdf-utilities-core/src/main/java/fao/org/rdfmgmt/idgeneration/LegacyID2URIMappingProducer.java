package fao.org.rdfmgmt.idgeneration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * this abstract class is an utility for producing maps between legacy ID coming from old formats for existing
 * FAO vocabularies and generated URIs. It can also be launched on existing mappings, and in that case, it
 * will keep original mappings and generate new URIs and produce new mappings only for new found IDs from the
 * legacy source</br> Implementations of this class should provide the readers for specific legacy formats
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public abstract class LegacyID2URIMappingProducer {

	private String comment;
	protected Properties properties = null;
	private File propFile = null;

	protected static Logger logger = LoggerFactory.getLogger(LegacyID2URIMappingProducer.class);

	/**
	 * @param mapperLongName
	 *            just a label for describing this mapping, which will be reported in the output file (a java
	 *            property file)
	 * @param propFile
	 *            the mappings file where old mappings are available. This file will be loaded to get the old
	 *            mappings, and it will be then updated with the new set of mappings (UNION of old and new
	 *            ones)
	 * @throws IOException
	 */
	public LegacyID2URIMappingProducer(String mapperLongName, File propFile) throws IOException {
		comment = mapperLongName;
		this.propFile = propFile;
		properties = new Properties();
		properties.load(new FileInputStream(propFile));
	}

	protected void updatePropertyFile() throws IOException {
		FileOutputStream os;
		os = new FileOutputStream(propFile);
		properties.store(os, comment);
		os.close();
	}

	public abstract String createURI(String legacyID);

	public void createMappings(LegacyDataLoader dataLoader, String dataSource) throws IOException {
		dataLoader.loadData(dataSource);
		while (dataLoader.hasNext()) {
			String legacyID = dataLoader.next();
			if (properties.getProperty(legacyID) == null) {
				String createdURI = null;
				// this loop assures that the newly created URI is not already existing by accident
				do {
					createdURI = createURI(legacyID);
				} while (properties.containsValue(createdURI));
				storeURI(legacyID, createdURI);
				logger.debug("created new URI: " + createdURI);
				properties.setProperty(legacyID.toString(), createdURI);
			} else
				logger.debug("legacy ID: " + legacyID + " was already mapped");
		}
		updatePropertyFile();
	}

	public abstract void storeURI(String legacyID, String newID);

	/**
	 * this abstract class represent a dataLoader for legacy codes.<br/>
	 * Codes can be implemented as arbitrary POJOs, however they need to be serialized to strings to be stored
	 * in the mapping file
	 * 
	 * @author Armando Stellato <stellato@info.uniroma2.it>
	 * 
	 */
	public static interface LegacyDataLoader extends Iterator<String> {

		public abstract void loadData(String sourceSpecification) throws IOException;

		/**
		 * resets the iterator
		 */
		public abstract void reset();

	}

}

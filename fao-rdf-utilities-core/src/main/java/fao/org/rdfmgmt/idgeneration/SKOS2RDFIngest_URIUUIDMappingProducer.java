package fao.org.rdfmgmt.idgeneration;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTLiteralIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.RDFS;
import it.uniroma2.art.owlart.vocabulary.SKOS;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Developed for TechCDR, this exporter generates a folder containing some XML files which drive the ingestion
 * process for data.fao.org, and a subfolder containing one rdf file per concept, with the RDF description of
 * each concept
 * 
 * @author Armando Stellato <a href="mailto:stellato@info.uniroma2.it">stellato@info.uniroma2.it</a>
 * 
 */
public class SKOS2RDFIngest_URIUUIDMappingProducer extends LegacyID2URIMappingProducer {

	protected static Logger logger = LoggerFactory.getLogger(SKOS2RDFIngest_URIUUIDMappingProducer.class);

	protected Document comDoc;
	protected Element commandsRootElem;
	protected SKOSXLModel model;
	protected File ingestionFilesDirectory;
	protected File commandsFileDirectory;
	HashSet<String> languages;
	protected ARTURIResource fdp_name; // http://data.fao.org/1.0/catalog/properties#name
	protected ARTURIResource fdp_group; // http://data.fao.org/1.0/catalog/properties#group
	protected ARTURIResource fdp_database; // http://data.fao.org/1.0/catalog/properties#database
	protected ARTURIResource cat_concept; // http://data.fao.org/1.0/catalog/types#Concept
	protected ARTURIResource urn_public; // urn:data.fao.org:group:public
	protected ARTURIResource agrovoc_uri; // http://ref.data.fao.org/bee6e167-c10f-4c02-8efd-754bd9d02882

	public SKOS2RDFIngest_URIUUIDMappingProducer(SKOSXLModel model, File propFile, File ingestionDirectory)
			throws IOException, ParserConfigurationException {
		super("SKOS URI/UUID Mapper for RDF Ingestion File", propFile);
		this.model = model;
		this.commandsFileDirectory = ingestionDirectory;
		this.ingestionFilesDirectory = new File(ingestionDirectory, "files");

		fdp_name = model.createURIResource("http://data.fao.org/1.0/catalog/properties#name");
		fdp_group = model.createURIResource("http://data.fao.org/1.0/catalog/properties#group");
		fdp_database = model.createURIResource("http://data.fao.org/1.0/catalog/properties#database");
		cat_concept = model.createURIResource("http://data.fao.org/1.0/catalog/types#Concept");
		urn_public = model.createURIResource("urn:faodata:group:public");
		agrovoc_uri = model.createURIResource("http://ref.data.fao.org/bee6e167-c10f-4c02-8efd-754bd9d02882");

		languages = new HashSet<String>();
		languages.add("en");
		languages.add("fr");
		languages.add("zh");
		languages.add("ru");
		languages.add("es");
		languages.add("ar");

		// Arabic, Spanish, English, French, Chinese, Russian

		comDoc = buildCommandFile();
		commandsRootElem = comDoc.createElement("commands");
		comDoc.appendChild(commandsRootElem);

		// mime type definition
		Element mimeTypeElem = comDoc.createElement("set-default-attr");
		commandsRootElem.appendChild(mimeTypeElem);
		mimeTypeElem.setAttribute("target-element", "c-res");
		mimeTypeElem.setAttribute("attr-name", "rdf-mimeType");
		mimeTypeElem.setAttribute("value", "application/rdf+xml");

		// replace-if-exists command
		Element replaceIfExistsElem = comDoc.createElement("set-default-attr");
		commandsRootElem.appendChild(replaceIfExistsElem);
		replaceIfExistsElem.setAttribute("target-element", "c-res");
		replaceIfExistsElem.setAttribute("attr-name", "replace-if-exists");
		replaceIfExistsElem.setAttribute("value", "true");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fao.org.owl2skos.conversion.LegacyID2URIMappingProducer#createURI(java.lang.String)
	 * 
	 * here the legacyID is actually the URI of the resource, and a new UUID is associated to it UUID creation
	 * ignores the original legacyID (in this case an URI). Simply, a new code is generated
	 */
	public String createURI(String legacyID) {
		// In this implementation, actually URIs are the legacy IDs, and UUIDs are the new "URIs" to be
		// generated. These new UUIDs will serve as local names for the new set of URIs in the next
		// major releases of AGROVOC, VOCBENCH and JAD
		return UUID.randomUUID().toString();
	}

	public void storeURI(String legacyID, String newID) {
		ARTURIResource concept = model.createURIResource(legacyID);
		File conceptIngestionFile = new File(ingestionFilesDirectory, concept.getLocalName() + ".rdf");

		ArrayList<ARTStatement> conceptDescription = new ArrayList<ARTStatement>();
		try {
			ARTLiteralIterator it = model.listPrefLabels(concept, false);
			while (it.streamOpen()) {
				ARTLiteral lbl = it.getNext();
				if (languages.contains(lbl.getLanguage())) {
					conceptDescription.add(model.createStatement(concept, RDFS.Res.LABEL, lbl));
				}
			}
			it.close();

			it = model.listAltLabels(concept, false);
			while (it.streamOpen()) {
				ARTLiteral lbl = it.getNext();
				if (languages.contains(lbl.getLanguage())) {
					conceptDescription.add(model.createStatement(concept, SKOS.Res.ALTLABEL, lbl));
				}
			}
			it.close();

			// there must be at least one label

			if (conceptDescription.size() > 0) {

				conceptDescription.add(model.createStatement(concept, RDF.Res.TYPE, cat_concept));
				conceptDescription.add(model.createStatement(concept, fdp_name,
						model.createLiteral(concept.getLocalName().replace("_", "-"))));
				conceptDescription.add(model.createStatement(concept, fdp_group, urn_public));
				conceptDescription.add(model.createStatement(concept, fdp_database, agrovoc_uri));

				// System.err.println("concept description:\n" + conceptDescription);

				FileWriter fw = new FileWriter(conceptIngestionFile);
				model.writeRDF(RDFIterators.createARTStatementIterator(conceptDescription.iterator()),
						RDFFormat.RDFXML_ABBREV, fw);

				fw.close();

				Element cresElem = comDoc.createElement("c-res");
				commandsRootElem.appendChild(cresElem);
				cresElem.setAttribute("src", "zip:///files/" + conceptIngestionFile.getName());
			}

		} catch (ModelAccessException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} catch (UnsupportedRDFFormatException e) {
			throw new RuntimeException(e);
		}
	}

	protected String getLocalName(String uri) {
		if (uri.contains("#"))
			return uri.substring(uri.lastIndexOf("#") + 1);
		else {
			return uri.substring(uri.lastIndexOf("/") + 1);
		}
	}

	public static void main(String args[]) throws Exception {
		if (args.length < 4) {
			System.out.println("usage:\n" + SKOS2RDFIngest_URIUUIDMappingProducer.class.getName()
					+ " <configFile> <skosFile> <mappingsFile> <ingestionDirectory>");
			return;
		}

		String configFile = args[0];
		String skosFile = args[1];
		String mappingsFile = args[2];
		String ingestionDirectory = args[3];

		SKOSDataLoader dataLoader = new SKOSDataLoader(configFile);

		SKOS2RDFIngest_URIUUIDMappingProducer mappingProducer = new SKOS2RDFIngest_URIUUIDMappingProducer(
				dataLoader.getModel(), new File(mappingsFile), new File(ingestionDirectory));
		logger.info("SKOS URI/UUID mapper initialized");
		mappingProducer.createMappings(dataLoader, skosFile);
		logger.info("SKOS URI/UUID mappings generated");
		mappingProducer.saveCommandsFile();
		logger.info("XML ingestion file generated");

	}

	public static class SKOSDataLoader implements LegacyDataLoader {

		ARTURIResourceIterator it;

		SKOSXLModel skosModel;

		public SKOSDataLoader(String configFile) throws IOException, ModelCreationException,
				BadConfigurationException {
			skosModel = ModelLoader.loadModel(configFile, SKOSXLModel.class);
		}

		public void loadData(String skosFilePath) throws IOException {

			File skosFile = new File(skosFilePath);
			try {
				skosModel.addRDF(skosFile, null, RDFFormat.guessRDFFormatFromFile(skosFile));
				it = skosModel.listConceptsInScheme(NodeFilters.ANY);
			} catch (ModelAccessException e) {
				throw new IOException(e);
			} catch (ModelUpdateException e) {
				throw new IOException(e);
			} catch (UnsupportedRDFFormatException e) {
				throw new IOException(e);
			}

		}

		public boolean hasNext() {
			return it.hasNext();
		}

		public String next() {
			return it.next().toString();
		}

		public void remove() {
			it.remove();
		}

		public void reset() {
			try {
				it = skosModel.listConceptsInScheme(NodeFilters.ANY);
			} catch (ModelAccessException e) {
				throw new IllegalAccessError(e.getMessage());
			}
		}

		public SKOSXLModel getModel() {
			return skosModel;
		}

	}

	protected Document buildCommandFile() throws ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		DOMImplementation _DOMImpl = db.getDOMImplementation();
		return _DOMImpl.createDocument(null, null, null);
	}

	public void saveCommandsFile() throws FileNotFoundException, TransformerConfigurationException {
		File ingestionFile = new File(commandsFileDirectory, "commands.xml");

		FileOutputStream os = new FileOutputStream(ingestionFile);

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer identityTransformer = transformerFactory.newTransformer();
		Properties outputProps = new Properties();
		outputProps.setProperty("encoding", "UTF-8");
		outputProps.setProperty("indent", "yes");
		outputProps.setProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		identityTransformer.setOutputProperties(outputProps);

		DOMSource domSource = new DOMSource(comDoc);
		StreamResult streamResult = new StreamResult(os);
		try {
			identityTransformer.transform(domSource, streamResult);
		} catch (TransformerException e) {
			// don't want to have this method throw an exception, so, considering that exception
			// should never happen...
			// ...at least I hope...
			e.printStackTrace();
		}
	}

}

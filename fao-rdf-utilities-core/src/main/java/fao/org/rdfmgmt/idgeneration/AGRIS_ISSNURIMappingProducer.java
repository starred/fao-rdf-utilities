package fao.org.rdfmgmt.idgeneration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fao.org.rdfmgmt.vocabularies.JAD;

public class AGRIS_ISSNURIMappingProducer extends LegacyID2URIMappingProducer {

	protected static Logger logger = LoggerFactory.getLogger(AGRIS_ISSNURIMappingProducer.class);

	public AGRIS_ISSNURIMappingProducer(File propFile) throws IOException {
		super("AGRIS ISSN/URI Mapper", propFile);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fao.org.owl2skos.conversion.LegacyID2URIMappingProducer#createURI(java.lang.String)
	 * 
	 * URI creation ignores the original legacyID. Simply, a new code is generated
	 */
	public String createURI(String legacyID) {
		UUID code = UUID.randomUUID();
		return JAD.defNamespace + "c_" + code.toString().split("-")[0];
	}
	
	public void storeURI(String legacyID, String newID) {
		// do nothing
	}

	public static void main(String args[]) throws IOException {
		if (args.length < 2) {
			System.out.println("usage:\n" + AGRIS_ISSNURIMappingProducer.class.getName()
					+ " <agrisISSNFile> <mappingsFile>");
			return;
		}

		String agrisFile = args[0];
		String mappingsFile = args[1];

		String agrisFileExtension = (agrisFile.lastIndexOf(".") == -1) ? "" : agrisFile.substring(
				agrisFile.lastIndexOf(".") + 1, agrisFile.length());

		LegacyDataLoader dataLoader;

		if (agrisFileExtension.equalsIgnoreCase("xml")) {
			System.out.println("format of source data for ISSN is xml, loading XML Legacy Data Loader");
			dataLoader = new AGRISXMLDataLoader();
		} else if (agrisFileExtension.equalsIgnoreCase("txt")) {
			System.out.println("format of source data for ISSN is txt, loading TXT Legacy Data Loader");
			dataLoader = new AGRISTXTDataLoader();
		} else {
			System.out.println("unknown format: " + agrisFileExtension + " for file: " + agrisFile
					+ "\nit must be either xml or txt");
			return;
		}

		AGRIS_ISSNURIMappingProducer mappingProducer = new AGRIS_ISSNURIMappingProducer(
				new File(mappingsFile));

		logger.info("AGRIS ISSN/URI mapper initialized");
		mappingProducer.createMappings(dataLoader, agrisFile);
		logger.info("AGRIS ISSN/URI mappings created");

	}

	public static class AGRISTXTDataLoader implements LegacyDataLoader {

		HashSet<String> issnSet;
		Iterator<String> it;

		public void loadData(String filePath) throws IOException {

			issnSet = new HashSet<String>();

			BufferedReader br = new BufferedReader(new FileReader(filePath));
			String line;
			String issn;
			while ((line = br.readLine()) != null) {
				issn = line.trim();
				issnSet.add(issn);
				logger.debug("parsed issn : " + issn);
			}
			it = issnSet.iterator();
		}

		public boolean hasNext() {
			return it.hasNext();
		}

		public String next() {
			return it.next();
		}

		public void remove() {
			it.remove();
		}

		public void reset() {
			it = issnSet.iterator();
		}

	}

	public static class AGRISXMLDataLoader implements LegacyDataLoader {

		HashSet<String> issnSet;
		Iterator<String> it;

		public void loadData(String filePath) throws IOException {
			issnSet = new HashSet<String>();
			File file = new File(filePath);
			try {
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				DocumentBuilder db;

				db = dbf.newDocumentBuilder();
				Document doc = db.parse(file);
				doc.getDocumentElement().normalize();
				NodeList nodeLst = doc.getElementsByTagName("Journal");

				for (int s = 0; s < nodeLst.getLength(); s++) {

					Node journalNode = nodeLst.item(s);

					if (journalNode.getNodeType() == Node.ELEMENT_NODE) {

						Element journalElmnt = (Element) journalNode;
						Element IssnElmnt = (Element) journalElmnt.getElementsByTagName("Issn").item(0);
						String issn = IssnElmnt.getChildNodes().item(0).getNodeValue();
						issnSet.add(issn);
						logger.debug("parsed issn : " + issn);
					}
				}
			} catch (ParserConfigurationException e) {
				throw new IOException(e);
			} catch (SAXException e) {
				throw new IOException(e);
			}
			it = issnSet.iterator();
		}

		public boolean hasNext() {
			return it.hasNext();
		}

		public String next() {
			return it.next();
		}

		public void remove() {
			it.remove();
		}

		public void reset() {
			it = issnSet.iterator();
		}

	}



}

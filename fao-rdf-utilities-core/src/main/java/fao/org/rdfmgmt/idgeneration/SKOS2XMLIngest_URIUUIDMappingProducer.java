package fao.org.rdfmgmt.idgeneration;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class SKOS2XMLIngest_URIUUIDMappingProducer extends LegacyID2URIMappingProducer {

	protected static Logger logger = LoggerFactory.getLogger(SKOS2XMLIngest_URIUUIDMappingProducer.class);

	protected Document ingDoc;
	protected Element ingDocRoot;

	public SKOS2XMLIngest_URIUUIDMappingProducer(File propFile) throws IOException, ParserConfigurationException {
		super("SKOS URI/UUID Mapper", propFile);
		ingDoc = buildXMLIngestionFile();
		Element ingestElement = ingDoc.createElement("ingest");
		ingDoc.appendChild(ingestElement);
		ingDocRoot = ingDoc.createElement("create-resources");
		ingestElement.appendChild(ingDocRoot);
		ingDocRoot.setAttribute("workspace", properties.getProperty("workspace"));
		ingDocRoot.setAttribute("default-resource-owner", properties.getProperty("default-resource-owner"));
		ingDocRoot.setAttribute("default-collection", "terms");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fao.org.owl2skos.conversion.LegacyID2URIMappingProducer#createURI(java.lang.String)
	 * 
	 * here the legacyID is actually the URI of the resource, and a new UUID is associated to it
	 * UUID creation ignores the original legacyID (in this case an URI). Simply, a new code is generated
	 */
	public String createURI(String legacyID) {
		// In this implementation, actually URIs are the legacy IDs, and UUIDs are the new "URIs" to be
		// generated. These new UUIDs will serve as local names for the new set of URIs in the next
		// major releases of AGROVOC, VOCBENCH and JAD

		return UUID.randomUUID().toString();
	}	

	public void storeURI(String legacyID, String newID) {
		// part related to production of differential IngestionFile
		Element newConcept = ingDoc.createElement("res");
		ingDocRoot.appendChild(newConcept);
		newConcept.setAttribute("name", getLocalName(legacyID));
		newConcept.setAttribute("UUID", newID);

		Element dsElement = ingDoc.createElement("ds");
		dsElement.setAttribute("type", "rdf");
		dsElement.setAttribute("ref", legacyID);
		dsElement.setAttribute("mime-type", "application/xml"); // should it not be application/rdf+xml?
		dsElement.setAttribute("class", "XML");
		newConcept.appendChild(dsElement);
	}
	
	protected String getLocalName(String uri) {
		if (uri.contains("#"))			
			return uri.substring(uri.lastIndexOf("#")+1);
		else {
			return uri.substring(uri.lastIndexOf("/")+1);
		}
	}

	public static void main(String args[]) throws Exception {
		if (args.length < 4) {
			System.out.println("usage:\n" + SKOS2XMLIngest_URIUUIDMappingProducer.class.getName()
					+ " <configFile> <skosFile> <mappingsFile> <ingestionFile>");
			return;
		}

		String configFile = args[0];
		String skosFile = args[1];
		String mappingsFile = args[2];
		String ingestionFile = args[3];

		SKOS2XMLIngest_URIUUIDMappingProducer mappingProducer = new SKOS2XMLIngest_URIUUIDMappingProducer(new File(mappingsFile));

		logger.info("SKOS URI/UUID mapper initialized");
		mappingProducer.createMappings(new SKOSDataLoader(configFile), skosFile);
		logger.info("SKOS URI/UUID mappings generated");
		mappingProducer.saveIngestionFile(ingestionFile);
		logger.info("XML ingestion file generated");

	}

	public static class SKOSDataLoader implements LegacyDataLoader {

		ARTURIResourceIterator it;

		SKOSXLModel model;

		public SKOSDataLoader(String configFile) throws IOException, ModelCreationException,
				BadConfigurationException {
			model = ModelLoader.loadModel(configFile, SKOSXLModel.class);
		}

		public void loadData(String skosFilePath) throws IOException {

			File skosFile = new File(skosFilePath);
			try {
				model.addRDF(skosFile, null, RDFFormat.guessRDFFormatFromFile(skosFile));
				it = model.listConceptsInScheme(NodeFilters.ANY);
			} catch (ModelAccessException e) {
				throw new IOException(e);
			} catch (ModelUpdateException e) {
				throw new IOException(e);
			} catch (UnsupportedRDFFormatException e) {
				throw new IOException(e);
			}

		}

		public boolean hasNext() {
			return it.hasNext();
		}

		public String next() {
			return it.next().toString();
		}

		public void remove() {
			it.remove();
		}

		public void reset() {
			try {
				it = model.listConceptsInScheme(NodeFilters.ANY);
			} catch (ModelAccessException e) {
				throw new IllegalAccessError(e.getMessage());
			}
		}

	}

	protected Document buildXMLIngestionFile() throws ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		DOMImplementation _DOMImpl = db.getDOMImplementation();
		return _DOMImpl.createDocument(null, null, null);
	}

	public void saveIngestionFile(String ingestionFilePath) throws FileNotFoundException,
			TransformerConfigurationException {
		File ingestionFile = new File(ingestionFilePath);

		FileOutputStream os = new FileOutputStream(ingestionFile);

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer identityTransformer = transformerFactory.newTransformer();
		Properties outputProps = new Properties();
		outputProps.setProperty("encoding", "UTF-8");
		outputProps.setProperty("indent", "yes");
		outputProps.setProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		identityTransformer.setOutputProperties(outputProps);

		DOMSource domSource = new DOMSource(ingDoc);
		StreamResult streamResult = new StreamResult(os);
		try {
			identityTransformer.transform(domSource, streamResult);
		} catch (TransformerException e) {
			// don't want to have this method throw an exception, so, considering that exception
			// should never happen...
			// ...at least I hope...
			e.printStackTrace();
		}
	}



}

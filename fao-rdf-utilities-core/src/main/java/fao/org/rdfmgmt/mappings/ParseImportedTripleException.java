package fao.org.rdfmgmt.mappings;

public class ParseImportedTripleException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1603927373422907049L;

	public ParseImportedTripleException(String parsedTriple) {
		super(parsedTriple);
	}
	
}

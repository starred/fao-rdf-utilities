package fao.org.rdfmgmt.mappings;

import java.io.File;
import java.io.IOException;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;

/**
 * This class exploits the {@link LoadMappingTriples} class by providing a concrete implementation
 * specifically configured for the (currently) existing mapping files from AGROVOC to other RDF resources
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class AGROVOCPrepareMappings {

	public static void main(String[] args) throws IOException, ModelCreationException,
			BadConfigurationException, ModelUpdateException, ModelAccessException,
			UnsupportedRDFFormatException {

		if (args.length < 2) {
			System.out.println("usage:\njava" + fao.org.rdfmgmt.mappings.LoadMappingTriples.class.getName()
					+ " <configFile> <outputfile> \n"
					+ "\t<configfile>  : config file for the chosen RDFModel Implementation \n"
					+ "\t<outputfile>  : file where mappings are being store");
			return;
		}

		String configFileName = args[0];
		String outputFileName = args[1];
 	
		// OWL ART SKOSXLMODEL LOADING
		RDFModel model = ModelLoader.loadRDFModel(configFileName);
		// skosXLModel.setDefaultNamespace(OWL2SKOSConverter.agrovocDefNamespace);
		try {
			// LoadMappingTriples.addTriples(model, new File("Input/MAPPINGS/Agrovoc_Eurovoc.txt"));
			// LoadMappingTriples.addTriples(model, new File("Input/MAPPINGS/Agrovoc_Gemet.txt"));
			// LoadMappingTriples.addTriples(model, new File("Input/MAPPINGS/Agrovoc_Lcsh.txt"));
			// LoadMappingTriples.addTriples(model, new File("Input/MAPPINGS/Agrovoc_Nal.txt"));
			// LoadMappingTriples.addTriples(model, new File("Input/MAPPINGS/Agrovoc_Stw.txt"));
			// LoadMappingTriples.addTriples(model, new File("Input/MAPPINGS/Agrovoc_TheSoz.txt"));
			LoadMappingTriples.addTriples(model, new File("Input/MAPPINGS/Agrovoc_DBpedia.txt"));
		} catch (ParseImportedTripleException e) {
			e.printStackTrace();
		}

		File mappingsFile = new File(outputFileName);

		model.writeRDF(mappingsFile, RDFFormat.guessRDFFormatFromFile(mappingsFile), NodeFilters.MAINGRAPH);

	}

}

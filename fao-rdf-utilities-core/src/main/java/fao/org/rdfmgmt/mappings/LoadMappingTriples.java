package fao.org.rdfmgmt.mappings;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * this class is mainly an utility for loading mappings in the format agreed with Ahsan Morshed and then
 * converting them into triples and loading them in a given RDF model
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class LoadMappingTriples {
	public static void main(String[] args) throws ModelCreationException,
			UnsupportedModelConfigurationException, ModelUpdateException, IOException,
			ClassNotFoundException, InstantiationException, IllegalAccessException,
			BadConfigurationException, UnloadableModelConfigurationException {

		if (args.length < 2) {
			System.out
					.println("usage:\n"
							+ "java fao.org.owl2skos.utilities.AddTriples <configfile>\n"
							+ "\t<configfile>  : config file for the chosen RDFModel Implementation \n"
							+ "\t<triplesfile>                      : file containing triples to be added to the rdf repository \n");
			return;
		}

		String configFile = args[0];
		String triplesFile = args[1];

		// OWL ART SKOSXLMODEL LOADING
		RDFModel model = ModelLoader.loadRDFModel(configFile);
		// skosXLModel.setDefaultNamespace(OWL2SKOSConverter.agrovocDefNamespace);
		try {
			addTriples(model, new File(triplesFile));
		} catch (ParseImportedTripleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void addTriples(RDFModel model, File triplesFile) throws IOException, ModelUpdateException,
			ParseImportedTripleException {
		BufferedReader br = new BufferedReader(new FileReader(triplesFile));
		int lineCounter = 0;
		String line;
		String[] triple = null;
		try {
			while ((line = br.readLine()) != null) {
				lineCounter++;
				if (!line.equals("")) {
					triple = line.split("\\|");
					model.addTriple(model.createURIResource(triple[0].trim()),
							model.createURIResource(triple[1].trim()),
							model.createURIResource(triple[2].trim()));
				}
			}
		} catch (IllegalArgumentException e) {
			StringBuffer tripleBuffer = new StringBuffer();
			for (String triplePart : triple) {
				tripleBuffer.append(triplePart);
				throw new ParseImportedTripleException("parsedTriple: " + tripleBuffer.toString()
						+ "at line:" + lineCounter + " of file: " + triplesFile);
			}
		}
	}

}

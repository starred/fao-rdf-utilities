package fao.org.rdfmgmt.mappings;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is meant to check that every mapping written for a given thesaurus is still referring to an
 * existing concept.<br/>
 * Note that this check is made only by considering the given thesaurus as containing concepts which are
 * subject (never object) of a mapping triple. No check is made on the concepts from the other thesaurus (the
 * one containing concepts mapped as objects of mapping triples.
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 */
public class MappingsMaintenanceEngine {

	public static final String sourceMappingsGraphURI = "http://source-mappings";
	public static final String processedMappingsGraphURI = "http://processed-mappings";
	public static final String discardedMappingsGraphURI = "http://discarded-mappings";
	
	protected static Logger logger = LoggerFactory.getLogger(MappingsMaintenanceEngine.class);
	
	public static void main(String[] args) throws IOException, ModelCreationException, BadConfigurationException, ModelAccessException, ModelUpdateException, UnsupportedRDFFormatException {

		if (args.length < 3) {
			System.out.println("usage:\njava " + MappingsMaintenanceEngine.class.getCanonicalName()
					+ " <configfile> <thesaurusfile> <mappingsfile>\n"
					+ "\t<configfile>    : config file for the chosen RDFModel Implementation \n"
					+ "\t<thesaurusfile> : thesaurus file whose mappings need to be maintained \n"
					+ "\t<mappingsfile>  : mappings which need to be maintained \n");
			;
			return;
		}

		String configFile = args[0];
		File thesaurusFile = new File(args[1]);
		File mappingsFile = new File(args[2]);

		RDFModel model = ModelLoader.loadRDFModel(configFile);
		
		ARTURIResource sourceMappingsGraph = model.createURIResource(sourceMappingsGraphURI);
		ARTURIResource processedMappingsGraph = model.createURIResource(processedMappingsGraphURI);
		ARTURIResource discardedMappingsGraph = model.createURIResource(discardedMappingsGraphURI);
		
		logger.info("loading thesaurus: " + thesaurusFile);
		model.addRDF(thesaurusFile, null, RDFFormat.guessRDFFormatFromFile(thesaurusFile), NodeFilters.MAINGRAPH);
		logger.info("loading mappings: " + mappingsFile);
		model.addRDF(mappingsFile, null, RDFFormat.guessRDFFormatFromFile(mappingsFile), sourceMappingsGraph);

		logger.info("processing mappings");
		ARTStatementIterator it = model.listStatements(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, false, sourceMappingsGraph);
		while (it.streamOpen()) {
			ARTStatement stat = it.getNext();
			if (model.existsResource(stat.getSubject(), NodeFilters.MAINGRAPH)) {
				model.addStatement(stat, processedMappingsGraph);
			} else {
				model.addStatement(stat, discardedMappingsGraph);
			}				
		}
		
		// String mappingsFilePathNameWithNoExtension = mappingsFile.getCanonicalPath().substring(0, mappingsFile.getCanonicalPath().lastIndexOf("."));
		File mappingFileParentDirectory = mappingsFile.getParentFile();		
		File outputKeptDir = new File(mappingFileParentDirectory, "kept"); 
		File outputDiscardedDir = new File(mappingFileParentDirectory, "discarded");
		if (!outputKeptDir.exists())
			outputKeptDir.mkdir();
		if (!outputDiscardedDir.exists())
			outputDiscardedDir.mkdir();
		
		logger.info("saving analyzed mappings to: " + outputKeptDir);
		model.writeRDF(new File(outputKeptDir, mappingsFile.getName()), RDFFormat.NTRIPLES, processedMappingsGraph);
		
		logger.info("saving discarded mappings to:" + outputDiscardedDir);
		model.writeRDF(new File(outputDiscardedDir, mappingsFile.getName()), RDFFormat.NTRIPLES, discardedMappingsGraph);
		
	}

}

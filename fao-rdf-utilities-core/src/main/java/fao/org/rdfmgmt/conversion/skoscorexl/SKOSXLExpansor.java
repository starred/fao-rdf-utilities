package fao.org.rdfmgmt.conversion.skoscorexl;

import fao.org.rdfmgmt.owlart.ModelLoader;
import fao.org.rdfmgmt.utilities.FormatConverter;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.utilities.transform.SKOS2SKOSXLConverter;

import java.io.File;
import java.io.IOException;

public class SKOSXLExpansor {

	/**
	 * this tool is just a wrapper around the skos core2xl transformation utility of OWLART
	 * 
	 * @param args
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ModelCreationException
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 * @throws UnsupportedRDFFormatException
	 * @throws BadConfigurationException
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException, ClassNotFoundException,
			InstantiationException, IllegalAccessException, ModelCreationException, ModelUpdateException,
			ModelAccessException, UnsupportedRDFFormatException, BadConfigurationException {
		if (args.length < 4) {
			System.out
					.println("usage:\n"
							+ SKOSXLExpansor.class.getName() + " <config> <inputfilename> <outputfilename> <xLabelsAsURIs>");
			return;
		}

		// OWL ART SKOSMODEL LOADING
				
		SKOSModel   sourceModel = ModelLoader.loadModel(args[0], SKOSModel.class);
		SKOSXLModel targetModel = ModelLoader.loadModel(args[0], SKOSXLModel.class);
		
		// LOADING PRODUCED DATA IN SOURCE SERIALIZATION FORMAT AND CONVERTING TO OUTPUT FORMAT

		File inputFile = new File(args[1]);
		File outputFile = new File(args[2]);
		sourceModel.addRDF(inputFile, sourceModel.getBaseURI(), RDFFormat.guessRDFFormatFromFile(inputFile));
		SKOS2SKOSXLConverter.convert(sourceModel, targetModel, Boolean.parseBoolean(args[3]));
		targetModel.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);
	}
	
}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is AGROVOC-owl2skos.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2010.
 * All Rights Reserved.
 *
 * AGROVOC-owl2skos was developed by the Artificial Intelligence Research Group
 * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about AGROVOC-owl2skos can be obtained at 
 * http//ai-nlp.info.uniroma2.it/software/...
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package fao.org.rdfmgmt.owlart;

import fao.org.rdfmgmt.vocabularies.RDFVocabulary;
import fao.org.rdfmgmt.vocabularies.SKOSVocabulary;
import fao.org.rdfmgmt.vocabularies.VocabularyFactory;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.models.ModelFactory;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * facility class for loading an empty model inside a given data directory
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 */
public class ModelLoader {

	protected static Logger logger = LoggerFactory.getLogger(ModelLoader.class);

	public static class ModelProps {
		public static final String modelFactoryImplClassNameProp = "modelFactoryImplClassName";
		public static final String modelConfigClassProp = "modelConfigClass";
		public static final String modelConfigFileProp = "modelConfigFile";
		public static final String modelDataDirProp = "modelDataDir";

		// user can either specifcy the vocabulary prop, in that case, information about baseuriProp,
		// namespaceProp and defaultSchemaProp is taken from the vocabulary, or specify the following three
		// properties individually
		public static final String vocabularyProp = "vocabulary";

		public static final String baseuriProp = "baseuri";
		public static final String namespaceProp = "namespace";
		public static final String defaultSchemeProp = "defaultScheme";

	}

	/**
	 * 
	 * 
	 * @param modelFactoryImplClassName
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	@SuppressWarnings("unchecked")
	public static OWLArtModelFactory<? extends ModelConfiguration> createModelFactory(
			String modelFactoryImplClassName) throws ClassNotFoundException, InstantiationException,
			IllegalAccessException {
		Class<? extends ModelFactory<? extends ModelConfiguration>> owlArtModelFactoryImplClass = (Class<? extends ModelFactory<? extends ModelConfiguration>>) Class
				.forName(modelFactoryImplClassName);
		OWLArtModelFactory fact = OWLArtModelFactory.createModelFactory(owlArtModelFactoryImplClass
				.newInstance());
		return fact;
	}

	@SuppressWarnings("unchecked")
	public static ModelConfiguration createModelConfiguration(OWLArtModelFactory fact,
			String modelConfigFile, String modelConfigClass) throws ClassNotFoundException,
			InstantiationException, IllegalAccessException, UnsupportedModelConfigurationException,
			UnloadableModelConfigurationException, BadConfigurationException, IOException {

		ModelConfiguration mcfg;

		if (modelConfigClass != null) {
			logger.debug("modelConfigClass=" + modelConfigClass);
			Class<? extends ModelConfiguration> mcfgClass = (Class<? extends ModelConfiguration>) Class
					.forName(modelConfigClass);
			mcfg = fact.createModelConfigurationObject(mcfgClass);
		} else {
			mcfg = fact.createModelConfigurationObject();
		}

		if (modelConfigFile != null)
			mcfg.loadParameters(new File(modelConfigFile));

		return mcfg;
	}

	public static RDFModel loadRDFModel(String configFile) throws IOException, ModelCreationException,
			BadConfigurationException {		
		return loadModel(configFile, RDFModel.class);
	}

	@SuppressWarnings("unchecked")
	public static <M extends RDFModel> M loadModel(String configFile, Class<M> modelClass)
			throws IOException, ModelCreationException, BadConfigurationException {
		File file = new File(configFile);
		Properties props = new Properties();
		FileReader fr = new FileReader(file);
		props.load(fr);
		fr.close();
		String modelFactoryImplClassName = props.getProperty(ModelProps.modelFactoryImplClassNameProp);
		String modelConfigClass = props.getProperty(ModelProps.modelConfigClassProp);
		String modelConfigFile = props.getProperty(ModelProps.modelConfigFileProp);
		String vocabulary = props.getProperty(ModelProps.vocabularyProp);
		String modelDataDir = props.getProperty((ModelProps.modelDataDirProp));

		String baseuri;
		String namespace;
		String defaultScheme = null;

		String errMsg = "badConfiguration in file " + configFile + ": missing parameter: ";

		if (modelFactoryImplClassName == null) {
			throw new BadConfigurationException(errMsg + ModelProps.modelFactoryImplClassNameProp);
		}

		if (modelDataDir == null) {
			throw new BadConfigurationException(errMsg + ModelProps.modelDataDirProp);
		}

		if (vocabulary != null) {
			RDFVocabulary vocab = VocabularyFactory.getVocabulary(vocabulary);
			if (vocab==null)
				throw new BadConfigurationException("vocabulary: " + vocabulary + " is not registered in the vocabulary register");
			baseuri = vocab.getBaseURI();
			namespace = vocab.getDefNamespace();
			if (vocab instanceof SKOSVocabulary)
				defaultScheme = ((SKOSVocabulary) vocab).getDefaultScheme();
		} else {
			baseuri = props.getProperty((ModelProps.baseuriProp));
			namespace = props.getProperty((ModelProps.namespaceProp));
			defaultScheme = props.getProperty((ModelProps.defaultSchemeProp));
		}

		if (baseuri == null) {
			throw new BadConfigurationException(errMsg + ModelProps.baseuriProp);
		}

		if (namespace == null) {
			throw new BadConfigurationException(errMsg + ModelProps.namespaceProp);
		}

		if (SKOSModel.class.isAssignableFrom(modelClass)) {
			if (defaultScheme == null)
				throw new BadConfigurationException(errMsg + ModelProps.defaultSchemeProp);
			return (M) loadSKOSXLModel(modelFactoryImplClassName, baseuri, namespace, defaultScheme,
					modelDataDir, modelConfigFile, modelConfigClass);
		} else
			return (M) loadOWLModel(modelFactoryImplClassName, baseuri, namespace, modelDataDir,
					modelConfigFile, modelConfigClass);

	}

	/**
	 * @param modelFactoryImplClassName
	 * @param baseuri
	 * @param defaultNamespace
	 * @param defaultScheme
	 * @param modelDataDir
	 * @param modelConfigFile
	 * @param modelConfigClass
	 * @return
	 * @throws ModelCreationException
	 */
	@SuppressWarnings("unchecked")
	public static SKOSXLModel loadSKOSXLModel(String modelFactoryImplClassName, String baseuri,
			String defaultNamespace, String defaultScheme, String modelDataDir, String modelConfigFile,
			String modelConfigClass) throws ModelCreationException {

		try {
			OWLArtModelFactory fact = createModelFactory(modelFactoryImplClassName);
			ModelConfiguration mcfg = createModelConfiguration(fact, modelConfigFile, modelConfigClass);
			SKOSXLModel skosXLModel = fact.loadSKOSXLModel(baseuri, modelDataDir, mcfg);
			skosXLModel.setDefaultNamespace(defaultNamespace);
			skosXLModel.setDefaultScheme(skosXLModel.createURIResource(defaultScheme));
			return skosXLModel;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ModelCreationException(e);
		}
	}

	/**
	 * @param modelFactoryImplClassName
	 * @param baseuri
	 * @param defaultNamespace
	 * @param modelDataDir
	 * @param modelConfigFile
	 * @param modelConfigClass
	 * @return
	 * @throws ModelCreationException
	 */
	@SuppressWarnings("unchecked")
	public static OWLModel loadOWLModel(String modelFactoryImplClassName, String baseuri,
			String defaultNamespace, String modelDataDir, String modelConfigFile, String modelConfigClass)
			throws ModelCreationException {

		OWLArtModelFactory fact;
		try {
			fact = createModelFactory(modelFactoryImplClassName);
			ModelConfiguration mcfg = createModelConfiguration(fact, modelConfigFile, modelConfigClass);
			OWLModel owlModel = fact.loadOWLModel(baseuri, modelDataDir, mcfg);
			owlModel.setDefaultNamespace(defaultNamespace);
			return owlModel;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ModelCreationException(e);
		}
	}

}

package fao.org.rdfmgmt.fix;

import fao.org.rdfmgmt.owlart.ModelLoader;
import fao.org.rdfmgmt.utilities.FormatConverter;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.utilities.fix.SKOSFixing;

import java.io.File;
import java.io.IOException;

public class FixSKOSConceptsAndSchemes {

	public static void main(String[] args) throws IOException, ClassNotFoundException,
			InstantiationException, IllegalAccessException, ModelCreationException, ModelUpdateException,
			ModelAccessException, UnsupportedRDFFormatException, BadConfigurationException {
		if (args.length < 4) {
			System.out.println("usage:\n" + FormatConverter.class.getName()
					+ " <config> <schemeURI> <inputFileName> <outputfilename>");
			return;
		}

		// OWL ART SKOSMODEL LOADING

		SKOSModel model = ModelLoader.loadModel(args[0], SKOSModel.class);

		ARTURIResource scheme = model.createURIResource(args[1]);
		
		// LOADING PRODUCED DATA IN SOURCE SERIALIZATION FORMAT AND CONVERTING TO OUTPUT FORMAT

		File inputFile = new File(args[2]);
		File outputFile = new File(args[3]);
		model.addRDF(inputFile, model.getBaseURI(), RDFFormat.guessRDFFormatFromFile(inputFile));
		SKOSFixing.attachAllConceptsToScheme(model, scheme);
		model.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);
	}

}

package fao.org.rdfmgmt.stats;

import fao.org.rdfmgmt.utilities.RDFLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFNodeSerializer;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.TupleBindings;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;

import java.io.IOException;

/**
 * this class helps in producing VoID stats<br/>
 * see also: <a
 * href="http://code.google.com/p/void-impl/wiki/SPARQLQueriesForStatistics">http://code.google.com
 * /p/void-impl/wiki/SPARQLQueriesForStatistics</a>
 * 
 * @author Armando Stellato <a href="mailto:stellato@uniroma2.it">stellato@uniroma2.it</a>
 * 
 */
public class VoIDStatsProducer {

	RDFModel model;

	public VoIDStatsProducer(String configFile, String subjectRDFFilePath) throws IOException,
			ModelCreationException, BadConfigurationException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException {
		RDFLoader loader = new RDFLoader(configFile);
		loader.addRDF(subjectRDFFilePath);
		model = loader.model;
	}

	public void printStats() throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {

		System.out.println("triples: 		   " + GetTriplesCount());
		System.out.println("entities: 		   " + getEntitiesCount());
		System.out.println("distinct subjects: " + getDistintSubjectsCount());
		System.out.println("distinct objects: "  + getDistintObjectsCount());
		System.out.println("distinct concepts: " + getDistintSKOSConcepts());
		System.out.println("distinct xlabels: "  + getDistintSKOSXLLabels());

	}

	private String getCount(String queryString) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException, QueryEvaluationException {
		TupleQuery query = model.createTupleQuery(queryString);
		TupleBindingsIterator it = query.evaluate(false);
		TupleBindings tb = it.getNext();
		it.close();
		return tb.getBinding("no").getBoundValue().toString();
	}

	public String GetTriplesCount() throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {
		return getCount("SELECT (COUNT(*) AS ?no) { ?s ?p ?o  }");
	}

	public String getEntitiesCount() throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {
		return getCount("SELECT (COUNT(DISTINCT ?s) AS ?no) { ?s a []  }");
	}

	public String getDistintSubjectsCount() throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {
		return getCount("SELECT (COUNT(DISTINCT ?s ) AS ?no) {  ?s ?p ?o   }");
	}

	public String getDistintObjectsCount() throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {
		return getCount("SELECT (COUNT(DISTINCT ?o ) AS ?no) {  ?s ?p ?o  filter(!isLiteral(?o)) }");
	}

	public String getDistintSKOSConcepts() throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {
		return getCount("SELECT (COUNT(DISTINCT ?s ) AS ?no) {  ?s a "
				+ RDFNodeSerializer.toNT(SKOS.Res.CONCEPT) + " }");
	}

	public String getDistintSKOSXLLabels() throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {
		return getCount("SELECT (COUNT(DISTINCT ?s ) AS ?no) {  ?s a "
				+ RDFNodeSerializer.toNT(SKOSXL.Res.LABEL) + " }");
	}

	public static void main(String[] args) throws IOException, ModelCreationException,
			BadConfigurationException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException, UnsupportedQueryLanguageException, MalformedQueryException,
			QueryEvaluationException {
		String configFile = args[0];
		String subjectFile = args[1];

		VoIDStatsProducer voider = new VoIDStatsProducer(configFile, subjectFile);
		voider.printStats();

	}

}

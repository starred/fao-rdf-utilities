package fao.org.rdfmgmt.stats;

import fao.org.rdfmgmt.stats.TwoToThreeLangCodeResolver.MissingLangCodeConversionException;
import fao.org.rdfmgmt.utilities.RDFLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.TupleBindings;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.vocabulary.XmlSchema;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * this class helps in producing LIME stats<br/>
 * see also: <a
 * href="http://code.google.com/p/void-impl/wiki/SPARQLQueriesForStatistics">http://code.google.com
 * /p/void-impl/wiki/SPARQLQueriesForStatistics</a>
 * 
 * see: https://www.w3.org/2016/05/ontolex/#metadata-lime see:
 * http://link.springer.com/chapter/10.1007%2F978-3-319-18818-8_20
 * 
 * lime:lexicalEntries number of distinct lexical entries lexicalizing the dataset lime:references number of
 * entities in the dataset lexicalized by at least a lexical entry lime:lexicalizations number of
 * lexicalizations in a lexicalization set, i.e. number of unique <lexentry,reference> pairs
 * lime:avgNumOfLexicalizations average number of lexicalizations per dataset entity =
 * lexicalizations/|ref-dataset| lime:percentage percentage of entities in the reference dataset which have at
 * least one lexicalization
 * 
 * @author Armando Stellato <a href="mailto:stellato@uniroma2.it">stellato@uniroma2.it</a>
 * 
 */
public class LIMEStatsProducer {

	RDFModel model;
	String refDataset;
	HashMap<LexModel, HashMap<String, LangStat>> langStatsMap;

	enum LexModel {
		RDFS("http://www.w3.org/2000/01/rdf-schema#"), SKOS("http://www.w3.org/2004/02/skos/core"), SKOSXL(
				"http://www.w3.org/2008/05/skos-xl"), ONTOLEX("http://www.w3.org/ns/lemon/ontolex for ");

		private final String lexModelURI;

		LexModel(String lexModelURI) {
			this.lexModelURI = lexModelURI;
		}

		public String uri() {
			return lexModelURI;
		}
	}

	class LangStat {

		private String langcode;
		private String lang3code;
		private LexModel lexModel;
		
		private HashMap<String, ARTLiteral> propMap = new HashMap<String, ARTLiteral>();

		public LangStat(String langcode, LexModel lexModel) throws MissingLangCodeConversionException {
			this.langcode = langcode;
			this.lexModel = lexModel;
			this.lang3code = TwoToThreeLangCodeResolver.resolve(langcode);
		}

		public void addCount(String prop, ARTLiteral count) {
			propMap.put(prop, count);
		}

		public int getIntCount(String prop) {
			return Integer.parseInt(propMap.get(prop).getLabel());
		}

		public String getLangcode() {
			return langcode;
		}

		public String getLang3code() {
			return lang3code;
		}
		
		public String getID() {
			return ":" + lang3code + "_lex";
		}
		
		public String toString() {

			StringBuffer preamble = new StringBuffer(

			// @formatter:off
					getID() + " a lime:LexicalizationSet ;\n"
					+ "   lime:language \"" + langcode + "\" ;\n"
					+ "   dct:language  <http://id.loc.gov/vocabulary/iso639-1/" + langcode + ">, <http://lexvo.org/id/iso639-3/" + lang3code + "> ;\n"
					+ "   lime:lexicalizationModel <" + lexModel.uri() + "> ;\n"
					+ "   lime:referenceDataset " + refDataset 
					// @formatter:on
			);

			for (String prop : propMap.keySet()) {
				preamble.append(" ;\n   " + prop + " " + propMap.get(prop));
			}

			preamble.append(" .");

			return preamble.toString();
		}

		@Override
		public int hashCode() {
			return (langcode + lexModel.uri() + refDataset).hashCode();
		}

		public boolean equals(LangStat ls) {
			return (this.hashCode() == ls.hashCode());
		}
	}

	public LIMEStatsProducer(String configFile, String refDataset, String subjectRDFFilePath)
			throws IOException, ModelCreationException, BadConfigurationException, ModelAccessException,
			ModelUpdateException, UnsupportedRDFFormatException {
		this.refDataset = refDataset;
		RDFLoader loader = new RDFLoader(configFile);

		if (subjectRDFFilePath != null)
			loader.addRDF(subjectRDFFilePath);
		else
			System.err.println("no file loaded, use only if the repository already contains the data");
		model = loader.model;
		langStatsMap = new HashMap<LIMEStatsProducer.LexModel, HashMap<String, LangStat>>();
	}

	public LangStat obtainLangStat(LexModel lexModel, String langcode)
			throws MissingLangCodeConversionException {
		HashMap<String, LangStat> langStats = langStatsMap.get(lexModel);
		if (langStats.containsKey(langcode))
			return langStats.get(langcode);
		else {
			LangStat newLangStat = new LangStat(langcode, lexModel);
			langStats.put(langcode, newLangStat);
			return newLangStat;
		}
	}

	public void processStats(LexModel lexModel) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException, QueryEvaluationException {

		int datasetReferences = getDatasetReferences();

		HashMap<String, LIMEStatsProducer.LangStat> lexModelStats = new HashMap<String, LIMEStatsProducer.LangStat>();

		langStatsMap.put(lexModel, lexModelStats);

		System.out.println("\ndataset references: " + datasetReferences + "\n");

		storeLangCountMap(lexModel, getLexicalEntities(lexModel), "lime:lexicalEntities");
		storeLangCountMap(lexModel, getLexicalizations(lexModel), "lime:lexicalizations");
		storeLangCountMap(lexModel, getReferences(lexModel), "lime:references");

		for (String lang : lexModelStats.keySet()) {
			LangStat langStat = lexModelStats.get(lang);
			
			int lexicalizations = langStat.getIntCount("lime:lexicalizations");
			float avgNumOfLexicalizations = new Float(lexicalizations) / new Float(datasetReferences);
			langStat.addCount("lime:avgNumOfLexicalizations",
					model.createLiteral(Float.toString(avgNumOfLexicalizations), XmlSchema.Res.FLOAT));
			
			int references = langStat.getIntCount("lime:references");
			float percentage = new Float(references) / new Float(datasetReferences);
			langStat.addCount("lime:percentage",
					model.createLiteral(Float.toString(percentage), XmlSchema.Res.FLOAT));
		}

		System.out.println("STATS PROCESSED AND STORED");
	}

	public void printStats(LexModel lexModel) {
		// System.out.println("general map: " + langStatsMap);
		HashMap<String, LangStat> map = langStatsMap.get(lexModel);
		// System.out.println("map: " + map);
		ArrayList<String> arrList = new ArrayList<String>(map.keySet());
		Collections.sort(arrList);
		
		for (String lang : arrList) {
			System.out.println("void:subset " + map.get(lang).getID() + " ;");
			
			
		}
		
		for (String lang : arrList) {
			// for (String lang : arrList ) {
			System.out.println(map.get(lang));
			System.out.println();
			System.out.println();
		}
	}

	private int getCount(String queryString) throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {
		TupleQuery query = model.createTupleQuery(queryString);
		TupleBindingsIterator it = query.evaluate(false);
		TupleBindings tb = it.getNext();
		it.close();
		return Integer.parseInt(tb.getBinding("count").getBoundValue().asLiteral().getLabel());
	}

	private HashMap<String, ARTLiteral> processLangCount(LexModel lexModel, String queryString)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException,
			QueryEvaluationException {
		HashMap<String, ARTLiteral> langCounts = new HashMap<String, ARTLiteral>();
		TupleQuery query = model.createTupleQuery(queryString);
		TupleBindingsIterator it = query.evaluate(false);
		while (it.hasNext()) {
			TupleBindings tb = it.getNext();
			String lang = tb.getBinding("lang").getBoundValue().asLiteral().getLabel();
			ARTLiteral count = tb.getBinding("count").getBoundValue().asLiteral();
			langCounts.put(lang, count);
		}
		it.close();
		return langCounts;
	}

	private void storeLangCountMap(LexModel lexModel, HashMap<String, ARTLiteral> map, String property) {
		// System.out.println("storing the map: " + map);
		for (String lang : map.keySet()) {
			obtainLangStat(lexModel, lang).addCount(property, map.get(lang));
		}
	}

	public int getDatasetReferences() throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {
		return getCount("" 
					// @formatter:off
					+ "prefix owl: <http://www.w3.org/2002/07/owl#>              "
					+ "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
					+ "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>      "
					+ "prefix skos:   <http://www.w3.org/2004/02/skos/core#>     "
					+ "prefix skosxl: <http://www.w3.org/2008/05/skos-xl#>       "
                    + "                                                          "
					+ "SELECT (COUNT(DISTINCT ?s) AS ?count) {                   "
					+ "    { ?s a* rdfs:Class . }                                "
					+ "    UNION                                                 "
					+ "	{ ?s a* owl:Class .}                                     "
					+ "    UNION                                                 "
					+ "    { ?s a* rdf:Property . }                              "
					+ "    UNION                                                 "
					+ "    { ?s a* skos:Concept .  }                             "
					+ "    UNION                                                 "
					+ "    { ?s a* skos:ConceptScheme . }                        "
					+ "    UNION                                                 "
					+ "    { ?s a* skos:Collection . }                           "
					+ "}                                                         "
					// @formatter:on
		);
	}

	public HashMap<String, ARTLiteral> getLexicalEntities(LexModel lexModel)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException,
			QueryEvaluationException {

		if (lexModel == LexModel.SKOSXL)

			return processLangCount(LexModel.SKOSXL,""		
				// @formatter:off
					+ "prefix skosxl: <http://www.w3.org/2008/05/skos-xl#>    "
                    + "                                                       "
					+ "SELECT ?lang (COUNT(DISTINCT ?lit) AS ?count)          "
					+ "WHERE {                                                "
					+ "   ?xl skosxl:literalForm ?lit .                       "
					+ "   BIND(lang(?lit) as ?lang) .                         "
					+ "}                                                      "
					+ "GROUP BY ?lang  ORDER BY ?lang                         "
					// @formatter:on
			);
		else
			// still to produce the other stats
			return null;

	}

	public HashMap<String, ARTLiteral> getLexicalizations(LexModel lexModel)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException,
			QueryEvaluationException {

		if (lexModel == LexModel.SKOSXL)

			return processLangCount(LexModel.SKOSXL,""		
				// @formatter:off
					+ "prefix skosxl: <http://www.w3.org/2008/05/skos-xl#>    "
                    + "                                                       "
					+ "SELECT ?lang (COUNT(?lit) AS ?count)          "
					+ "WHERE {                                                "
					+ "   ?xl skosxl:literalForm ?lit .                       "
					+ "   BIND(lang(?lit) as ?lang) .                         "
					+ "}                                                      "
					+ "GROUP BY ?lang  ORDER BY ?lang                         "
					// @formatter:on
			);
		else
			// still to produce the other stats
			return null;

	}

	public HashMap<String, ARTLiteral> getReferences(LexModel lexModel)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException,
			QueryEvaluationException {

		if (lexModel == LexModel.SKOSXL)

			return processLangCount(LexModel.SKOSXL,""		
				// @formatter:off							
					+ "prefix skosxl: <http://www.w3.org/2008/05/skos-xl#>						"
                    + "                                                                         "
					+ "	SELECT ?lang (COUNT (DISTINCT ?c) AS ?count)                            "
					+ "	WHERE {                                                                 "
					+ "	   ?c  (skosxl:prefLabel|skosxl:altLabel|skosxl:hiddenLabel)   ?xl  .   "
					+ "	   ?xl skosxl:literalForm ?lit .                                        "
					+ "	   BIND(lang(?lit) as ?lang)   .                                        "
					+ "	}                                                                       "
					+ "	GROUP BY ?lang  ORDER BY ?lang                                          "
					// @formatter:on
			);
		else
			// still to produce the other stats
			return null;

	}

	public static void main(String[] args) throws IOException, ModelCreationException,
			BadConfigurationException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException, UnsupportedQueryLanguageException, MalformedQueryException,
			QueryEvaluationException {
		String configFile = args[0];
		String subjectFile = null;
		if (args.length > 1)
			subjectFile = args[1];

		LIMEStatsProducer limer = new LIMEStatsProducer(configFile, ":Agrovoc", subjectFile);
		limer.processStats(LexModel.SKOSXL);
		limer.printStats(LexModel.SKOSXL);

	}

}

package fao.org.rdfmgmt.stats;

import java.util.HashMap;

public class TwoToThreeLangCodeResolver {

	static HashMap<String,String> codeMap = new HashMap<>();

	
	public static class MissingLangCodeConversionException extends RuntimeException {

		private static final long serialVersionUID = 7884969762451647945L;

		String offendingCode;
		
		MissingLangCodeConversionException(String inputCode) {
			offendingCode = inputCode;			
		}
		
		public String getMessage() {
			return new String("no conversion code for: " + offendingCode);
		}

	}
	
	static {
		codeMap.put("en", "eng");
		codeMap.put("ar", "ara");
		codeMap.put("cs", "ces");
		codeMap.put("de", "deu");
		codeMap.put("es", "spa");
		codeMap.put("fa", "fas");
		codeMap.put("fr", "fra");
		codeMap.put("hi", "hin");
		codeMap.put("hu", "hun");
		codeMap.put("id", "ind");
		codeMap.put("it", "ita");
		codeMap.put("ja", "jpn");
		codeMap.put("ka", "kat");
		codeMap.put("km", "khm");
		codeMap.put("ko", "kor");
		codeMap.put("lo", "lao");
		codeMap.put("mo", "mo");  // was it for moldovan (then deprecated) or the special for the no-language tag?
		codeMap.put("my", "mya");
		codeMap.put("mr", "mar");
		codeMap.put("ms", "msa");
		codeMap.put("pl", "pol");
		codeMap.put("pt", "por");
		codeMap.put("ru", "rus");
		codeMap.put("sk", "slk");
		codeMap.put("sv", "swe");
		codeMap.put("te", "tel");
		codeMap.put("th", "tha");
		codeMap.put("tr", "tur");
		codeMap.put("uk", "ukr");
		codeMap.put("vi", "vie");
		codeMap.put("zh", "zho");		
	}
	
	public static String resolve(String twoDigitCode) throws MissingLangCodeConversionException {
		String threeDigitCode = codeMap.get(twoDigitCode);
		if (threeDigitCode!=null)
			return threeDigitCode;
		throw new MissingLangCodeConversionException(twoDigitCode);
	}
	
}

package fao.org.rdfmgmt.release;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;
import it.uniroma2.art.owlart.vocabulary.XmlSchema;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fao.org.rdfmgmt.owlart.ModelLoader;
import fao.org.rdfmgmt.transform.RDFTransformer;
import fao.org.rdfmgmt.transform.SKOSXL2SKOSConverter;
import fao.org.rdfmgmt.utilities.AddInverseTriples;
import fao.org.rdfmgmt.vocabularies.DCMIMetadataTerms;

/**
 * Utility for preparing various dumps of a new version of an RDF Dataset inside FAO<br/>
 * This assumes the starting point is a SKOSXL Dataset serialized in a RDF file (this utility has been created
 * after the big move to Vocbench 2.0, which natively supports and handles RDF content). The serialization
 * format is not important, the utility infers the format from the file extension. <br/>
 * <br/>
 * The processing chain is:
 * <ol>
 * <li>load vocbench dump</li>
 * <li>add metadata, per scheme (release date)</li>
 * <li>FIRST EXPORT (RDF/NT): dump it as agrovoc_yyyy-mm-dd_core.nt (and rdf)</li>
 * <li>add metadata, per concept (VoID pointer)</li>
 * <li>materialize plain SKOS core labelling triples</li>
 * <li>remove deprecated?</li>
 * <li>load agrontology vocabulary into its dedicated context (&lt;http://aims.fao.org/aos/agrontology&gt;)</li>
 * <li>load agrovoc mappings dataset into its dedicated context
 * (&lt;http://aims.fao.org/aos/agrovoc-mappings&gt;)</li>
 * <li>SECOND EXPORT (NQ): dump it as agrovoc_yyyy-mm-dd_lod.qt</li>
 * </ol>
 * 
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 */
public class FAORDFDatasetReleaser extends RDFTransformer {

	protected static Logger logger = LoggerFactory.getLogger(FAORDFDatasetReleaser.class);

	ReleaseInfo releaseInfo;
	SKOSXL2SKOSConverter skosxl2skos;
	
	ARTResource inferredTripleGraph = targetModel
			.createURIResource("http://aims.fao.org/aos/agrovoc-inferred");

	public static SimpleDateFormat yyyyMMddDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	public FAORDFDatasetReleaser(RDFModel model, File releaseConfigFile) throws IOException,
			BadConfigurationException {
		super(model);
		releaseInfo = new ReleaseInfo(releaseConfigFile);
		// this one does not inherit the RDFTransformer paradigm
		skosxl2skos = new SKOSXL2SKOSConverter(model);

	}

	public void execute() throws ModelUpdateException, IOException, ModelAccessException,
			UnsupportedRDFFormatException {

		// load vocbench dump
		logger.info("load vocbench dump");
		String vocbenchDumpFilePath = releaseInfo.getVocBenchDump();
		File vbDumpFile = new File(vocbenchDumpFilePath);
		targetModel.addRDF(vbDumpFile, null, RDFFormat.guessRDFFormatFromFile(vbDumpFile));
		File relaseDir = releaseInfo.getReleaseDirectory();
				
				
		// add metadata, per scheme (release date)
		logger.info("add metadata, per scheme (release date)");
		ArrayList<ARTURIResource> schemes = releaseInfo.getAllSchemes();
		for (ARTURIResource scheme : schemes) {
			// removes the previous dct:modified declarations about the default scheme
			targetModel.deleteTriple(scheme, DCMIMetadataTerms.Res.MODIFIED,NodeFilters.ANY);
			// adds the new dct:modified declarations about the default scheme
			targetModel.addTriple(scheme, DCMIMetadataTerms.Res.MODIFIED,
					targetModel.createLiteral(XmlSchema.formatCurrentUTCDateTime(), XmlSchema.Res.DATETIME));
		}		
		
		// IMPORTANT: put this line of code before the CORE export (or after) depending if the presence
		// of mappings is wanted in the core file.
		// add mappings file, if a reference is available
		logger.info("add mappings file, if a reference is available");
		String mappingsRef = releaseInfo.getMappingsRef();
		if (mappingsRef != null) {
			ARTResource mappingsNG = releaseInfo.getMappingsNG();
			logger.info("adding mappings to named graph: " + mappingsNG);
			if (mappingsRef.startsWith("http:")) {
				URL mappingsRefURL = new URL(mappingsRef);
				targetModel.addRDF(mappingsRefURL, null, null, mappingsNG);
			} else {
				File mappingsRefFile = new File(mappingsRef);
				targetModel.addRDF(mappingsRefFile, null, RDFFormat.guessRDFFormatFromFile(mappingsRefFile),
						mappingsNG);
			}
		} else
			logger.info("no mappings reference has been provided; no alignment file is being loaded");

		// FIRST EXPORT (RDF/NT): dump it as agrovoc_yyyy-mm-dd_core.nt (and rdf)
		logger.info("FIRST EXPORT (RDF/NT): dump it as agrovoc_yyyy-mm-dd_core.nt (and rdf)");
		String dataset = releaseInfo.getDataset();
		String coreFileBaseName = dataset.toLowerCase() + "_" + yyyyMMddDateFormat.format(new Date())
				+ "_core";
		File ntTargetFile = new File(relaseDir, coreFileBaseName + ".nt");
		File rdfxmlTargetFile = new File(relaseDir, coreFileBaseName + ".rdf");

		targetModel.writeRDF(ntTargetFile, RDFFormat.guessRDFFormatFromFile(ntTargetFile),
				NodeFilters.MAINGRAPH);
		targetModel.writeRDF(rdfxmlTargetFile, RDFFormat.guessRDFFormatFromFile(rdfxmlTargetFile),
				NodeFilters.MAINGRAPH);

		// add reference to metadata file(VoID pointer) for each type
		// this step is processed only if VoID reference is provided
		logger.info("add metadata, per concept (VoID pointer)");
		ARTURIResource voidDataset = releaseInfo.getVoIDDataset();
		if (voidDataset != null) {
			addInDatasetPerType(SKOS.Res.CONCEPT, voidDataset);
			addInDatasetPerType(SKOSXL.Res.LABEL, voidDataset);
			addInDatasetPerType(SKOS.Res.CONCEPTSCHEME, voidDataset);
		}

		// materialize plain SKOS core labelling triples, keeping the
		logger.info("materialize plain SKOS core labelling triples");
		skosxl2skos.transform(true, inferredTripleGraph);

		// remove deprecated?
		// TODO (check first if we have to do it)

		// add ontology vocabulary, if a reference is available
		logger.info("add ontology vocabulary, if a reference is available");
		String ontoRef = releaseInfo.getOntologyRef();
		if (ontoRef != null) {
			ARTURIResource ontoNG = releaseInfo.getOntologyNG();
			if (ontoRef.startsWith("http:")) {
				URL ontoRefURL = new URL(ontoRef);
				targetModel.addRDF(ontoRefURL, null, null, ontoNG);
			} else {
				File ontoRefFile = new File(ontoRef);
				targetModel.addRDF(ontoRefFile, null, RDFFormat.guessRDFFormatFromFile(ontoRefFile), ontoNG);
			}
		}

		// materialize inferrable triples related to symmetric and inverse properties
		// done before the mapping, as mappings cannot be inverted due to mention of external content
		AddInverseTriples invTriplesRoutine = new AddInverseTriples(targetModel, SKOSXLModel.class);
		invTriplesRoutine.addInverseTriples(inferredTripleGraph);

		// SECOND (and third) EXPORT (NQ): dump it as <datasetName>_yyyy-mm-dd_lod.qt</li
		logger.info("SECOND EXPORT (NQ): dump it as <datasetName>_yyyy-mm-dd_lod.nq");
		String lodFileBaseName = dataset.toLowerCase() + "_" + yyyyMMddDateFormat.format(new Date()) + "_lod";
		File qtLodTargetFile = new File(relaseDir, lodFileBaseName + ".nq"); // LOD version, graph separated quads
		File ntLodTargetFile = new File(relaseDir, lodFileBaseName + ".nt"); // LOD version, all merged triples

		targetModel.writeRDF(qtLodTargetFile, RDFFormat.guessRDFFormatFromFile(qtLodTargetFile));
		targetModel.writeRDF(ntLodTargetFile, RDFFormat.guessRDFFormatFromFile(ntLodTargetFile));
	}

	// ?? scheme <http://purl.org/dc/terms/modified> ??date
	// ??concept <http://rdfs.org/ns/void#inDataset> <http://aims.fao.org/aos/agrovoc/void.ttl#Agrovoc>

	// this is adding not only concepts, but also labels
	private void addInDatasetPerType(ARTURIResource type, ARTURIResource voidDataset)
			throws ModelAccessException, ModelUpdateException {
		ARTResourceIterator it = sourceModel.listSubjectsOfPredObjPair(RDF.Res.TYPE, type, true,
				NodeFilters.MAINGRAPH);
		while (it.streamOpen())
			addTriple(it.getNext(), targetModel.createURIResource("http://rdfs.org/ns/void#inDataset"),
					voidDataset);
		it.close();
	}

	public static void main(String[] args) throws IOException, ModelCreationException,
			BadConfigurationException, ModelUpdateException, ModelAccessException,
			UnsupportedRDFFormatException {
		if (args.length < 2) {
			System.out.println("usage:\njava " + FAORDFDatasetReleaser.class.getCanonicalName()
					+ " <modelConfigfile> <releaseConfigFile> \n"
					+ "\t<modelConfigfile>    : config file for the chosen RDFModel Implementation \n"
					+ "\t<releaseConfigFile> : config file for managing the release  \n");
			;
			return;
		}

		String modelConfigFile = args[0];
		File releaseConfigFile = new File(args[1]);

		RDFModel model = ModelLoader.loadRDFModel(modelConfigFile);
		FAORDFDatasetReleaser releaser = new FAORDFDatasetReleaser(model, releaseConfigFile);

		releaser.execute();

	}

}

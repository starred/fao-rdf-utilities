package fao.org.rdfmgmt.release;

import fao.org.rdfmgmt.owlart.ModelLoader.ModelProps;
import fao.org.rdfmgmt.vocabularies.RDFVocabulary;
import fao.org.rdfmgmt.vocabularies.SKOSVocabulary;
import fao.org.rdfmgmt.vocabularies.VocabularyFactory;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.model.impl.ARTNodeFactoryImpl;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class ReleaseInfo {

	private final static String datasetProp = "dataset";
	private final static String vocbenchDumpProp = "vocbenchDump";
	private final static String voidDatasetProp = "voidDataset";
	private final static String ontologyRefProp = "ontologyRef";
	private final static String ontologyNGProp = "ontologyNG";
	private final static String mappingsRefProp = "mappingsRef";
	private final static String mappingsNGProp = "mappingsNG";
	private final static String releaseDirectory = "releaseDirectory";
	private final static String otherConceptSchemes = "otherConceptSchemes";

	Properties properties;
	ARTNodeFactoryImpl nf;

	private String baseuri;
	private String namespace;
	private String defaultScheme;
	private File propFile;

	ReleaseInfo(File propFile) throws IOException, BadConfigurationException {
		properties = new Properties();
		FileInputStream fis = new FileInputStream(propFile);
		properties.load(fis);
		initializeVocabularyProperties();
		fis.close();
		nf = new ARTNodeFactoryImpl();
		this.propFile = propFile;
	}

	/**
	 * baseuri, namespace and default scheme may be accessed by declaring the <code>vocabulary</code>
	 * property. In this property
	 * 
	 * @throws BadConfigurationException
	 */
	private void initializeVocabularyProperties() throws BadConfigurationException {
		String dataset = getDataset();
		if (dataset != null) {
			RDFVocabulary datasetvocab = VocabularyFactory.getVocabulary(dataset);
			if (datasetvocab == null)
				throw new BadConfigurationException("dataset: " + dataset
						+ " is not registered in the vocabulary register");
			baseuri = datasetvocab.getBaseURI();
			namespace = datasetvocab.getDefNamespace();
			if (datasetvocab instanceof SKOSVocabulary)
				defaultScheme = ((SKOSVocabulary) datasetvocab).getDefaultScheme();
		} else {
			baseuri = getMandatoryPropertyValue(ModelProps.baseuriProp);
			namespace = getMandatoryPropertyValue(ModelProps.namespaceProp);
			defaultScheme = getPropertyValue(ModelProps.defaultSchemeProp);
		}
	}

	/**
	 * this is made public in case of needing to add dynamically more properties. However, specific getters
	 * should be adopted
	 * 
	 * @param propName
	 * @return
	 */
	public String getPropertyValue(String propName) {
		String temp = properties.getProperty(propName);
		if (temp != null)
			return temp.trim();
		return null;
	}

	/**
	 * as for {@link #getPropertyValue(String)}, but the value need to be present. Note that being mandatory
	 * is not an intrinsic characteristic of the property, and this is the reason for creating a specific
	 * method. It may well be that the property is not always necessary, but a particular subcase of the
	 * routine accessing it, may require it to be present, or fail. Thus, in that subcase, this method is
	 * invoked instead of the standard one
	 * 
	 * @param propName
	 * @return
	 * @throws BadConfigurationException
	 */
	public String getMandatoryPropertyValue(String propName) throws BadConfigurationException {
		String value = getPropertyValue(propName);
		if (value != null)
			return value;
		throw new BadConfigurationException("property: " + propName
				+ " is missing from the loaded configuration file");
	}

	private ARTURIResource getURIResourceFromString(String value) {
		if (value == null)
			return null;
		return nf.createURIResource(value);
	}

	public String getDataset() {
		return getPropertyValue(datasetProp);
	}

	public String getOntologyRef() {
		return getPropertyValue(ontologyRefProp);
	}

	public String getBaseuri() {
		return baseuri;
	}

	public String getNamespace() {
		return namespace;
	}

	public ARTURIResource getDefaultScheme() {
		return getURIResourceFromString(defaultScheme);
	}

	public ArrayList<ARTURIResource> getOtherSchemes() {
		ArrayList<ARTURIResource> schemes = new ArrayList<ARTURIResource>();
		String[] schemesString = getPropertyValue(otherConceptSchemes).split(";");
		for (String schemeString : schemesString)
		schemes.add(getURIResourceFromString(schemeString));
		return schemes;
	}
	
	public ArrayList<ARTURIResource> getAllSchemes() {
		ArrayList<ARTURIResource> schemes = getOtherSchemes();
		schemes.add(getDefaultScheme());
		return schemes;
	}
	
	
	public ARTURIResource getVoIDDataset() {
		return getURIResourceFromString(getPropertyValue(voidDatasetProp));
	}

	public String getVocBenchDump() {
		return getPropertyValue(vocbenchDumpProp);
	}

	public ARTURIResource getOntologyNG() {
		return getURIResourceFromString(getPropertyValue(ontologyNGProp));
	}

	public String getMappingsRef() {
		return getPropertyValue(mappingsRefProp);
	}

	public ARTResource getMappingsNG() {
		String mappingsNGValue = getPropertyValue(mappingsNGProp);
		if (mappingsNGValue != null)
			return getURIResourceFromString(getPropertyValue(mappingsNGProp));
		return NodeFilters.MAINGRAPH;
	}

	public File getReleaseInfoFile() {
		return propFile;
	}

	/**
	 * @return the directory indicated by property <code>releaseDirectory</code>, or if the property is not
	 *         specified, the directory in which the releaseInfo config file is located
	 */
	public File getReleaseDirectory() {
		String relDir = getPropertyValue(releaseDirectory);
		if (relDir != null)
			return new File(relDir);
		return getReleaseInfoFile().getParentFile();
	}

}

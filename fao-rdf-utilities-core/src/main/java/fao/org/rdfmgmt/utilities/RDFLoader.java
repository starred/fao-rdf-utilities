package fao.org.rdfmgmt.utilities;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple utility class for loading one or more RDF files into a given model. Each file passed as argument can
 * be a directory, and in that case, its content is recursively inspected. The sole requirement is that the
 * directory structure is made only of rdf files or directories.
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class RDFLoader {

	protected static Logger logger = LoggerFactory.getLogger(RDFLoader.class);

	public RDFModel model;

	public RDFLoader(String configFile) throws IOException, ModelCreationException, BadConfigurationException {
		model = ModelLoader.loadRDFModel(configFile);
		// skosXLModel.setDefaultNamespace(OWL2SKOSConverter.agrovocDefNamespace);
	}

	public void addRDF(String... filePaths) throws IOException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException {
		int numFiles = filePaths.length;
		File[] files = new File[filePaths.length];
		for (int i = 0; i < numFiles; i++) {
			files[i] = new File(filePaths[i]);
		}
		addRDF(files);
	}

	public void addRDF(File... files) throws IOException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException {
		for (File file : files) {
			if (file.isDirectory()) {
				// if a directory, recursively load content
				logger.info("processing content of directory: " + file);
				addRDF(file.listFiles());
			} else {
				// if a file, just load that file
				logger.info("loading: " + file);
				model.addRDF(file, null, RDFFormat.guessRDFFormatFromFile(file));
			}
		}
		// ((TransactionBasedModel) model).commit();
	}

	public RDFModel getModel() {
		return model;
	}

	public static void main(String[] args) throws ModelCreationException,
			UnsupportedModelConfigurationException, ModelUpdateException, IOException,
			ClassNotFoundException, InstantiationException, IllegalAccessException,
			BadConfigurationException, UnloadableModelConfigurationException, ModelAccessException,
			UnsupportedRDFFormatException {

		if (args.length < 3) {
			System.err
					.println("usage:\n"
							+ "java "
							+ fao.org.rdfmgmt.utilities.RDFLoader.class.getName()
							+ " <configfile> <write> <rdfFile> [<rdfFile>]*\n"

							+ "\t<configfile>  : config file for the chosen RDFModel Implementation \n"
							+ "\t<write>  : if true, the last rdfFile specified with the last vararg parameter is actually written with the content of the loaded files"
							+ "\t[<rdfFile>]                       : files to be uploaded to the rdf repository \n");
			return;
		}

		String configFile = args[0];
		boolean write = Boolean.parseBoolean(args[1]);

		// OWL ART SKOSXLMODEL LOADING
		RDFLoader loader = new RDFLoader(configFile);

		int lastFile = args.length;
		if (write) {
			lastFile = lastFile - 1;
			logger.info("configured for writing to file: " + args[lastFile]);
		}

		String[] filesToBeLoaded = new String[lastFile - 2];
		for (int i = 2; i < lastFile; i++) {
			filesToBeLoaded[i - 2] = args[i];
		}

		loader.addRDF(filesToBeLoaded);

		if (write) {
			File outputFile = new File(args[lastFile]);
			logger.info("saving to: " + outputFile);
			loader.model.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile),
					NodeFilters.MAINGRAPH);
			// loader.model.writeRDF(outputFile, RDFFormat.RDFXML_ABBREV, NodeFilters.MAINGRAPH);
		}

		System.out.println("RDF data uploaded into the model");
	}

}

package fao.org.rdfmgmt.utilities;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.RDFSModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;

import java.io.File;
import java.io.IOException;

public class FormatConverter {

	/**
	 * this main method takes an input file (first argument) in some RDF serialization format, loads it by
	 * guessing the format from the file extension, and then converts it into another serialization format
	 * writing it onto an output file (second argument). The third optional parameter specifies the class
	 * implementing OWL ART API (the ModelFactory implementation class). By default, if third argument is not
	 * specified, Sesame2 implementation is being used.
	 * 
	 * @param args
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ModelCreationException
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 * @throws UnsupportedRDFFormatException
	 * @throws BadConfigurationException
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException, ClassNotFoundException,
			InstantiationException, IllegalAccessException, ModelCreationException, ModelUpdateException,
			ModelAccessException, UnsupportedRDFFormatException, BadConfigurationException {
		if (args.length < 3) {
			System.out.println("usage:\n" + FormatConverter.class.getName()
					+ " <configFile> <inputfilename> <outputfilename> [DEF|ANY]");
			return;
		}

		// OWL ART SKOSXLMODEL LOADING

		RDFModel model = ModelLoader.loadRDFModel(args[0]);

		// LOADING PRODUCED DATA IN SOURCE SERIALIZATION FORMAT AND CONVERTING TO OUTPUT FORMAT

		File inputFile = new File(args[1]);
		File outputFile = new File(args[2]);
		ARTResource exportedGraphs = NodeFilters.MAINGRAPH;
		if (args.length > 3 && args[3].equals("ANY"))
			exportedGraphs = NodeFilters.ANY;

		model.addRDF(inputFile, model.getBaseURI(), RDFFormat.guessRDFFormatFromFile(inputFile));
		model.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), exportedGraphs);
	}
}

package fao.org.rdfmgmt.utilities;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.vocabulary.OWL;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * This utility allows to add inverse triples for:
 * <ul>
 * <li>symmetric properties: s p o are inverted into o p s</li>
 * <li>inverse properties: given p inverseOf p1: all s p1 o produce o p1 s</li>
 * </ul>
 * Note that this is not a complete reasoning system, no iteration steps are done, no transitivity is computed
 * 
 * Also, the model allows to ingest a plain RDFModel, and then manually add the symmetric/inverseOf properties
 * by specifying the kind of data which is inside (should allow)
 * 
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 *
 */
public class AddInverseTriples {

	RDFModel model;
	HashMap<ARTURIResource, ARTURIResource> inversePropMap;
	Set<ARTURIResource> symmetricProperties;

	public AddInverseTriples(RDFModel model, Class<? extends RDFModel> modelClass) throws ModelAccessException {
		this.model = model;
		inversePropMap = new HashMap<ARTURIResource, ARTURIResource>();
		ARTStatementIterator it = model.listStatements(NodeFilters.ANY, OWL.Res.INVERSEOF, NodeFilters.ANY,
				true);
		while (it.hasNext()) {
			ARTStatement st = it.getNext();
			ARTURIResource subject = st.getSubject().asURIResource();
			ARTURIResource object = st.getObject().asURIResource();
			inversePropMap.put(subject, object);
			inversePropMap.put(object, subject);
		}
		it.close();

		symmetricProperties = new HashSet<ARTURIResource>();
		symmetricProperties.addAll(RDFIterators.getCollectionFromIterator(RDFIterators
				.toURIResourceIterator(model.listSubjectsOfPredObjPair(RDF.Res.TYPE,
						OWL.Res.SYMMETRICPROPERTY, true))));
		
		
		// case of SKOSModel
		if (SKOSModel.class.isAssignableFrom(modelClass)) {
			symmetricProperties.add(SKOS.Res.RELATED);
			symmetricProperties.add(SKOS.Res.RELATEDMATCH);
			symmetricProperties.add(SKOS.Res.EXACTMATCH);
			symmetricProperties.add(SKOS.Res.CLOSEMATCH);
			
			inversePropMap.put(SKOS.Res.HASTOPCONCEPT, SKOS.Res.TOPCONCEPTOF);
			inversePropMap.put(SKOS.Res.TOPCONCEPTOF, SKOS.Res.HASTOPCONCEPT);
			
			inversePropMap.put(SKOS.Res.BROADER, SKOS.Res.NARROWER);
			inversePropMap.put(SKOS.Res.NARROWER, SKOS.Res.BROADER);
			
			inversePropMap.put(SKOS.Res.BROADERTRANSITIVE, SKOS.Res.NARROWERTRANSITIVE);
			inversePropMap.put(SKOS.Res.NARROWERTRANSITIVE, SKOS.Res.BROADERTRANSITIVE);
			
			inversePropMap.put(SKOS.Res.BROADMATCH, SKOS.Res.NARROWMATCH);
			inversePropMap.put(SKOS.Res.NARROWMATCH, SKOS.Res.BROADMATCH);
			
			
			// case of SKOSModel
			if (SKOSXLModel.class.isAssignableFrom(modelClass)) {
				symmetricProperties.add(SKOSXL.Res.LABELRELATION);
			}
		}

	}

	public Set<ARTURIResource> getSymmetricProperties() throws ModelAccessException {
		return symmetricProperties;
	}

	public HashMap<ARTURIResource, ARTURIResource> getInversePropertiesMap() {
		return inversePropMap;
	}

	public void addInverseTriples(ARTResource graph) throws ModelAccessException, ModelUpdateException {
			
		for (ARTURIResource symProp : symmetricProperties) {
			ARTStatementIterator it = model.listStatements(NodeFilters.ANY, symProp, NodeFilters.ANY, false);
			while (it.hasNext()) {
				ARTStatement st = it.getNext();
				model.addTriple(st.getObject().asResource(), symProp, st.getSubject(), graph);
			}
			it.close();			
		}
		
		for (ARTURIResource invProp1 : inversePropMap.keySet()) {
			ARTURIResource invProp2 = inversePropMap.get(invProp1);
			
			ARTStatementIterator it = model.listStatements(NodeFilters.ANY, invProp1, NodeFilters.ANY, false);
			while (it.hasNext()) {
				ARTStatement st = it.getNext();
				model.addTriple(st.getObject().asResource(), invProp2, st.getSubject(), graph);
			}
			it.close();			
		}
	}

	public static void main(String[] args) throws ModelCreationException,
			UnsupportedModelConfigurationException, ModelUpdateException, IOException,
			ClassNotFoundException, InstantiationException, IllegalAccessException,
			BadConfigurationException, UnloadableModelConfigurationException, ModelAccessException,
			UnsupportedRDFFormatException {

		if (args.length < 2) {
			System.out.println("usage:\n" + "java "
					+ fao.org.rdfmgmt.utilities.AddInverseNarrowerTriples.class.getName()
					+ " <configfile> <inputRDFFile> <outputRDFFile>\n"
					+ "\t<configfile>  : config file for the chosen RDFModel Implementation \n"
					+ "\t<inputRDFFile> : file to be uploaded to the rdf repository \n"
					+ "\t<outputRDFFile> : output file with added inverse narrower triples \n");
			return;
		}

		String configFile = args[0];
		String inputFileName = args[1];
		String outputFileName = args[2];

		// OWL ART SKOSXLMODEL LOADING
		RDFModel model = ModelLoader.loadRDFModel(configFile);
		// skosXLModel.setDefaultNamespace(OWL2SKOSConverter.agrovocDefNamespace);

		File inputFile = new File(inputFileName);
		File outputFile = new File(outputFileName);

		System.out.println("adding RDF data to the model");
		model.addRDF(inputFile, null, RDFFormat.guessRDFFormatFromFile(inputFile));
		
		AddInverseTriples routine = new AddInverseTriples(model, SKOSXLModel.class);
		
		System.out.println("symmetric properties\n" + routine.getSymmetricProperties());

		System.out.println("inverse properties map\n" + routine.getInversePropertiesMap());
		
		model.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);

	}

}

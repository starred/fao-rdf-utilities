package fao.org.rdfmgmt.utilities;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.query.GraphQuery;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.Query;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.query.io.TupleBindingsWriterException;
import it.uniroma2.art.owlart.query.io.TupleBindingsWritingFormat;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.vocabulary.OWL;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.RDFS;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.openrdf.sail.nativerdf.datastore.DataFile;

/**
 * This simple utility is meant to recover those triples which, due to a bug in OWLIM, are lost at the very
 * moment in which these are uploaded (notice: uploaded, not when exported) into OWLIM. The bug (see related
 * page on Confluence) seems to be related to not importing redundant triples as, while it is importing a
 * datafile, the OWLIM reasoner is already working, and OWLIM does not assert triples which may have been
 * inferred in the meantime.<br/>
 * <br/>
 * The good solution is to keep track of these lost triples by exporting the data immediately after it has
 * been imported into OWLIM. This way, we trace exactly the triples which have been missing from the original
 * file.<br/>
 * <br/>
 * However, it happened that we did not do this thing, thus I was able to track a difference only after months
 * of changes. For this reason, I wrote this utility, which just limits the recovery to rdf:type triples.
 * Other triples, such as skos:broader, could in fact be cancelled for real from the original system in the
 * meanwhile. Also, the fac that only redudnant triples should be missing, let us suggest to recover only
 * rdf:type (or at least, be not too worried about other potentially missing triples, as they were in any
 * case...redundant). <br/>
 * <p>
 * So, in short, this class does:
 * <ul>
 * <li>loads a input dataset with missing rdf:types</li>
 * <li>loads a file with the recovered rdf:types</li>
 * <li>each concept in the recovered rdf:type file, is checked for existence (i.e. there's at least a triple
 * in the input dataset with that concept). The risk is that some concepts are generated by mistake from other
 * data, or maybe they are not concept and for some bug in the extraction mechanism, they have been reported
 * as concepts with missing type</li>
 * </ul>
 * </p>
 * <p>
 * <em>Last note: to have a complete check: we exclude those elements which do not compare at all as subject of
 * any triple in the Agrovoc file to which the triples are being re-added.</em>
 * </p>
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 *
 */
public class RecoverMissingRDFTypesDueToOWLIMBug {

	public static void main(String[] args) throws IOException, ModelCreationException,
			BadConfigurationException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException, QueryEvaluationException, TupleBindingsWriterException,
			UnsupportedQueryLanguageException, MalformedQueryException {

		if (args.length < 4) {
			System.out.println("usage:\n" + "java " + RecoverMissingRDFTypesDueToOWLIMBug.class.toString()
					+ "\n" + "\t<configfile>  : config file for the chosen RDFModel Implementation \n"
					+ "\t<dataFile>  : the file missing some triples \n"
					+ "\t<missingTriplesFile>  : the file containing the identified missing triples \n"
					+ "\t<outputfile>  : the merge of the two previous files \n");
			return;
		}

		String configFile = args[0];
		File dataFile = new File(args[1]);
		File missingTriplesFile = new File(args[2]);
		File outputFile = new File(args[3]);

		RDFModel sourceModel = ModelLoader.loadModel(configFile, RDFModel.class);
		sourceModel.addRDF(dataFile, null, RDFFormat.guessRDFFormatFromFile(dataFile));

		// TupleQuery query = sourceModel.createTupleQuery("SELECT (COUNT(*) AS ?no) { ?s ?p ?o  }");
		// System.out.println("triples in the model before the addition of the missing triples");
		// query.evaluate(false, TupleBindingsWritingFormat.XML, System.out);

		RDFModel missingTriplesModel = ModelLoader.loadModel(configFile, RDFModel.class);
		missingTriplesModel.addRDF(missingTriplesFile, null,
				RDFFormat.guessRDFFormatFromFile(missingTriplesFile));

		ARTStatementIterator it = missingTriplesModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
				NodeFilters.ANY, false, NodeFilters.MAINGRAPH);

		while (it.hasNext()) {
			ARTStatement st = it.getNext();
			ARTResource subject = st.getSubject();
			if (sourceModel.existsResource(subject, NodeFilters.MAINGRAPH)) {
				sourceModel.addStatement(st);
			} else {
				System.err.println("resource " + subject
						+ " present in the missing triples file, was not present in Agrovoc");
			}
		}

		it.close();

		// System.out.println("triples in the model after the addition of the missing triples");
		// query.evaluate(false, TupleBindingsWritingFormat.XML, System.out);

		ARTResource[] graphFilter = null;
		RDFFormat outputFormat = RDFFormat.guessRDFFormatFromFile(outputFile);
		if ((outputFormat == RDFFormat.NTRIPLES) || (outputFormat == RDFFormat.RDFXML)
				|| (outputFormat == RDFFormat.RDFXML_ABBREV))
			graphFilter = new ARTResource[] { NodeFilters.MAINGRAPH };
		if ((outputFormat == RDFFormat.NQUADS)) {
			Collection<ARTResource> ngs = RDFIterators.getCollectionFromIterator(sourceModel
					.listNamedGraphs());
			System.out.println("named graphs: " + ngs);
			ngs.add(NodeFilters.MAINGRAPH);
			ngs.remove(RDFS.Res.URI);
			ngs.remove(OWL.Res.URI);
			ngs.remove(SKOS.Res.URI);
			ngs.remove(SKOSXL.Res.URI);
			graphFilter = new ARTResource[ngs.size()];
			ngs.toArray(graphFilter);
			System.out.println("named graphs: " + graphFilter + " after cleanup of model vocabularies");
		}
		if (graphFilter == null) {
			System.err
					.println("output format not recognized, add this to either the quad or triple format lists in this routine");
		}

		sourceModel.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), graphFilter);

	}
}

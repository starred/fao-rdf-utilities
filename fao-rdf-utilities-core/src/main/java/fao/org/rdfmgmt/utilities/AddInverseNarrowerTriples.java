package fao.org.rdfmgmt.utilities;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.vocabulary.SKOS;

import java.io.File;
import java.io.IOException;

/**
 * Simple utility class for adding inverse triples to a model loaded through a rdf file (various formats are
 * allowed)
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class AddInverseNarrowerTriples {
	public static void main(String[] args) throws ModelCreationException,
			UnsupportedModelConfigurationException, ModelUpdateException, IOException,
			ClassNotFoundException, InstantiationException, IllegalAccessException,
			BadConfigurationException, UnloadableModelConfigurationException, ModelAccessException,
			UnsupportedRDFFormatException {

		if (args.length < 2) {
			System.out.println("usage:\n" + "java "
					+ fao.org.rdfmgmt.utilities.AddInverseNarrowerTriples.class.getName()
					+ " <configfile> <inputRDFFile> <outputRDFFile>\n"
					+ "\t<configfile>  : config file for the chosen RDFModel Implementation \n"
					+ "\t<inputRDFFile> : file to be uploaded to the rdf repository \n"
					+ "\t<outputRDFFile> : output file with added inverse narrower triples \n");
			return;
		}

		String configFile = args[0];
		String inputFileName = args[1];
		String outputFileName = args[2];

		// OWL ART SKOSXLMODEL LOADING
		RDFModel model = ModelLoader.loadRDFModel(configFile);
		// skosXLModel.setDefaultNamespace(OWL2SKOSConverter.agrovocDefNamespace);

		File inputFile = new File(inputFileName);
		File outputFile = new File(outputFileName);
		
		System.out.println("adding RDF data to the model");
		model.addRDF(inputFile, null, RDFFormat.guessRDFFormatFromFile(inputFile));
		System.out.println("RDF data uploaded into the model");

		File originalFile = new File("originalFile.nt");
		model.writeRDF(originalFile, RDFFormat.guessRDFFormatFromFile(originalFile), NodeFilters.MAINGRAPH);
		
		ARTStatementIterator it = model.listStatements(NodeFilters.ANY, SKOS.Res.BROADER, NodeFilters.ANY,
				false);
		while (it.streamOpen()) {
			ARTStatement stat = it.getNext();
			model.addTriple(stat.getObject().asURIResource(), SKOS.Res.NARROWER, stat.getSubject());			
		}
		it.close();
				
		model.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);
		
		model.writeRDF(new File("rdfxmloutput.rdf"), RDFFormat.RDFXML_ABBREV, NodeFilters.MAINGRAPH);
	}

}

package fao.org.rdfmgmt.utilities;

import fao.org.rdfmgmt.vocabularies.DCMIMetadataTerms;
import fao.org.rdfmgmt.vocabularies.VOCBENCH;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.XmlSchema;

import java.util.UUID;

/**
 * this utility class provides the implementation of some typical actions performed by VOCBENCH (creating a
 * concept, creating an xLabel), so that we don't need to recall them each time.
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 *
 */
public class VOCBENCH_OPS {

	ARTLiteral statusPublishedValue;
	ARTURIResource agrovocCodeDataType;

	SKOSXLModel model;

	public VOCBENCH_OPS(SKOSXLModel model) {
		this.model = model;
		statusPublishedValue = model.createLiteral("Published", XmlSchema.Res.STRING);
		agrovocCodeDataType = model.createURIResource("http://aims.fao.org/aos/agrovoc/AgrovocCode");
	}

	private static String createTruncatedUUID() {
		return UUID.randomUUID().toString().split("-")[4];
	}

	/**
	 * beware! when preferred==true, this also removes the old label if it exists
	 * 
	 * @param concept
	 * @param label
	 * @param language
	 * @param preferred
	 * @return
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	public ARTURIResource addXLabel(ARTURIResource concept, String label, String language, boolean preferred)
			throws ModelUpdateException, ModelAccessException {

		String truncatedUUID = createTruncatedUUID();

		ARTURIResource xLabel = model.addXLabel(model.getDefaultNamespace() + "xl_" + language + "_"
				+ truncatedUUID, label, language);
		if (preferred)
			model.setPrefXLabel(concept, xLabel);
		else
			model.addAltXLabel(concept, xLabel);

		// adding PUBLISHED of creation
		model.addTriple(xLabel, VOCBENCH.Res.hasStatus, statusPublishedValue);

		// adding time of creation
		model.addTriple(xLabel, DCMIMetadataTerms.Res.CREATED,
				model.createLiteral(XmlSchema.formatCurrentUTCDateTime(), XmlSchema.Res.DATETIME));

		// adding the notation
		model.addTriple(xLabel, SKOS.Res.NOTATION,
				model.createLiteral(truncatedUUID, agrovocCodeDataType));

		return xLabel;
	}

}

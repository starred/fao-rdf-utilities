package fao.org.rdfmgmt.utilities;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * this class produces bags of narrower concepts from a series of root concepts passed as argument
 * 
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class BagOfNarrowers {

	protected static Logger logger = LoggerFactory.getLogger(BagOfNarrowers.class);

	public static void main(String[] args) throws ModelCreationException,
			UnsupportedModelConfigurationException, ModelUpdateException, IOException,
			ClassNotFoundException, InstantiationException, IllegalAccessException,
			BadConfigurationException, UnloadableModelConfigurationException, ModelAccessException,
			UnsupportedRDFFormatException {

		if (args.length < 2) {
			System.out
					.println("usage:\n"
							+ "java fao.org.owl2skos.utilities.ListConceptsAndLabels\n"
							+ "\t<configfile>  : config file for the chosen RDFModel Implementation \n"
							+ "\t<rootConcept>  : the root concept after which all narrower concepts are calculated \n"
							+ "\t<inputfile>|preloaded   : rdf input file; \"preloaded\" means no file will be loaded \n"
							+ "\t<outputfile>  : file containing the list of concept URIs and their preferred labels \n"							
							);
			return;
		}

		String configFile = args[0];
		String rootConceptPar = args[1];
		String inputFilePar = args[2];
		String outputFilePar = args[3];
		

		// OWL ART SKOSXLMODEL LOADING
		RDFModel model = ModelLoader.loadModel(configFile, SKOSXLModel.class);

		if (!inputFilePar.equals("preloaded")) {
			File inputFile = new File(inputFilePar);
			logger.info("loading file: " + inputFile);
			model.addRDF(inputFile, model.getBaseURI(), RDFFormat.guessRDFFormatFromFile(inputFile));
		}

		ARTURIResource rootConcept = model.createURIResource(rootConceptPar);

		listSKOSConceptsAndPrefLabels((SKOSXLModel) model, rootConcept, new File(outputFilePar));
	}

	public static void listSKOSConceptsAndPrefLabels(SKOSXLModel model, ARTURIResource rootConcept,
			File outputFile) throws IOException, ModelUpdateException, ModelAccessException {

		BufferedWriter obw = new BufferedWriter(new FileWriter(outputFile));

		ARTURIResourceIterator it = model.listNarrowerConcepts(rootConcept, true, true);
		
		while (it.streamOpen()) {
			ARTURIResource concept = it.getNext();
			obw.write(concept.getURI() + "|");

			writeLabel(model, concept, obw, "en");
			writeLabel(model, concept, obw, "fr");
			writeLabel(model, concept, obw, "es");	

			obw.write("\n");
		}
		it.close();

		obw.close();
	}

	static void writeLabel(SKOSModel model, ARTURIResource concept, Writer w, String lang)
			throws ModelAccessException, IOException {
		ARTLiteral label = model.getPrefLabel(concept, lang, false);
		if (label != null)
			w.write(label.getLabel());
		w.write("|");
	}
	

	public static void listClassesAndLabels(OWLModel model, File outputFile) throws IOException,
			ModelUpdateException {
		System.out.println("still to be implemented for OWLModels");
	}

	public static void listSKOSConceptsAndPrefXLabels(SKOSXLModel model, File outputFile) throws IOException,
			ModelUpdateException {
		System.out.println("still to be implemented for SKOS-XL Models");
	}

}

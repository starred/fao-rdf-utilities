package fao.org.rdfmgmt.export;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fao.org.rdfmgmt.owlart.ModelLoader;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.RDFSModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTLiteralIterator;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.RDFS;
import it.uniroma2.art.owlart.vocabulary.SKOS;

/**
 * Third version (2013/1) of an Agrovoc exporter for TechCDR. This time they asked for a selected export of
 * concepts flooded in a single RDF file.
 * 
 * @author Armando Stellato <a href="mailto:stellato@info.uniroma2.it">stellato@info.uniroma2.it</a>
 * 
 */
public class TechCDRExporter {
	
	protected static Logger logger = LoggerFactory.getLogger(TechCDRExporter.class);

	protected SKOSXLModel skosModel;
	protected RDFModel outputModel;
	HashSet<String> languages;
	protected ARTURIResource fdp_name; // http://data.fao.org/1.0/catalog/properties#name
	protected ARTURIResource fdp_group; // http://data.fao.org/1.0/catalog/properties#group
	protected ARTURIResource fdp_database; // http://data.fao.org/1.0/catalog/properties#database
	protected ARTURIResource cat_concept; // http://data.fao.org/1.0/catalog/types#Concept
	protected ARTURIResource urn_public; // urn:data.fao.org:group:public
	protected ARTURIResource agrovoc_uri; // http://ref.data.fao.org/bee6e167-c10f-4c02-8efd-754bd9d02882
	protected ARTURIResource isPartOfSubvocabulary; // http://aims.fao.org/aos/agrontology#isPartOfSubvocabulary

	protected ARTURIResource outputGraph; // urn:faodata:context:biz:concept:agrovoc

	/**
	 * @param skosFile
	 * @param configFile
	 *            note that this config is used both for the source and output file
	 * @throws IOException
	 * @throws ModelCreationException
	 * @throws BadConfigurationException
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 * @throws UnsupportedRDFFormatException
	 */
	public TechCDRExporter(File skosFile, String configFile) throws IOException, ModelCreationException,
			BadConfigurationException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException {

		logger.info("initializing TechCDRExporter");
		
		skosModel = ModelLoader.loadModel(configFile, SKOSXLModel.class);
		
		logger.info("skosxl model inizialized; loading source skos file");
		
		skosModel.addRDF(skosFile, null, RDFFormat.guessRDFFormatFromFile(skosFile));
		
		logger.info("source skos file loaded");

		outputModel = ModelLoader.loadModel(configFile, RDFSModel.class);

		fdp_name = skosModel.createURIResource("http://data.fao.org/1.0/catalog/properties#name");
		fdp_group = skosModel.createURIResource("http://data.fao.org/1.0/catalog/properties#group");
		fdp_database = skosModel.createURIResource("http://data.fao.org/1.0/catalog/properties#database");
		cat_concept = skosModel.createURIResource("http://data.fao.org/1.0/catalog/types#Concept");
		urn_public = skosModel.createURIResource("urn:faodata:group:public");
		agrovoc_uri = skosModel
				.createURIResource("http://ref.data.fao.org/bee6e167-c10f-4c02-8efd-754bd9d02882");
		isPartOfSubvocabulary = skosModel
				.createURIResource("http://aims.fao.org/aos/agrontology#isPartOfSubvocabulary");

		outputGraph = skosModel.createURIResource("urn:faodata:context:biz:concept:agrovoc");

		languages = new HashSet<String>();
		languages.add("en");
		languages.add("fr");
		languages.add("zh");
		languages.add("ru");
		languages.add("es");
		languages.add("ar");

	}

	public void convert() throws ModelUpdateException, ModelAccessException {

		logger.info("starting data export");
		
		ARTURIResourceIterator conceptIterator = skosModel.listConceptsInScheme(NodeFilters.ANY);

		try {

			while (conceptIterator.streamOpen()) {
				ARTURIResource concept = conceptIterator.getNext();

				boolean atLeastOneLabel = false;

				ARTLiteralIterator it = skosModel.listPrefLabels(concept, false);
				if (it.streamOpen()) {
					atLeastOneLabel = true;
					while (it.streamOpen()) {
						ARTLiteral lbl = it.getNext();
						if (languages.contains(lbl.getLanguage())) {
							outputModel.addTriple(concept, RDFS.Res.LABEL, lbl, outputGraph);
						}
					}
				}
				it.close();

				it = skosModel.listAltLabels(concept, false);
				if (it.streamOpen()) {
					atLeastOneLabel = true;
					while (it.streamOpen()) {
						ARTLiteral lbl = it.getNext();
						if (languages.contains(lbl.getLanguage())) {
							outputModel.addTriple(concept, SKOS.Res.ALTLABEL, lbl, outputGraph);
						}
					}
				}
				it.close();

				// there must be at least one label
				if (atLeastOneLabel) {

					outputModel.addTriple(concept, RDF.Res.TYPE, cat_concept, outputGraph);
					outputModel.addTriple(concept, fdp_name,
							outputModel.createLiteral(concept.getLocalName().replace("_", "-")), outputGraph);
					outputModel.addTriple(concept, fdp_group, urn_public, outputGraph);
					outputModel.addTriple(concept, fdp_database, agrovoc_uri, outputGraph);

					ARTStatementIterator statIt = skosModel.listStatements(concept, isPartOfSubvocabulary,
							NodeFilters.ANY, false);
					while (statIt.streamOpen()) {
						outputModel.addStatement(statIt.getNext(), outputGraph);
					}
					statIt.close();
					
					statIt = skosModel.listStatements(concept, SKOS.Res.BROADER,
							NodeFilters.ANY, false);
					while (statIt.streamOpen()) {
						outputModel.addStatement(statIt.getNext(), outputGraph);
					}
					statIt.close();
					
					statIt = skosModel.listStatements(concept, SKOS.Res.NARROWER,
							NodeFilters.ANY, false);
					while (statIt.streamOpen()) {
						outputModel.addStatement(statIt.getNext(), outputGraph);
					}
					statIt.close();
					
					statIt = skosModel.listStatements(concept, SKOS.Res.RELATED,
							NodeFilters.ANY, false);
					while (statIt.streamOpen()) {
						outputModel.addStatement(statIt.getNext(), outputGraph);
					}
					statIt.close();

				}

			}

		} catch (ModelAccessException e) {
			throw new RuntimeException(e);
		}
		
		logger.info("data conversion concluded");
	}

	public void storeConversion(File exportFile) throws IOException, ModelAccessException,
			UnsupportedRDFFormatException {
		
		logger.info("starting serialization of converted data");
		
		outputModel.writeRDF(exportFile, RDFFormat.TRIG, outputGraph);
		
		logger.info("converted data serialized to file: " + exportFile );
	}

	public static void main(String[] args) throws IOException, ModelCreationException,
			BadConfigurationException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException {

		if (args.length < 3) {
			System.out.println("usage:\n" + TechCDRExporter.class.getName()
					+ " <skosFile> <configFile> <outputFile>");
			return;
		}

		File skosFile = new File(args[0]);
		File exportFile = new File(args[2]);

		TechCDRExporter exporter = new TechCDRExporter(skosFile, args[1]);
		exporter.convert();
		exporter.storeConversion(exportFile);
	}

}

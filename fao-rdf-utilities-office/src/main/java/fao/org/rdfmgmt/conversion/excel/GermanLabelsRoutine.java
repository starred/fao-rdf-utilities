package fao.org.rdfmgmt.conversion.excel;

import fao.org.rdfmgmt.utilities.VOCBENCH_OPS;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLARTModelLoader;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 */
public class GermanLabelsRoutine {

	/**
	 * @param args
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ModelCreationException
	 * @throws BadConfigurationException
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 * @throws UnsupportedRDFFormatException
	 */
	public static void main(String[] args) throws InvalidFormatException, IOException,
			ModelCreationException, BadConfigurationException, ModelUpdateException, ModelAccessException,
			UnsupportedRDFFormatException {

		String lang = "de";

		SKOSXLModel model = OWLARTModelLoader.loadModel(args[1], SKOSXLModel.class);

		// load Agrovoc data
		File agrovocFile = new File(args[2]);
		model.addRDF(agrovocFile, null, RDFFormat.guessRDFFormatFromFile(agrovocFile));

		// RESOURCE LOADING (both input and output)
		Workbook mainWB = WorkbookFactory.create(new File(args[0]));

		VOCBENCH_OPS vbOps = new VOCBENCH_OPS(model);

		// **** PARSING THE EXCEL FILE **** //

		// gets the sheet
		Sheet mainSheet = mainWB.getSheetAt(0);

		// removes the HEADER row
		mainSheet.removeRow(mainSheet.getRow(0));

		// preprocessing to create the table of correspondences between "pref labels" and "concepts"
		for (Row row : mainSheet) {
			String uri = row.getCell(0).getStringCellValue();
			Cell labelCell = row.getCell(2);
			if (labelCell == null) {
				System.err.println(row.getRowNum() + " | " + uri + " has a missing label");
				continue;
			}
			String label = labelCell.getStringCellValue();
			// System.out.println(row.getRowNum() + " | " + uri + ":" + label);
			ARTURIResource concept = model.createURIResource(uri);

			if (!model.hasTriple(concept, NodeFilters.ANY, NodeFilters.ANY, false)) {
				System.err.println(row.getRowNum() + " | ERROR: " + concept
						+ " is not present in the current Agrovoc");
				continue;
			}

			ARTResource prefXLabelRes = model.getPrefXLabel(concept, lang);
			// if there is already a pref label, skip it in any case, but provide most appropriate error msg
			if (prefXLabelRes != null) {
				ARTURIResource prefXLabel = prefXLabelRes.asURIResource();
				ARTLiteral prefLiteral = model.getLiteralForm(prefXLabel);
				String prefLabel = prefLiteral.getLabel();
				if (prefLabel.equalsIgnoreCase(label))
					System.err.println(row.getRowNum() + " | ERROR: " + concept
							+ " has already identical or almost identical preferredLabel: " + prefLabel);
				else
					System.err.println(row.getRowNum() + " | ERROR (VERIFY): " + concept
							+ " has already another prefLabel: " + prefLabel + "; check if " + label
							+ "is not presented already among altLabels or in case, add it as an altLabel");
				continue;
			}

			ARTResourceIterator it = model.listAltXLabels(concept, lang);
			boolean hasAltLabels = it.hasNext();
			ARTLiteral matchingAltLiteral = null;
			while (it.hasNext() && matchingAltLiteral == null) {
				ARTLiteral altLiteral = model.getLiteralForm(it.getNext());
				if (altLiteral.getLabel().equalsIgnoreCase(label))
					matchingAltLiteral = altLiteral;
			}
			it.close();

			if (matchingAltLiteral != null) {
				System.err
						.println(row.getRowNum()
								+ " | ERROR : "
								+ concept
								+ " has already an alternative label: "
								+ matchingAltLiteral
								+ " identical or nearly identical to: "
								+ label
								+ ";so no need to add this one. But pleace notice that the concept, though having an alternative label, has no preferred label for that same language, so check it manually");
				continue;
			}

			if (hasAltLabels) {
				System.err
						.println(row.getRowNum()
								+ " | INFO : "
								+ concept
								+ " has no label similar to the new one: "
								+ label
								+ "though, pleace notice that the concept had alternative labels, but had no preferred label for that same language. However, now it has this one as pref label");
			}

			vbOps.addXLabel(concept, label, lang, true);
		}

		File outputFile = new File(args[3]);
		model.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);

	}
}

package fao.org.rdfmgmt.conversion.excel;

import fao.org.rdfmgmt.utilities.VOCBENCH_OPS;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLARTModelLoader;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTNodeIterator;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 */
public class ItalianLabelsRoutine {


	/**
	 * @param args
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ModelCreationException
	 * @throws BadConfigurationException
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 * @throws UnsupportedRDFFormatException
	 */
	public static void main(String[] args) throws InvalidFormatException, IOException,
			ModelCreationException, BadConfigurationException, ModelUpdateException, ModelAccessException,
			UnsupportedRDFFormatException {
		SKOSXLModel model = OWLARTModelLoader.loadModel(args[1], SKOSXLModel.class);

		// load Agrovoc data
		File agrovocFile = new File(args[2]);
		model.addRDF(agrovocFile, null, RDFFormat.guessRDFFormatFromFile(agrovocFile));

		// RESOURCE LOADING (both input and output)
		Workbook mainWB = WorkbookFactory.create(new File(args[0]));

		VOCBENCH_OPS vbOps = new VOCBENCH_OPS(model);

		// **** PARSING THE EXCEL FILE **** //

		// gets the sheet
		Sheet mainSheet = mainWB.getSheetAt(0);

		// removes the HEADER row
		// mainSheet.removeRow(mainSheet.getRow(0));

		// preprocessing to create the table of correspondences between "pref labels" and "concepts"
		for (Row row : mainSheet) {
			String englishString = row.getCell(1).getStringCellValue();
			ARTLiteral englishLabel = model.createLiteral(englishString, "en");

			Cell italianLabelCell = row.getCell(0);
			if (italianLabelCell == null) {
				System.err.println(row.getRowNum() + " | ERROR: " + englishString
						+ " has a missing italian label");
				continue;
			}
			String italianString = italianLabelCell.getStringCellValue();

			ARTResourceIterator xEnlabelIterator = model.listSubjectsOfPredObjPair(SKOSXL.Res.LITERALFORM,
					englishLabel, false, NodeFilters.MAINGRAPH);
			if (!xEnlabelIterator.hasNext()) {
				System.err.println(row.getRowNum() + " | ERROR: " + englishLabel
						+ " is not present in the current Agrovoc");
				continue;
			}

			ARTResource xEnLabel = xEnlabelIterator.getNext();

			if (xEnlabelIterator.hasNext()) {
				System.err.println(row.getRowNum() + " | ambiguity WARNING: there are two " + englishLabel
						+ " in the current Agrovoc");
			}
			xEnlabelIterator.close();

			ARTResourceIterator conceptIterator = model.listSubjectsOfPredObjPair(SKOSXL.Res.PREFLABEL,
					xEnLabel, false, NodeFilters.MAINGRAPH);
			if (!conceptIterator.hasNext()) {
				conceptIterator.close();
				conceptIterator = model.listSubjectsOfPredObjPair(SKOSXL.Res.ALTLABEL, xEnLabel, false,
						NodeFilters.MAINGRAPH);
			}
			if (!conceptIterator.hasNext()) {
				System.err.println(row.getRowNum() + " | ERROR: there is no concept for " + xEnLabel
						+ " in the current Agrovoc");
				continue;
			}

			
			ARTURIResource concept = conceptIterator.getNext().asURIResource();
			conceptIterator.close();

			ARTNodeIterator xOldPrefIterator = model.listValuesOfSubjPredPair(concept,
					SKOSXL.Res.PREFLABEL, false, NodeFilters.MAINGRAPH);
			ARTLiteral oldPrefLabel = null;
			while (xOldPrefIterator.hasNext()) {
				ARTLiteral lit = model.getLiteralForm(xOldPrefIterator.getNext().asURIResource(), NodeFilters.MAINGRAPH);
				if (lit.getLanguage().equals("it"))
					oldPrefLabel = lit;
			}
			xOldPrefIterator.close();
			
			if (oldPrefLabel!=null) {
				if (oldPrefLabel.getLabel().equals(italianString))
					System.err.println(row.getRowNum() + " | ERROR: concept: " + concept + " has already identical prefLabel: " + italianString);
				else
					System.err.println(row.getRowNum() + " | ERROR (VERIFY): concept: " + concept + " has already prefLabel: " + oldPrefLabel.getLabel() + " which is different from one to be added: " + italianString);
				continue;
			}
			
			vbOps.addXLabel(concept, italianString, "it", true);
		}

		File outputFile = new File(args[3]);
		model.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);

	}
}

package fao.org.rdfmgmt.conversion.excel;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLARTModelLoader;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.SKOS;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * an adapted copy of {@link OracleExcel2SKOSXL} to be used for importing the Soil thesaurus in the context of
 * the AgInfra project<br/>
 * Note, the excel has two big issues:<br/>
 * - reference to broader projects through labels (even more, these are often with different cases)<br/>
 * - missing parents. Some of them are due to different cases<br/>
 * - multiple concepts with same prefLabel (which cause confusion! This should be actually multiple
 * inheritance)<br/>
 * My solution to the above:<br/>
 * - I continue to use the ID column (note, previously I used the LID, something like Label ID) so I continue
 * is just a "relative" statement<br/>
 * - since the label-ID mapping is an hash, it will store the last concept ID it finds for a given repeating
 * label<br/>
 * - I was thinkng abiout using deterministic random codes generated from the labels, but I have anmother
 * simple but big change!<br/>
 * I store all labels lowercare in the mapping, and when retrieving them, I search them lowercase again.<br/>
 * thus everything will clash on a single conceptID and I'm still using a code (one of them manys) coming from
 * the excel<br/>
 * I'm not using a found-concept counter:<br/>
 * - the setPrefLabel for EN and IT should ensure I have no repetitions for them<br/>
 * - I will have only repetitions for the alt labels<br/>
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 */
public class SoilDBExcel2SKOSXL {

	public static final int LID_HEADER_NUM = 0;
	public static final int ID_HEADER_NUM = 1;
	public static final int TYPE_HEADER_NUM = 2; // CONCEPT/TERM
	public static final int BROADER_HEADER_NUM = 3;
	public static final int PREFLABELEN_HEADER_NUM = 4;
	public static final int ALTLABELEN_HEADER_NUM = 5;
	public static final int PREFLABELIT_HEADER_NUM = 6;
	public static final int EXACTMATCH_HEADER_NUM = 7;
	public static final int NARROWMATCH_HEADER_NUM = 8;
	public static final int RELATEDMATCH_HEADER_NUM = 9;
	public static final int BROADMATCH_HEADER_NUM = 10;
	public static final int CLOSEMATCH_HEADER_NUM = 11;
	public static final int DEFINITION_HEADER_NUM = 12;

	public static ARTURIResource createConcept(RDFModel model, int number) {
		return model.createURIResource(model.getDefaultNamespace() + "c_" + number);
	}

	public static ARTURIResource createScheme(RDFModel model, String schemeCode) {
		return model.createURIResource(model.getDefaultNamespace() + "s_" + schemeCode);
	}

	public static void createXLabel(SKOSXLModel model, ARTURIResource subject, int number, Cell labelCell,
			String language, boolean pref) throws ModelUpdateException, ModelAccessException {
		if (labelCell == null)
			return;
		String label = labelCell.getStringCellValue();
		if (label.equals(""))
			return;
		String prefix = pref ? "xl_" : "xl_alt_";
		ARTURIResource xLabel = model.addXLabel(model.getDefaultNamespace() + prefix + language + "_"
				+ number, label, language);
		if (pref)
			model.setPrefXLabel(subject, xLabel, false);
		else
			model.addAltXLabel(subject, xLabel);
	}

	public static void createDefinition(SKOSXLModel model, ARTURIResource subject, int number, Cell defCell,
			String language) throws ModelUpdateException, ModelAccessException {
		if (defCell == null)
			return;
		String definitionLiteral = defCell.getStringCellValue();
		ARTURIResource definitionURI = model.createURIResource(model.getDefaultNamespace() + "def_" + number);
		model.addTriple(subject, SKOS.Res.DEFINITION, definitionURI);
		model.addTriple(definitionURI, RDF.Res.VALUE, model.createLiteral(definitionLiteral, language));
	}

	public static void addAlignedConcepts(RDFModel model, ARTURIResource subject, Cell cell,
			ARTURIResource mappingRel) throws ModelUpdateException {
		if (cell == null)
			return;
		String temp = cell.getStringCellValue();
		String[] uris = temp.split("§");
		for (String uri : uris) {
			if (!uri.equals(""))
				model.addTriple(subject, mappingRel, model.createURIResource(uri));
		}

	}
	
	public static String getProcessedForSearchString(String label) {
		return label.toLowerCase().trim();
	}
	

	/**
	 * @param args
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ModelCreationException
	 * @throws BadConfigurationException
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 * @throws UnsupportedRDFFormatException
	 */
	public static void main(String[] args) throws InvalidFormatException, IOException,
			ModelCreationException, BadConfigurationException, ModelUpdateException, ModelAccessException,
			UnsupportedRDFFormatException {
		SKOSXLModel model = OWLARTModelLoader.loadModel(args[1], SKOSXLModel.class);

		// RESOURCE LOADING (both input and output)
		Workbook mainWB = WorkbookFactory.create(new File(args[0]));

		HashMap<String, ARTURIResource> prefLabelConceptMap = new HashMap<String, ARTURIResource>();

		// HashMap<String, String> srcHash = new HashMap<String, String>();

		// **** PARSING THE EXCEL FILE **** //

		// gets the sheet
		Sheet mainSheet = mainWB.getSheetAt(0);

		// removes the HEADER row
		mainSheet.removeRow(mainSheet.getRow(0));

		// preprocessing to create the table of correspondences between "pref labels" and "concepts"
		// important, see notes in the javadoc of the class
		for (Row row : mainSheet) {
			// System.out.println("row: " + row.getRowNum());
			Cell enLabelCell = row.getCell(PREFLABELEN_HEADER_NUM);
			if (enLabelCell != null) {
				prefLabelConceptMap.put(getProcessedForSearchString(enLabelCell.getStringCellValue()),
						createConcept(model, (int) row.getCell(ID_HEADER_NUM).getNumericCellValue()));
			} else {
				System.err.println("missing enLabel at row: " + (row.getRowNum() + 1));
			}
		}

		System.out.println(prefLabelConceptMap);

		ARTURIResource scheme = model.getDefaultSchema();
		model.addSKOSConceptScheme(scheme);

		for (Row row : mainSheet) {
			int rowNum = row.getRowNum();

			// SKIPPING THIS, NOW THE CONCEPT IS ALWAYS RETRIEVED FROM THE TABLE ON THE BASIS OF THE LABEL-MAP
			// SO I AVOID ALL IDIOSINCRASIES DETAILED IN THE JAVADOC
			// Cell conceptCell = row.getCell(ID_HEADER_NUM);
			// // skip this row if no concept num is found, and report it on the err stream
			// if (conceptCell == null) {
			// System.err.println("no concept reported at row: " + rowNum);
			// continue;
			// }
			// int conceptNumber = (int) conceptCell.getNumericCellValue();
			// ARTURIResource concept = createConcept(model, conceptNumber);

			// generate concept by looking up on the prefLabelConceptMap 
			Cell labelCell = row.getCell(PREFLABELEN_HEADER_NUM);
			// skip this row if no concept num is found, and report it on the err stream
			if (labelCell == null) {
				System.err.println("no en pref label reported at row: " + rowNum);
				continue;
			}
			String enLabel = labelCell.getStringCellValue();			
			ARTURIResource concept = prefLabelConceptMap.get(getProcessedForSearchString(enLabel));					

			Cell broaderConceptThroughLabelCell = row.getCell(BROADER_HEADER_NUM);

			ARTURIResource broaderConcept;
			if (broaderConceptThroughLabelCell == null)
				broaderConcept = NodeFilters.NONE;
			else {
				String broaderConceptString = broaderConceptThroughLabelCell.getStringCellValue();
				if (broaderConceptString.equals("topConcept")) {
					System.out.println("concept: " + concept + " is a topConcept");
					broaderConcept = NodeFilters.NONE;
				} else {
					broaderConcept = prefLabelConceptMap.get(getProcessedForSearchString(broaderConceptString));
					if (broaderConcept == null) {
						broaderConcept = NodeFilters.NONE;
						System.err.println("broader concept identified through label: "
								+ broaderConceptString + " has not been found");
					}
				}
			}

			// creates the concept and adds it to the default scheme
			model.addConcept(concept.getURI(), broaderConcept);
			
			Cell labelIDCell = row.getCell(LID_HEADER_NUM);
			if (labelIDCell == null) {
				System.err.println("no LID reported at row: " + rowNum);
				continue;
			}
			int labelID = (int) labelIDCell.getNumericCellValue();

			createXLabel(model, concept, labelID, row.getCell(PREFLABELEN_HEADER_NUM), "en", true);
			createXLabel(model, concept, labelID, row.getCell(ALTLABELEN_HEADER_NUM), "en", false);
			createXLabel(model, concept, labelID, row.getCell(PREFLABELIT_HEADER_NUM), "it", true);

			addAlignedConcepts(model, concept, row.getCell(EXACTMATCH_HEADER_NUM), SKOS.Res.EXACTMATCH);
			addAlignedConcepts(model, concept, row.getCell(NARROWMATCH_HEADER_NUM), SKOS.Res.NARROWMATCH);
			addAlignedConcepts(model, concept, row.getCell(RELATEDMATCH_HEADER_NUM), SKOS.Res.RELATEDMATCH);
			addAlignedConcepts(model, concept, row.getCell(BROADMATCH_HEADER_NUM), SKOS.Res.BROADMATCH);
			addAlignedConcepts(model, concept, row.getCell(CLOSEMATCH_HEADER_NUM), SKOS.Res.CLOSEMATCH);

			createDefinition(model, concept, labelID, row.getCell(DEFINITION_HEADER_NUM), "en");

		}

		File outputFile = new File(args[2]);
		model.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);

	}
}

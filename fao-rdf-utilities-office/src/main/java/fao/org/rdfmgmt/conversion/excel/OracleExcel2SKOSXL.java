package fao.org.rdfmgmt.conversion.excel;

import fao.org.rdfmgmt.owlart.ModelLoader;
import fao.org.rdfmgmt.vocabularies.FAORGANIZATION;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.RDFS;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.XmlSchema;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * this class has been used to import data for the FAO "Land and Water" thesaurus, which was maintained
 * through some Oracle software which produced these Excel sheets
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 */
public class OracleExcel2SKOSXL {

	public static ARTURIResource createScheme(RDFModel model, String schemeCode) {
		return model.createURIResource(model.getDefaultNamespace() + "s_" + schemeCode);
	}

	public static void createXLabelAndSetAsPref(SKOSXLModel model, ARTURIResource subject, String number,
			String label, String language) throws ModelUpdateException, ModelAccessException {
		ARTURIResource xLabel = model.addXLabel(
				model.getDefaultNamespace() + "xl_" + language + "_" + number, label, language);
		model.setPrefXLabel(subject, xLabel, false);
	}

	public static ARTURIResource createDefinition(SKOSXLModel model, String number)
			throws ModelUpdateException, ModelAccessException {
		return model.createURIResource(model.getDefaultNamespace() + "def_" + number);
	}

	public static void main(String[] args) throws InvalidFormatException, IOException,
			ModelCreationException, BadConfigurationException, ModelUpdateException, ModelAccessException,
			UnsupportedRDFFormatException {
		SKOSXLModel model = ModelLoader.loadModel(args[1], SKOSXLModel.class);

		String excelBaseFileName = args[0];

		// RESOURCE LOADING (both input and output)
		Workbook mainWB = WorkbookFactory.create(new File(excelBaseFileName + "_tbfull.xls"));
		Workbook catWB = WorkbookFactory.create(new File(excelBaseFileName + "cat_tbfull.xls"));
		Workbook srcWB = WorkbookFactory.create(new File(excelBaseFileName + "src_tbfull.xls"));

		HashMap<String, String> srcHash = new HashMap<String, String>();

		// **** PARSING CATEGORY/SCHEME EXCEL FILE **** //
		// gets the sheet
		Sheet catSheet = catWB.getSheetAt(0);
		// removes the HEADER row
		catSheet.removeRow(catSheet.getRow(0));
		for (Row row : catSheet) {
			ARTURIResource scheme = createScheme(model, row.getCell(4).getStringCellValue());
			model.addTriple(scheme, RDF.Res.TYPE, SKOS.Res.CONCEPTSCHEME);
			createXLabelAndSetAsPref(model, scheme, row.getCell(4).getStringCellValue(), row.getCell(1)
					.getStringCellValue(), "en");
			createXLabelAndSetAsPref(model, scheme, row.getCell(4).getStringCellValue(), row.getCell(2)
					.getStringCellValue(), "fr");
			createXLabelAndSetAsPref(model, scheme, row.getCell(4).getStringCellValue(), row.getCell(3)
					.getStringCellValue(), "es");
		}

		// **** PARSING SUBJECTS EXCEL FILE **** //
		// gets the sheet
		Sheet srcSheet = srcWB.getSheetAt(0);
		// removes the HEADER row
		srcSheet.removeRow(srcSheet.getRow(0));
		for (Row row : srcSheet) {
			srcHash.put(row.getCell(0).getStringCellValue(), row.getCell(1).getStringCellValue());
		}

		// **** PARSING MAIN EXCEL FILE **** //

		// gets the sheet
		Sheet sheet1 = mainWB.getSheetAt(0);
		// removes the HEADER row
		sheet1.removeRow(sheet1.getRow(0));
		for (Row row : sheet1) {
			int rowNum = row.getRowNum() + 1;
			String conceptURIString = model.getDefaultNamespace() + "c_" + rowNum;
			model.addConcept(conceptURIString, NodeFilters.NONE);
			ARTURIResource skosConcept = model.createURIResource(conceptURIString);

			// LABELS CREATION
			createXLabelAndSetAsPref(model, skosConcept, "" + rowNum, row.getCell(1).getStringCellValue(),
					"en");
			createXLabelAndSetAsPref(model, skosConcept, "" + rowNum, row.getCell(2).getStringCellValue(),
					"fr");
			createXLabelAndSetAsPref(model, skosConcept, "" + rowNum, row.getCell(3).getStringCellValue(),
					"es");

			// getting schemes from subject column
			String[] schemesLocalNames = row.getCell(4).getStringCellValue().split(",");
			for (String schemeLocalName : schemesLocalNames) {
				ARTURIResource scheme = createScheme(model, schemeLocalName);
				model.addTriple(skosConcept, SKOS.Res.INSCHEME, scheme);
				model.addTriple(skosConcept, SKOS.Res.TOPCONCEPTOF, scheme);
			}

			// SOURCES CONVERSION
			String source1 = row.getCell(5).getStringCellValue();
			String source2 = row.getCell(6).getStringCellValue();

			if (!source1.equals(""))
				model.addTriple(skosConcept, FAORGANIZATION.Res.SOURCE,
						model.createLiteral(srcHash.get(source1), XmlSchema.Res.STRING));
			if (!source2.equals(""))
				model.addTriple(skosConcept, FAORGANIZATION.Res.SOURCE,
						model.createLiteral(srcHash.get(source2), XmlSchema.Res.STRING));

			// DEFINITIONS CONVERSION
			String def_en = row.getCell(7).getStringCellValue();
			String def_fr = row.getCell(13).getStringCellValue();
			String def_es = row.getCell(19).getStringCellValue();

			ARTURIResource def = createDefinition(model, rowNum + "");
			model.addTriple(skosConcept, SKOS.Res.DEFINITION, def);
			if (!def_en.equals(""))
				model.addTriple(def, RDF.Res.VALUE, model.createLiteral(def_en, "en"));
			if (!def_fr.equals(""))
				model.addTriple(def, RDF.Res.VALUE, model.createLiteral(def_fr, "fr"));
			if (!def_es.equals(""))
				model.addTriple(def, RDF.Res.VALUE, model.createLiteral(def_es, "es"));

			// UNITS CONVERSION
			String unit_en = row.getCell(8).getStringCellValue();
			String unit_fr = row.getCell(14).getStringCellValue();
			String unit_es = row.getCell(20).getStringCellValue();

			if (!unit_en.equals(""))
				model.addTriple(skosConcept, FAORGANIZATION.Res.UNIT, model.createLiteral(unit_en, "en"));
			if (!unit_fr.equals(""))
				model.addTriple(skosConcept, FAORGANIZATION.Res.UNIT, model.createLiteral(unit_fr, "fr"));
			if (!unit_es.equals(""))
				model.addTriple(skosConcept, FAORGANIZATION.Res.UNIT, model.createLiteral(unit_es, "es"));

			// COMMENTS CONVERSION
			String comment_en = row.getCell(9).getStringCellValue();
			String comment_fr = row.getCell(15).getStringCellValue();
			String comment_es = row.getCell(21).getStringCellValue();

			if (!comment_en.equals(""))
				model.addTriple(skosConcept, SKOS.Res.NOTE, model.createLiteral(comment_en, "en"));
			if (!comment_fr.equals(""))
				model.addTriple(skosConcept, SKOS.Res.NOTE, model.createLiteral(comment_fr, "fr"));
			if (!comment_es.equals(""))
				model.addTriple(skosConcept, SKOS.Res.NOTE, model.createLiteral(comment_es, "es"));

			// OTHERINFO CONVERSION
			String otherinfo_en = row.getCell(10).getStringCellValue();
			String otherinfo_fr = row.getCell(16).getStringCellValue();
			String otherinfo_es = row.getCell(22).getStringCellValue();

			if (!otherinfo_en.equals(""))
				model.addTriple(skosConcept, SKOS.Res.EDITORIALNOTE, model.createLiteral(otherinfo_en, "en"));
			if (!otherinfo_fr.equals(""))
				model.addTriple(skosConcept, SKOS.Res.EDITORIALNOTE, model.createLiteral(otherinfo_fr, "fr"));
			if (!otherinfo_es.equals(""))
				model.addTriple(skosConcept, SKOS.Res.EDITORIALNOTE, model.createLiteral(otherinfo_es, "es"));

			// LINKLAB CONVERSION
			String linklab1_en = row.getCell(11).getStringCellValue();
			String linklab1_fr = row.getCell(17).getStringCellValue();
			String linklab1_es = row.getCell(23).getStringCellValue();

			String linklab2_en = row.getCell(12).getStringCellValue();
			String linklab2_fr = row.getCell(18).getStringCellValue();
			String linklab2_es = row.getCell(24).getStringCellValue();

			if (!linklab1_en.equals(""))
				model.addTriple(skosConcept, FAORGANIZATION.Res.LINKLAB,
						model.createLiteral(linklab1_en, "en"));
			if (!linklab1_fr.equals(""))
				model.addTriple(skosConcept, FAORGANIZATION.Res.LINKLAB,
						model.createLiteral(linklab1_fr, "fr"));
			if (!linklab1_es.equals(""))
				model.addTriple(skosConcept, FAORGANIZATION.Res.LINKLAB,
						model.createLiteral(linklab1_es, "es"));

			if (!linklab2_en.equals(""))
				model.addTriple(skosConcept, FAORGANIZATION.Res.LINKLAB,
						model.createLiteral(linklab2_en, "en"));
			if (!linklab2_fr.equals(""))
				model.addTriple(skosConcept, FAORGANIZATION.Res.LINKLAB,
						model.createLiteral(linklab2_fr, "fr"));
			if (!linklab2_es.equals(""))
				model.addTriple(skosConcept, FAORGANIZATION.Res.LINKLAB,
						model.createLiteral(linklab2_es, "es"));

			// LINKs CONVERSION
			String link1 = row.getCell(25).getStringCellValue();
			String link2 = row.getCell(26).getStringCellValue();

			if (!link1.equals(""))
				model.addTriple(skosConcept, RDFS.Res.SEEALSO,
						model.createLiteral(link1, XmlSchema.Res.STRING));
			if (!link2.equals(""))
				model.addTriple(skosConcept, RDFS.Res.SEEALSO,
						model.createLiteral(link2, XmlSchema.Res.STRING));

			// OWNERs CONVERSION
			String[] owners1 = row.getCell(29).getStringCellValue().split(",");
			String[] owners2 = row.getCell(30).getStringCellValue().split(",");

			// the triples with rdf:type will be written repeatedly lot of times, but RDF admits no
			// repetitions, so it's not
			// a problem, and it's not worth parsing the values before and storing them in a hash
			for (String owner1 : owners1) {
				ARTURIResource owner = model.createURIResource(model.getDefaultNamespace() + owner1);
				model.addTriple(skosConcept, FAORGANIZATION.Res.OWNER_PROP, owner);
				model.addTriple(owner, RDF.Res.TYPE, FAORGANIZATION.Res.OWNER);
			}

			for (String owner2 : owners2) {
				ARTURIResource owner = model.createURIResource(model.getDefaultNamespace() + owner2);
				model.addTriple(skosConcept, FAORGANIZATION.Res.OWNER_PROP, owner);
				model.addTriple(owner, RDF.Res.TYPE, FAORGANIZATION.Res.OWNER);
			}

		}

		File outputFile = new File(args[2]);
		model.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);

		// for (Cell cell : row) {
		// CellReference cellRef = new CellReference(row.getRowNum(), cell.getColumnIndex());
		// System.out.print(cellRef.formatAsString());
		// System.out.print(" - ");
		//
		// switch (cell.getCellType()) {
		// case Cell.CELL_TYPE_STRING:
		// System.out.println(cell.getRichStringCellValue().getString());
		// break;
		// case Cell.CELL_TYPE_NUMERIC:
		// if (DateUtil.isCellDateFormatted(cell)) {
		// System.out.println(cell.getDateCellValue());
		// } else {
		// System.out.println(cell.getNumericCellValue());
		// }
		// break;
		// case Cell.CELL_TYPE_BOOLEAN:
		// System.out.println(cell.getBooleanCellValue());
		// break;
		// case Cell.CELL_TYPE_FORMULA:
		// System.out.println(cell.getCellFormula());
		// break;
		// default:
		// System.out.println();
		// }
		// }

	}
}

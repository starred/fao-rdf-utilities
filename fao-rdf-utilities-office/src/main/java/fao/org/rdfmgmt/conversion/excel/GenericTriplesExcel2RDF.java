package fao.org.rdfmgmt.conversion.excel;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.io.RDFNodeSerializer;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFSModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * This class is a generic Excel2RDF utility. The format of the excel file is suppose to be a plain sequence
 * of rows where each row contains an RDF triple, serialized in NT format (see {@link RDFNodeSerializer})
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 */
public class GenericTriplesExcel2RDF {

	public static void main(String[] args) throws InvalidFormatException, IOException,
			ModelCreationException, BadConfigurationException, ModelUpdateException, ModelAccessException,
			UnsupportedRDFFormatException {
		RDFSModel model = ModelLoader.loadModel(args[1], RDFSModel.class);

		String excelFileName = args[0];

		// RESOURCE LOADING (both input and output)
		Workbook mainWB = WorkbookFactory.create(new File(excelFileName));

		// **** PARSING EXCEL FILE **** //

		// gets the sheet
		Sheet sheet1 = mainWB.getSheetAt(0);

		// removes the HEADER row
		// sheet1.removeRow(sheet1.getRow(0));
		for (Row row : sheet1) {
			ARTResource subject = RDFNodeSerializer
					.createResource(model, row.getCell(0).getStringCellValue());
			ARTURIResource predicate = RDFNodeSerializer.createURI(model, row.getCell(1).getStringCellValue());
			ARTNode object = RDFNodeSerializer.createNode(model, row.getCell(2).getStringCellValue());

			model.addTriple(subject, predicate, object);

			/*
			 * if (!source1.equals("")) model.addTriple(skosConcept, FAORGANIZATION.Res.SOURCE,
			 * model.createLiteral(srcHash.get(source1), XmlSchema.Res.STRING) ); if (!source2.equals(""))
			 * model.addTriple(skosConcept, FAORGANIZATION.Res.SOURCE,
			 * model.createLiteral(srcHash.get(source2), XmlSchema.Res.STRING) );
			 */

		}

		// **** GENERATING OUTPUT FILE **** //

		File outputFile = new File(args[2]);
		model.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);

	}
}

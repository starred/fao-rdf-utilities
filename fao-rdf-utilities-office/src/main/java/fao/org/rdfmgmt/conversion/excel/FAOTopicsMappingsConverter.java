package fao.org.rdfmgmt.conversion.excel;

import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.RDFSModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.vocabulary.SKOS;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * This is a dedicated RDF exporter for the FAOTopics mappings produced by Gudrun in an excel file. The bad
 * thing about these mappings is that they have a non-scalable format, where each FAOTopic is reported only in
 * a row, and for each row, there can be up to four mappings, reported in different columns
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 */
public class FAOTopicsMappingsConverter {

	public static void main(String[] args) throws InvalidFormatException, IOException,
			ModelCreationException, BadConfigurationException, ModelUpdateException, ModelAccessException,
			UnsupportedRDFFormatException {
		RDFSModel model = ModelLoader.loadModel(args[1], RDFSModel.class);

		String excelFileName = args[0];

		// RESOURCE LOADING (both input and output)
		Workbook mainWB = WorkbookFactory.create(new File(excelFileName));

		// **** PARSING EXCEL FILE **** //

		// gets the sheet
		Sheet sheet1 = mainWB.getSheetAt(0);

		// removes the HEADER row
		sheet1.removeRow(sheet1.getRow(0));

		for (Row row : sheet1) {
			// create TOPIC URI from column B (1)						
			Cell FAOTopicCell = row.getCell(1);
			if (FAOTopicCell==null)
				break;
			String FAOTopicURI = FAOTopicCell.getStringCellValue();
			if (FAOTopicURI.isEmpty()) {
				System.out.println("empty line");
				continue;
			}
			System.out.println("processing row: " + row.getRowNum() + " with FAOTopic: " + FAOTopicURI);
			ARTURIResource FAOTopic = model.createURIResource(FAOTopicURI);

			try {
			ARTStatement mapping1 = parseMapping(model, FAOTopic, row, 2);
			if (mapping1 != null) {
				model.addStatement(mapping1);
				ARTStatement mapping2 = parseMapping(model, FAOTopic, row, 5);
				if (mapping2 != null) {
					model.addStatement(mapping2);
					ARTStatement mapping3 = parseMapping(model, FAOTopic, row, 8);
					if (mapping3 != null) {
						model.addStatement(mapping3);
						ARTStatement mapping4 = parseMapping(model, FAOTopic, row, 11);
						if (mapping4 != null) {
							model.addStatement(mapping4);
						}
					}
				}
			}
			} catch (Exception e) {
				System.err.println("exception on row: " + row.getRowNum() + " with FAOTopic: " + FAOTopicURI);
				e.printStackTrace();
				break;
			}
			

			/*
			 * if (!source1.equals("")) model.addTriple(skosConcept, FAORGANIZATION.Res.SOURCE,
			 * model.createLiteral(srcHash.get(source1), XmlSchema.Res.STRING) ); if (!source2.equals(""))
			 * model.addTriple(skosConcept, FAORGANIZATION.Res.SOURCE,
			 * model.createLiteral(srcHash.get(source2), XmlSchema.Res.STRING) );
			 */

		}

		// **** GENERATING OUTPUT FILE **** //

		File outputFile = new File(args[2]);
		model.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);

	}

	public static ARTStatement parseMapping(RDFSModel model, ARTURIResource subject, Row row, int columnIndex) {
		System.out.println( "\t cell: " + row.getCell(columnIndex + 2 )  );
		Cell cell = row.getCell(columnIndex + 2);
		if (cell==null)
			return null;
		String mappedURI = row.getCell(columnIndex + 2).getStringCellValue();
		if (!mappedURI.equals("")) {
			ARTURIResource object = model.createURIResource(mappedURI);
			ARTURIResource predicate = parseMappingRelation(model, row, columnIndex);
			return model.createStatement(subject, predicate, object);
		}
		return null;
	}

	public static ARTURIResource parseMappingRelation(RDFModel model, Row row, int columnIndex) {

		String mappingRel = row.getCell(columnIndex).getStringCellValue();

		if (mappingRel.equals("exactMatch"))
			return SKOS.Res.EXACTMATCH;
		if (mappingRel.equals("broadMatch"))
			return SKOS.Res.BROADMATCH;
		if (mappingRel.equals("narrowMatch"))
			return SKOS.Res.NARROWMATCH;
		if (mappingRel.equals("closeMatch"))
			return SKOS.Res.CLOSEMATCH;
		if (mappingRel.equals("relatedMatch"))
			return SKOS.Res.RELATEDMATCH;

		throw new IllegalArgumentException("cannot parse relation: " + mappingRel + " at row: "
				+ row.getRowNum() + ", column :" + columnIndex);

	}
}

package fao.org.rdfmgmt.conversion.excel;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 */
public class ExtractingLinks {

	/**
	 * @param args
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ModelCreationException
	 * @throws BadConfigurationException
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 * @throws UnsupportedRDFFormatException
	 */
	public static void main(String[] args) throws InvalidFormatException, IOException,
			ModelCreationException, BadConfigurationException, ModelUpdateException, ModelAccessException,
			UnsupportedRDFFormatException {

		if (args.length<1)
			System.err.println("you must pass an input excel file as 1st argument from command line!");
		
		
		// RESOURCE LOADING (both input and output)
		Workbook mainWB = WorkbookFactory.create(new File(args[0]));

		// **** PARSING THE EXCEL FILE **** //

		// gets the sheet
		Sheet mainSheet = mainWB.getSheetAt(0);

		// removes the HEADER row
		mainSheet.removeRow(mainSheet.getRow(0));

		// preprocessing to create the table of correspondences between "pref labels" and "concepts"
		for (Row row : mainSheet) {
			Hyperlink link;
			String address=null;
			Cell cell = row.getCell(0);
			if (cell!=null) {
				 link = cell.getHyperlink();
				 if (link!=null)
					 address=link.getAddress();
			}
			if (address!=null)
				System.out.println(address);
			else
				System.out.println();
		}
	}

}

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ingest>
    <create-resources workspace="agrovoc" default-resource-owner="agrovoc" default-collection="terms">

        <res name="some_unique_resource_name_1" UUID=�a009e1bf-f924-451f-b4e7-fe8674eb2616� >
            <ds type="rdf" ref="file:///dataStreams/some_xml_file_1.xml" mime-type="application/xml" 
class="XML" />
        </res>

        <res name="some_unique_resource_name_2" UUID=�91ca6e9d-5082-4ca9-9471-b2b714146c3e�>
            <ds type="rdf" ref="file:///dataStreams/some_xml_file_2.xml" mime-type="application/xml" 
class="XML" />
        </res>

        <res name="some_unique_resource_name_3" UUID=�8edb86be-7563-4420-9775-57bd6600466f�>
            <ds type="rdf" ref="http://some_server/some_path/some_xml_file_3.xml" mime-type="application/xml" 
class="XML"/>
        </res>

�
</ingest>

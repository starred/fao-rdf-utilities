package fao.org.rdfmgmt.filters;

import fao.org.rdfmgmt.conversion.ConversionException;
import fao.org.rdfmgmt.owlart.ModelLoader;
import fao.org.rdfmgmt.vocabularies.BIBO;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 *         this class is a simple utility for producing a list of couples of the form
 *         &lt;URI, ISSN&gt; from the JAD repository
 * 
 */
public class JAD_ISSNURI_Reporter {

	protected static Logger logger = LoggerFactory.getLogger(JAD_ISSNURI_Reporter.class);

	public static void main(String[] args) throws ModelUpdateException, ModelAccessException, IOException,
			UnsupportedRDFFormatException, ModelCreationException, BadConfigurationException,
			ConversionException {
		if (args.length < 2) {
			System.out.println("usage:\n" + JAD_ISSNURI_Reporter.class.getName()
					+ " <owlARTConfig> <serialsFile> <mappingsFile>");
			return;
		}

		logger.info("SETTING UP THE EXTRACTION OF ISSN - URI MAPPINGS :\n");

		// OWL ART SKOSXLMODEL LOADING
		OWLModel owlModel = ModelLoader.loadModel(args[0], OWLModel.class);
		File serialsFile = new File(args[1]);
		owlModel.addRDF(serialsFile, null, RDFFormat.guessRDFFormatFromFile(serialsFile));

		String mappingsFile = args[2];

		FileWriter fw = new FileWriter(mappingsFile);

		ARTStatementIterator it = owlModel.listStatements(NodeFilters.ANY, BIBO.Res.ISSN, NodeFilters.ANY,
				false);
		while (it.streamOpen()) {
			ARTStatement stat = it.getNext();
			String issn = stat.getObject().asLiteral().getLabel();
			if (issn.endsWith("-X"))
				issn = issn.replace("-X", "X");
			if (issn.length() == 8)
				issn = issn.substring(0, 4) + "-" + issn.substring(4);
			fw.write(stat.getSubject().asURIResource().getURI() + "	|	" + issn + "\n");
		}
		it.close();
		fw.close();
	}

}

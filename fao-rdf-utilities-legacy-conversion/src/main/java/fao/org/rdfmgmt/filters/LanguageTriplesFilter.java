package fao.org.rdfmgmt.filters;

import fao.org.rdfmgmt.conversion.AOSVocabulary;
import fao.org.rdfmgmt.owlart.ModelLoader;
import fao.org.rdfmgmt.vocabularies.AOS;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.vocabulary.RDFS;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;

/**
 * an utility for exporting a dataset by including only triples in certain languages.<br/>
 * Note that this contains reference to Agrovoc specific properties (which have maybe changed, as they are old
 * AOS ones)
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 */
public class LanguageTriplesFilter {

	HashSet<ARTURIResource> skosxlLabelProperties = new HashSet<ARTURIResource>();
	HashSet<String> tagFilter = new HashSet<String>();
	SKOSXLModel model;
	ARTURIResource HAS_DEFINITION;

	LanguageTriplesFilter(SKOSXLModel model, String... langs) {
		// maybe not useful, literalform is the discriminant
		skosxlLabelProperties.add(SKOSXL.Res.PREFLABEL);
		skosxlLabelProperties.add(SKOSXL.Res.ALTLABEL);
		skosxlLabelProperties.add(SKOSXL.Res.HIDDENLABEL);

		tagFilter.addAll(Arrays.asList(langs));
		this.model = model;
		HAS_DEFINITION = model.createURIResource(AOS.defNamespace + AOSVocabulary.hasDefinitionName);

		System.out.println(langs);

	}

	public void filterLanguageTriples() throws ModelAccessException, ModelUpdateException {
		ARTStatementIterator it = model.listStatements(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY,
				false);
		while (it.hasNext()) {
			ARTStatement stat = it.getNext();
			ARTNode obj = stat.getObject();
			if (obj.isLiteral() && !tagFilter.contains(obj.asLiteral().getLanguage())) {
				ARTURIResource pred = stat.getPredicate();

				// XLabel removal
				if (pred.equals(SKOSXL.Res.LITERALFORM)) {
					System.err.println("deleting xlabel: " + stat.getSubject());
					model.deleteXLabel(stat.getSubject());
				}

				else if (pred.equals(RDFS.Res.LABEL)) {
					ARTResource subj = stat.getSubject();

					// case: the subj is a aos:definition, which has label on a reified definition
					// this wont' be anymore in VOCBENCH 2.x
					if (model.hasTriple(NodeFilters.ANY, HAS_DEFINITION, subj, false)) {
						model.deleteTriple(NodeFilters.ANY, NodeFilters.ANY, subj);
						model.deleteTriple(subj, NodeFilters.ANY, NodeFilters.ANY);
					}

					// case: the triple was a simple connection between a concept and a label
					// we fall in the last case, where the stat is simply removed
				}

				// if there's no known pattern of reified label, then simply the statement is deleted, with no
				// further action
				else
					model.deleteStatement(stat);

			}
		}
		it.close();
	}

	public static void main(String[] args) throws Exception {

		if (args.length < 3) {
			System.out
					.println("usage:\n"
							+ "java "
							+ LanguageTriplesFilter.class
							+ " <configfile> <inputRDFFile> <outputRDFFile> <language tag> [language tag [language tag]] \n"
							+ "\t<configfile>  : config file for the chosen RDFModel Implementation \n"
							+ "\t<inputRDFFile> : file to be loaded \n"
							+ "\t<outputRDFFile> the output file, stripped of unwanted language properties \n"
							+ "\t a list of language tags the properties of which will be maintained in the output (at least one)");
			return;
		}

		String configFile = args[0];
		String inputFileName = args[1];
		String outputFileName = args[2];

		// OWL ART SKOSXLMODEL LOADING
		SKOSXLModel model = ModelLoader.loadModel(configFile, SKOSXLModel.class);

		File inputFile = new File(inputFileName);
		File outputFile = new File(outputFileName);

		System.out.println("adding RDF data to the model");
		model.addRDF(inputFile, null, RDFFormat.guessRDFFormatFromFile(inputFile));
		System.out.println("RDF data uploaded into the model");

		String[] langs = new String[args.length - 3];
		for (int i = 0; i < langs.length; i++) {
			langs[i] = args[i + 3];
		}

		LanguageTriplesFilter filt = new LanguageTriplesFilter(model, langs);

		filt.filterLanguageTriples();

		model.writeRDF(outputFile, RDFFormat.RDFXML_ABBREV, NodeFilters.MAINGRAPH);

	}

}

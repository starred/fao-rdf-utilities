package fao.org.rdfmgmt.conversion;

public class ConversionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -100401027321999319L;

    public ConversionException(String message, Throwable cause) {
        super(message, cause);
    }
	
	
}

package fao.org.rdfmgmt.conversion;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.protegeimpl.factory.ProtegeDBModelLoader;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import edu.stanford.smi.protege.model.Project;
import edu.stanford.smi.protegex.owl.model.OWLIndividual;
import edu.stanford.smi.protegex.owl.model.RDFResource;
import fao.org.rdfmgmt.RunConversion;
import fao.org.rdfmgmt.owlart.ModelLoader;

public class ASFA_ProtDB2SKOSXLConverter extends FAOProtDB2SKOSXLConverter {

	/**
	 * 
	 * 
	 * @param protegeProject
	 * @param skosXLModel
	 * @param discardRedundants
	 * @param forceTypedConversions
	 */
	public ASFA_ProtDB2SKOSXLConverter(Project protegeProject, SKOSXLModel skosXLModel,
			boolean discardRedundants, boolean forceTypedConversions) {
		super(protegeProject, skosXLModel, discardRedundants, true, forceTypedConversions, false);
	}

	public static void main(String[] args) throws ModelUpdateException, ModelAccessException, IOException,
			UnsupportedRDFFormatException, ModelCreationException, BadConfigurationException,
			ConversionException {
		if (args.length < 3) {
			System.out.println("usage:\n" + ASFA_ProtDB2SKOSXLConverter.class.getName()
					+ " <ProtegeDBConfigFile> <owlARTConfig> <conversionOutputFile> [discardRedundants [forceTypedConversions]]");
			return;
		}

		logger.info("SETTING UP CONVERSION:\n");

		// PROTEGE PROJECT LOADING
		ProtegeDBModelLoader loader = new ProtegeDBModelLoader();
		Project protProject = loader.loadProtegeProject(args[0]);

		// OWL ART SKOSXLMODEL LOADING
		SKOSXLModel skosXLModel = ModelLoader.loadModel(args[1], SKOSXLModel.class);

		String ntFile = args[2];

		boolean discardRedundants = true;
		if (args.length > 3) {
			discardRedundants = Boolean.parseBoolean(args[3]);
		}
		logger.info("discardRedundants: " + discardRedundants);

		ASFA_ProtDB2SKOSXLConverter converter = new ASFA_ProtDB2SKOSXLConverter(protProject, skosXLModel,
				discardRedundants, true);

		String rdfFile = ntFile.replace(".nt", ".rdf");
		File ntOutputFile = new File(ntFile);
		File rdfOutputFile = new File(rdfFile);
		Date d1 = new Date();
		System.out.println("CONVERSION STARTED AT : " + d1);
		converter.convert();
		Date d2 = new Date();
		System.out.println("CONVERSION ENDED AT : " + d2);
		System.out.println("TOTAL TIME FOR CONVERSION: " + RunConversion.getTimeDifference(d2, d1));

		// **************
		// MAPPINGS
		// **************
		/*
		 * System.out.println("Adding 50 triples linking to EUROVOC: " + RunConversion.getTimeDifference(new
		 * Date(), d1));
		 * 
		 * 
		 * AddTriples.addTriples(skosXLModel, new File("Input/eurovoc_links.txt"));
		 * System.out.println("EUROVOC skos:matches ADDED: " + RunConversion.getTimeDifference(new Date(),
		 * d1));
		 */

		// **************
		// EXPORTS
		// **************
		converter.saveConversion(ntOutputFile);
		System.out.println("model exported to NT file: " + RunConversion.getTimeDifference(new Date(), d1));
		converter.saveConversion(rdfOutputFile);
		System.out.println("model exported to RDF/XML file: "
				+ RunConversion.getTimeDifference(new Date(), d1));

		System.out.println("TOTAL TIME : " + RunConversion.getTimeDifference(new Date(), d1));
	}

	public void convertExceptionProperties(OWLIndividual ind, ARTURIResource convertedConcept) {
		// No need for special conversions

	}

	protected ARTURIResource convertResourceURI(RDFResource owlInstance) {
		String conceptLocalName = owlInstance.getLocalName().replace("i_", "c_");
		// TODO beware that this does not preserve the URI of the target concept, this is ok if there are
		// no references to external data, but can break them if there are
		return rdfModel.createURIResource(rdfModel.getDefaultNamespace() + conceptLocalName);
		//return rdfModel.createURIResource(owlInstance.getNamespace() + conceptLocalName);
	}

}

package fao.org.rdfmgmt.conversion;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.vocabulary.XmlSchema;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.stanford.smi.protege.model.Project;
import edu.stanford.smi.protegex.owl.model.OWLDatatypeProperty;
import edu.stanford.smi.protegex.owl.model.OWLIndividual;
import edu.stanford.smi.protegex.owl.model.OWLNamedClass;
import edu.stanford.smi.protegex.owl.model.OWLObjectProperty;
import edu.stanford.smi.protegex.owl.model.OWLProperty;
import edu.stanford.smi.protegex.owl.model.RDFProperty;
import edu.stanford.smi.protegex.owl.model.RDFResource;
import edu.stanford.smi.protegex.owl.model.RDFSLiteral;
import edu.stanford.smi.protegex.owl.model.RDFSNames;
import fao.org.rdfmgmt.RunConversion;
import fao.org.rdfmgmt.mappings.LoadMappingTriples;
import fao.org.rdfmgmt.mappings.ParseImportedTripleException;
import fao.org.rdfmgmt.owlart.ModelLoader;
import fao.org.rdfmgmt.protege.ProtegeModelLoader;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * @deprecated use {@link FAOProtDB2RDFConverter} instead, using the appropriate abstract subclass (OWL or
 *             SKOS {@link FAOProtDB2SKOSXLConverter} conversion) and the concrete sub-sub class for the
 *             specific vocabulary
 */
public class OWL2SKOSConverter {

	public static OWLProperty rdfsComment;
	public static OWLProperty rdfsLabel;

	public static enum ResType {
		label, instance
	};

	protected static Logger logger = LoggerFactory.getLogger(OWL2SKOSConverter.class);

	Project protegeProject;
	edu.stanford.smi.protegex.owl.model.OWLModel protOWLModel;
	SKOSXLModel skosXLModel;
	HashSet<OWLNamedClass> convertedConcepts;

	public OWL2SKOSConverter(Project protegeProject, SKOSXLModel skosXLModel) {
		this.protegeProject = protegeProject;
		this.skosXLModel = skosXLModel;
		protOWLModel = (edu.stanford.smi.protegex.owl.model.OWLModel) protegeProject.getKnowledgeBase();

		AOSVocabulary.initialize(protOWLModel);

		convertedConcepts = new HashSet<OWLNamedClass>();

		logger.info("cdomainConcept: " + AOSVocabulary.c_domain_concept);

		rdfsLabel = protOWLModel.getOWLProperty(RDFSNames.Slot.LABEL);
		rdfsComment = protOWLModel.getOWLProperty(RDFSNames.Slot.COMMENT);

		// logger.info("hasLexicalization: " + hasLexicalization.getBrowserText());
	}

	public void convert() throws ModelUpdateException, ModelAccessException {

		AOSConversionResourceLists.initialize(protOWLModel, true);

		// **************************************
		// COUNTING ALL CONCEPTS
		// *************************************

		Date d0 = new Date();
		int totalConcepts = countTotalConcepts();

		// **************************************
		// CONCEPTS CONVERSION
		// *************************************

		// does a first round for rootConcepts, since they need to be reported as topConcepts for the above
		// scheme
		Collection<OWLNamedClass> rootConcepts = convertRootConcepts();
		int count = rootConcepts.size();
		// recursive descent along the tree, converting all concepts
		for (OWLNamedClass cls : rootConcepts) {
			count = exploreConcept(cls, 0, count, totalConcepts, d0);
		}
	}

	/**
	 * @param ind
	 *            the instance representation of a concept in the AGROVOC OWL Vocabulary (see AGROVOC OWL
	 *            Vocabulary for details)
	 * @param skosConcept
	 *            the representation of the same concept in SKOS API
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	@SuppressWarnings("unchecked")
	public void convertInstance(OWLIndividual ind, ARTURIResource skosConcept) throws ModelUpdateException,
			ModelAccessException {

		// **************************************
		// CONCEPT PROPERTIES CONVERSION
		// *************************************

		// convertDatatypePlainLiteralLanguageProperty(ind, skosConcept, rdfsLabel);
		// convertDatatypePlainLiteralLanguageProperty(ind, skosConcept, rdfsComment);

		// System.out.print("|its properties...");

		for (OWLObjectProperty prop : AOSConversionResourceLists.conceptToConcept) {
			convertObjectProperty(ind, skosConcept, prop, ResType.instance);
		}

		for (OWLDatatypeProperty prop : AOSConversionResourceLists.conceptToPlainLiterals) {
			convertDatatypePlainLiteralLanguageProperty(ind, skosConcept, prop);
		}

		for (OWLDatatypeProperty prop : AOSConversionResourceLists.conceptToTypeStrings) {
			convertDatatypeStringTypedProperty(ind, skosConcept, prop);
		}

		for (OWLDatatypeProperty prop : AOSConversionResourceLists.conceptToTypeDates) {
			convertDatatypeDateTypedProperty(ind, skosConcept, prop);
		}

		// **************************************
		// ENTITY ANNOTATIONS CONVERSION
		// *************************************

		// System.out.print("|its entity annotations...");

		// HAS DEFINITION CONVERSION
		Collection<OWLIndividual> hasDefinitions = ind.getPropertyValues(AOSVocabulary.hasDefinition);
		for (OWLIndividual definition : hasDefinitions) {
			ARTURIResource newDef = convertDefinitionName(definition);

			// adding the hasDefinition triple with modified name
			skosXLModel.addTriple(skosConcept, skosXLModel.createURIResource(AOSVocabulary.hasDefinition
					.getURI()), newDef);

			// LABELS
			Collection<RDFSLiteral> labels = definition.getLabels();

			for (RDFSLiteral label : labels) {
				skosXLModel.addLabel(newDef, label.getString(), label.getLanguage());
			}
			// COMMENTS
			/*
			 * Collection<RDFSLiteral> comments = definition.getComments();
			 * 
			 * // TODO modified to add multiple comments in different languages - pms if (comments.size() !=
			 * 1) throw new IllegalStateException(
			 * "there should be only one comment per c_definition!, while here we have: " + comments);
			 * 
			 * RDFSLiteral comment = comments.iterator().next(); skosXLModel.addComment(newDef,
			 * comment.getString(), comment.getLanguage()); for(RDFSLiteral comment : comments){
			 * skosXLModel.addComment(newDef, comment.getString(), comment.getLanguage()); }
			 */

			// OTHER PROPERTIES OF DEFINITION
			convertDatatypeStringTypedProperty(definition, newDef, AOSVocabulary.takenFromSource);
			convertDatatypeStringTypedProperty(definition, newDef, AOSVocabulary.hasSourceLink);
			convertDatatypeDateTypedProperty(definition, newDef, AOSVocabulary.hasDateCreated);
			convertDatatypeDateTypedProperty(definition, newDef, AOSVocabulary.hasDateLastUpdated);
		}

		// NO CONVERSION FOR HAS_IMAGE, SINCE THERE IS NO DATA FOR THAT

		// **************************************
		// LEXICALIZATIONS CONVERSION
		// *************************************

		// System.out.print("|its lexicalizations...");

		Collection<OWLIndividual> lexicalizations = ind.getPropertyValues(AOSVocabulary.hasLexicalization);
		for (OWLIndividual lexicalization : lexicalizations) {

			ARTURIResource skosLex = getSKOSXLLabelFromOWLCNOUN(lexicalization);

			// LABELS
			Collection<RDFSLiteral> labels = lexicalization.getLabels();
			if (labels.size() != 1)
				throw new IllegalStateException(
						"there should be only one label per Lexicalization!, while here we have: " + labels);

			RDFSLiteral label = labels.iterator().next();
			skosXLModel.addXLabel(skosLex.getURI(), label.getString(), label.getLanguage());

			// DETERMINING IF IT IS A PREFLABEL OR ALTLABEL

			Collection<Boolean> values = lexicalization.getPropertyValues(AOSVocabulary.isMainLabel);
			if (values.size() > 1) {
				throw new IllegalStateException(
						"there should be only one isMainLabel value per Lexicalization!, while here we have: "
								+ values);
			} else {
				boolean main;
				if (values.size() == 0)
					main = false;
				else {
					Boolean value = values.iterator().next();
					main = value.booleanValue();
				}

				if (main)
					skosXLModel.setPrefXLabel(skosConcept, skosLex, false);
				else {
					skosXLModel.addAltXLabel(skosConcept, skosLex);
				}
			}

			// LEXICALIZATION PROPERTIES CONVERSION

			for (OWLObjectProperty prop : AOSConversionResourceLists.lexicalizationToLexicalization) {
				convertObjectProperty(lexicalization, skosLex, prop, ResType.label);
			}

			for (OWLDatatypeProperty prop : AOSConversionResourceLists.lexicalizationToPlainLiterals) {
				convertDatatypePlainLiteralLanguageProperty(lexicalization, skosLex, prop);
			}

			for (OWLDatatypeProperty prop : AOSConversionResourceLists.lexicalizationToTypeStrings) {
				convertDatatypeStringTypedProperty(lexicalization, skosLex, prop);
			}

			for (OWLDatatypeProperty prop : AOSConversionResourceLists.lexicalizationToTypeDates) {
				convertDatatypeDateTypedProperty(lexicalization, skosLex, prop);
			}

			for (OWLDatatypeProperty prop : AOSConversionResourceLists.lexicalizationToTypeInts) {
				convertDatatypeINTTypedProperty(lexicalization, skosLex, prop);
			}

		}

		// System.out.println("|concept converted");

	}

	@SuppressWarnings("unchecked")
	void convertDatatypePlainLiteralLanguageProperty(RDFResource owlSubject, ARTURIResource skosSubject,
			RDFProperty predicate) throws ModelUpdateException {
		Collection<?> values = owlSubject.getPropertyValues(predicate);
		for (Object obj : values) {
			if (obj instanceof RDFSLiteral) {
				RDFSLiteral value = (RDFSLiteral) obj;
				skosXLModel.addTriple(skosSubject, skosXLModel.createURIResource(predicate.getURI()),
						skosXLModel.createLiteral(value.getString(), value.getLanguage()));
			}
			// made for cases where errounsly a datatype property points to a resource
			else if (obj instanceof OWLIndividual) {
				OWLIndividual value = (OWLIndividual) obj;
				Collection<RDFSLiteral> labels = value.getLabels();
				for (RDFSLiteral label : labels) {
					skosXLModel.addTriple(skosSubject, skosXLModel.createURIResource(predicate.getURI()),
							skosXLModel.createLiteral(label.getString(), label.getLanguage()));
				}

			} else {
				System.out.println("predicate: " + predicate.getURI() + "   subject: " + owlSubject.getURI());
				System.out.println("Not converted: " + obj);
			}
		}
	}

	@SuppressWarnings("unchecked")
	void convertDatatypeStringTypedProperty(RDFResource owlSubject, ARTURIResource skosSubject,
			RDFProperty predicate) throws ModelUpdateException {
		Collection<String> values = owlSubject.getPropertyValues(predicate);
		for (String value : values) {
			skosXLModel.addTriple(skosSubject, skosXLModel.createURIResource(predicate.getURI()), skosXLModel
					.createLiteral(value.toString(), XmlSchema.Res.STRING));
		}
	}

	// NOT SURE IF VALUE IS A DATE!!!
	@SuppressWarnings("unchecked")
	void convertDatatypeDateTypedProperty(RDFResource owlSubject, ARTURIResource skosSubject,
			RDFProperty predicate) throws ModelUpdateException {
		Collection<String> values = owlSubject.getPropertyValues(predicate);
		for (String value : values) {
			skosXLModel.addTriple(skosSubject, skosXLModel.createURIResource(predicate.getURI()), skosXLModel
					.createLiteral(value.toString(), XmlSchema.Res.DATETIME));
		}
	}

	@SuppressWarnings("unchecked")
	void convertDatatypeINTTypedProperty(RDFResource owlSubject, ARTURIResource skosSubject,
			RDFProperty predicate) throws ModelUpdateException {
		Collection<Integer> values = owlSubject.getPropertyValues(predicate);
		for (Integer value : values) {
			skosXLModel.addTriple(skosSubject, skosXLModel.createURIResource(predicate.getURI()), skosXLModel
					.createLiteral(value.toString(), XmlSchema.Res.INT));
		}
	}

	/**
	 * conversion from/to object properties. The <code>resType</code> argument specifies if the destination
	 * resources involved in the conversion are SKOS-XL labels (label) or skos concepts (instance) and thus
	 * drives the conversion of their URI.<br/>
	 * This method is invoked when a property from the AGROVOC OWL vocabulary is being converted into a native
	 * SKOS(XL) property or in any case, its name is being changed.
	 * 
	 * @param owlSubject
	 *            the subject of the triples in the Protege OWL Model
	 * @param skosSubject
	 *            the subject of the triples in the SKOS Conversion
	 * @param owlPredicate
	 *            the predicate of the triples in the Protege OWL Model
	 * @param skosPredicate
	 *            the predicate of the triples in the SKOS Conversion
	 * @param resType
	 *            specifies if the destination resources involved in the conversion are SKOS-XL labels (label)
	 *            or skos concepts (instance) and thus drives the conversion of their URI
	 * @throws ModelUpdateException
	 */
	@SuppressWarnings("unchecked")
	void convertObjectProperty(RDFResource owlSubject, ARTURIResource skosSubject, RDFProperty owlPredicate,
			ARTURIResource skosPredicate, ResType resType) throws ModelUpdateException {
		Collection<RDFResource> values = owlSubject.getPropertyValues(owlPredicate);
		for (RDFResource value : values) {
			ARTURIResource object = null;
			if (resType == ResType.label)
				object = getSKOSXLLabelFromOWLCNOUN(value);
			else if (resType == ResType.instance)
				object = getSKOSConceptFromOWLInstance(value);
			skosXLModel.addTriple(skosSubject, skosPredicate, object);
		}
	}

	/**
	 * as for
	 * {@link #convertObjectProperty(RDFResource, ARTURIResource, RDFProperty, ARTURIResource, ResType)} with
	 * the <code>predicate</code> being passed as <code>owlPredicate</code> of that method, and this same
	 * predicate being converted to SKOS-XL API resource, with the same URI. <br/>
	 * This method is being used when a property is left as it is in the new vocabulary.
	 * 
	 * @param owlSubject
	 *            the subject of the triples in the Protege OWL Model
	 * @param skosSubject
	 *            the subject of the triples in the SKOS Conversion
	 * @param predicate
	 *            the predicate
	 * @param resType
	 *            specifies if the destination resources involved in the conversion are SKOS-XL labels (label)
	 *            or skos concepts (instance) and thus drives the conversion of their URI
	 * @throws ModelUpdateException
	 */
	void convertObjectProperty(RDFResource owlSubject, ARTURIResource skosSubject, RDFProperty predicate,
			ResType resType) throws ModelUpdateException {
		convertObjectProperty(owlSubject, skosSubject, predicate, skosXLModel.createURIResource(predicate
				.getURI()), resType);
	}

	@SuppressWarnings("unchecked")
	public void countConcept(OWLNamedClass cls) throws ModelUpdateException, ModelAccessException {
		Collection<OWLNamedClass> subConcepts = cls.getSubclasses(false);
		for (OWLNamedClass subCls : subConcepts) {
			if (!convertedConcepts.contains(subCls)) {
				convertedConcepts.add(subCls);
				countConcept(subCls);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public int countTotalConcepts() throws ModelUpdateException, ModelAccessException {
		Collection<OWLNamedClass> rootConcepts = AOSVocabulary.c_domain_concept.getSubclasses(false);
		System.out.println("TOTAL TOP CONCEPTS FOUND : " + rootConcepts.size());
		// recursive descent along the tree, converting all concepts
		convertedConcepts.addAll(rootConcepts);
		for (OWLNamedClass cls : rootConcepts) {
			countConcept(cls);
		}
		int totalCount = convertedConcepts.size();
		System.out.println("TOTAL CONCEPTS FOUND : " + totalCount);
		convertedConcepts.clear();
		return totalCount;
	}

	@SuppressWarnings("unchecked")
	public int exploreConcept(OWLNamedClass cls, int level, int count, int totalCount, Date d0)
			throws ModelUpdateException, ModelAccessException {
		Collection<OWLIndividual> instances = cls.getInstances(false);
		Date d1 = new Date();
		if (instances.size() != 1)
			throw new IllegalStateException(
					"no domain class in the OWL AGROVOC Model should have other than one instance, while: "
							+ cls + " has these instances: " + instances);
		OWLIndividual inst = instances.iterator().next();
		ARTURIResource skosConcept = skosXLModel.createURIResource(cls.getURI());
		convertInstance(inst, skosConcept);
		Date d2 = new Date();
		Collection<OWLNamedClass> subConcepts = cls.getSubclasses(false);

		String timediff = RunConversion.getTimeDifference(d2, d1);

		String tabs = "";
		for (int i = 0; i < level; i++)
			tabs += "|\t";
		tabs += "|--";
		level++;
		String format = "%5d of %5d - in: %s - ET: %s :: %-80s\n";
		System.out.format(format, count, totalCount, timediff, RunConversion.getTimeDifference(d2, d0), tabs
				+ "[" + cls.getURI() + "]");

		for (OWLNamedClass subCls : subConcepts) {
			if (!convertedConcepts.contains(subCls)) {
				convertedConcepts.add(subCls);
				skosXLModel.addConcept(subCls.getURI(), skosConcept);
				count++;
				count = exploreConcept(subCls, level, count, totalCount, d0);
			}
		}
		return count;
	}

	public void saveConversion(File outputFile) throws IOException, ModelAccessException,
			UnsupportedRDFFormatException {
		skosXLModel.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);
	}

	@SuppressWarnings("unchecked")
	protected Collection<OWLNamedClass> convertRootConcepts() throws ModelUpdateException, ModelAccessException {
		Collection<OWLNamedClass> rootConcepts = AOSVocabulary.c_domain_concept.getSubclasses(false);
		for (OWLNamedClass cls : rootConcepts) {
			convertedConcepts.add(cls);
			skosXLModel.addConcept(cls.getURI(), NodeFilters.NONE);
		}
		return rootConcepts;
	}

	protected ARTURIResource getSKOSConceptFromOWLInstance(RDFResource owlInstance) {
		String conceptLocalName = owlInstance.getLocalName().replace("i_", "c_");
		return skosXLModel.createURIResource(skosXLModel.getDefaultNamespace() + conceptLocalName);
	}

	protected ARTURIResource getSKOSXLLabelFromOWLCNOUN(RDFResource owlInstance) {
		String conceptLocalName = owlInstance.getLocalName().replace("i_", "xl_");
		return skosXLModel.createURIResource(skosXLModel.getDefaultNamespace() + conceptLocalName);
	}

	protected ARTURIResource convertDefinitionName(RDFResource owlInstance) {
		String conceptLocalName = owlInstance.getLocalName().replace("i_", "c_");
		return skosXLModel.createURIResource(skosXLModel.getDefaultNamespace() + conceptLocalName);
	}

	/**
	 * this main method converts a ProtegeDB owl repository for AGROVOC into a SKOS version of AGROVOC,
	 * following mappings established by the AGROVOC OWL2SKOS working group at FAO.<br/>
	 * The method takes two arguments (plus an optional third one):
	 * <ul>
	 * <li>first argument points to the config file for the protege DB</li>
	 * <li>second argument provides an output file for conversion</li>
	 * <li>third (optional) argument specifies which OWL ART Implementation class (ModelFactory
	 * implementation) will be used during the process</li>
	 * </ul>
	 * 
	 * @param args
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ModelCreationException
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 * @throws UnsupportedRDFFormatException
	 * @throws BadConfigurationException
	 * @throws UnsupportedModelConfigurationException
	 * @throws UnloadableModelConfigurationException
	 * @throws ParseImportedTripleException 
	 */
	public static void main(String[] args) throws IOException, ClassNotFoundException,
			InstantiationException, IllegalAccessException, ModelCreationException, ModelUpdateException,
			ModelAccessException, UnsupportedRDFFormatException, BadConfigurationException,
			UnsupportedModelConfigurationException, UnloadableModelConfigurationException, ParseImportedTripleException {
		if (args.length < 3) {
			System.out.println("usage:\n" + OWL2SKOSConverter.class.getName()
					+ " <ProtegeDBConfigFile> <owlARTConfig> <conversionOutputFile>");
			return;
		}

		// PROTEGE PROJECT LOADING
		ProtegeModelLoader loader = new ProtegeModelLoader();
		Project protProject = loader.loadProtegeProject(args[0]);

		// OWL ART SKOSXLMODEL LOADING
		SKOSXLModel skosXLModel = ModelLoader.loadModel(args[1], SKOSXLModel.class);

		// CONVERSION
		OWL2SKOSConverter converter = new OWL2SKOSConverter(protProject, skosXLModel);
		String ntFile = args[2];
		String rdfFile = ntFile.replace(".nt", ".rdf");
		File ntOutputFile = new File(ntFile);
		File rdfOutputFile = new File(rdfFile);
		Date d1 = new Date();
		System.out.println("CONVERSION STARTED AT : " + d1);
		converter.convert();
		Date d2 = new Date();
		System.out.println("CONVERSION ENDED AT : " + d2);
		System.out.println("TOTAL TIME FOR CONVERSION: " + RunConversion.getTimeDifference(d2, d1));
		System.out.println("Adding 50 triples linking to EUROVOC: "
				+ RunConversion.getTimeDifference(new Date(), d1));
		LoadMappingTriples.addTriples(skosXLModel, new File("Input/eurovoc_links.txt"));
		System.out.println("EUROVOC skos:matches ADDED: " + RunConversion.getTimeDifference(new Date(), d1));
		converter.saveConversion(ntOutputFile);
		System.out.println("model exported to NT file: " + RunConversion.getTimeDifference(new Date(), d1));
		converter.saveConversion(rdfOutputFile);
		System.out.println("model exported to RDF/XML file: "
				+ RunConversion.getTimeDifference(new Date(), d1));
		System.out.println("TOTAL TIME : " + RunConversion.getTimeDifference(new Date(), d1));

	}

}

package fao.org.rdfmgmt.conversion.jad;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.vocabulary.RDFS;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.XmlSchema;
import edu.stanford.smi.protege.model.Project;
import edu.stanford.smi.protegex.owl.model.OWLIndividual;
import edu.stanford.smi.protegex.owl.model.OWLNamedClass;
import edu.stanford.smi.protegex.owl.model.RDFResource;
import edu.stanford.smi.protegex.owl.model.RDFSLiteral;
import fao.org.rdfmgmt.RunConversion;
import fao.org.rdfmgmt.conversion.AGROVOCProtDB2SKOSXLConverter;
import fao.org.rdfmgmt.conversion.AOSVocabulary;
import fao.org.rdfmgmt.conversion.ConversionException;
import fao.org.rdfmgmt.conversion.FAOProtDB2RDFConverter;
import fao.org.rdfmgmt.conversion.FAORDFConversionInfo;
import fao.org.rdfmgmt.owlart.ModelLoader;
import it.uniroma2.art.owlart.protegeimpl.factory.ProtegeDBModelLoader;
import fao.org.rdfmgmt.vocabularies.AUTHCON_CORPORATEBODIES;
import fao.org.rdfmgmt.vocabularies.AUTHCON_JOURNALS;
import fao.org.rdfmgmt.vocabularies.AUTHCON_SERIES;
import fao.org.rdfmgmt.vocabularies.BIBO;
import fao.org.rdfmgmt.vocabularies.DCMIMetadataTerms;
import fao.org.rdfmgmt.vocabularies.FOAF;
import fao.org.rdfmgmt.vocabularies.JAD;

/**
 * this class extends the {@link FAOProtDB2RDFConverter} class to specifically map the definitions and
 * lexicalizations of the FAO legacy OWL Model for the JAD resource into standard vocabularies such as BIBO
 * and FOAF
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class JADProtDB2BIBOFOAFConverter extends FAOProtDB2RDFConverter {

	public static enum Vocabulary {
		journals, series, corporate_bodies
	}

	protected Vocabulary vocabulary;
	protected OWLModel model;

	private String scopeNoteDateRegex = "(\\d\\d\\d\\d)-?(\\d\\d\\d\\d)?(.*)";

	/**
	 * @param protegeProject
	 * @param model
	 * @param discardRedundants
	 * @param voc
	 *            an additional parameter with respect to the standard constructor from
	 *            {@link FAOProtDB2RDFConverter}, this tells which vocabulary is being converted
	 */
	public JADProtDB2BIBOFOAFConverter(Project protegeProject, OWLModel model, Vocabulary voc,
			boolean discardRedundants) {
		super(protegeProject, model, discardRedundants, false, false);
		this.model = model;
		vocabulary = voc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fao.org.owl2skos.conversion.FAOProtDB2RDFConverter#convertRootConcepts()
	 * 
	 * this method has been overridden since in Authority Files we have to avoid those concepts which are not
	 * to be converted. Only children of Journals and Series will be converted
	 */
	@Override
	protected Collection<OWLNamedClass> getRootConcepts() throws ModelUpdateException {
		OWLNamedClass rootConcept;
		if (vocabulary == Vocabulary.journals)
			rootConcept = AOSVocabulary.c_journal;
		else if (vocabulary == Vocabulary.series)
			rootConcept = AOSVocabulary.c_series;
		else
			rootConcept = AOSVocabulary.c_corporate;

		// rootConcept = AOSVocabulary.c_domain_concept;

		System.out.println("root meta-concept: " + rootConcept);

		@SuppressWarnings("unchecked")
		Collection<OWLNamedClass> rootConcepts = rootConcept.getSubclasses(false);

		// System.out.println("subclasses of meta-root: " + rootConcepts );
		return rootConcepts;
	}

	protected void convertHasRelatedConcept(OWLIndividual ind, ARTURIResource convertedConcept)
			throws ModelUpdateException {
		System.err.println("there seems to be a related concept property inside JAD!");
	}

	protected void convertDefinitions(OWLIndividual ind, ARTURIResource convertedConcept) throws ModelUpdateException {
		@SuppressWarnings("unchecked")
		Collection<OWLIndividual> hasDefinitions = ind.getPropertyValues(AOSVocabulary.hasDefinition);
		for (OWLIndividual definition : hasDefinitions) {
			// LABELS
			@SuppressWarnings("unchecked")
			Collection<RDFSLiteral> labels = definition.getLabels();

			for (RDFSLiteral label : labels) {
				model.instantiatePropertyWithPlainLiteral(convertedConcept, BIBO.Res.SHORTDESCRIPTION,
						label.getString(), label.getLanguage());
			}
		}
	}
	
	protected void convertImages(OWLIndividual ind, ARTURIResource convertedConcept) throws ModelUpdateException {
		//no images in JAD...I think!!!
	}


	// this implementation flattens reified lexicalizations to plain dcterms:title and skos:altLabel
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fao.org.owl2skos.conversion.FAOProtDB2RDFConverter#convertLexicalization(edu.stanford.smi.protegex.
	 * owl.model.OWLIndividual, it.uniroma2.art.owlart.model.ARTURIResource)
	 */
	protected ARTURIResource convertLexicalization(OWLIndividual lexicalization, ARTURIResource convertedConcept)
			throws ModelUpdateException, ModelAccessException {
		// LABELS
		@SuppressWarnings("unchecked")
		Collection<RDFSLiteral> labels = lexicalization.getLabels();
		if (labels.size() != 1)
			throw new IllegalStateException(
					"there should be exactly one label per Lexicalization!, while here we have: " + labels);

		RDFSLiteral label = labels.iterator().next();

		// DETERMINING IF IT IS A PREFLABEL OR ALTLABEL

		@SuppressWarnings("unchecked")
		Collection<Boolean> values = lexicalization.getPropertyValues(AOSVocabulary.isMainLabel);
		if (values.size() > 1) {
			throw new IllegalStateException(
					"there should be only one isMainLabel value per Lexicalization!, while here we have: "
							+ values);
		} else {
			boolean main;
			if (values.size() == 0)
				main = false;
			else {
				Boolean value = values.iterator().next();
				main = value.booleanValue();
			}

			if (main) {
				if (vocabulary == Vocabulary.corporate_bodies)
					model.instantiatePropertyWithPlainLiteral(convertedConcept, RDFS.Res.LABEL,
							label.getString(), label.getLanguage());
				else
					model.instantiatePropertyWithPlainLiteral(convertedConcept, DCMIMetadataTerms.Res.TITLE,
							label.getString(), label.getLanguage());
			} else {
				// TODO check this
				model.instantiatePropertyWithPlainLiteral(convertedConcept, SKOS.Res.ALTLABEL,
						label.getString(), label.getLanguage());
			}
		}
		return null;
	}

	public void convertExceptionProperties(OWLIndividual ind, ARTURIResource convertedConcept)
			throws ModelUpdateException {
		
		// hasIssn

		@SuppressWarnings("unchecked")
		Collection<String> issns = ind.getPropertyValues(AOSVocabulary.hasIssn);
		for (String issn : issns) {

			if (issn.endsWith("-X"))
				issn = issn.replace("-X", "X");
			if (issn.length() == 8)
				issn = issn.substring(0, 4) + "-" + issn.substring(4);

			rdfModel.addTriple(convertedConcept, BIBO.Res.ISSN,
					rdfModel.createLiteral(issn, XmlSchema.Res.STRING));

		}

		// for scopeNote --> dates || beware! at the moment, data such as:
		// "1988-2000. ALCOM News articles incorporated into: SADC Inland Fisheries News"
		// is catched as a date by the regex!

		Collection<?> values = ind.getPropertyValues(AOSVocabulary.hasScopeNote);
		for (Object obj : values) {
			if (obj instanceof RDFSLiteral) {
				RDFSLiteral value = (RDFSLiteral) obj;
				String scopeNoteValue = value.getString();

				// DUE TO SPURIOUS INFORMATION IN SCOPENOTES, THESE ARE DECOMPOSED:
				// - first part (year of publ) is projected as dct:issued
				// - second year (end of publ) is projected, together with first part, as date
				// - if the regex is not fully matched, then a scope note is published
				// note that this was not perfect, as the regex scopeNoteDateRegex matched even elements of
				// the form: xxxx-xxxx followed by other scopeNote information, such as:
				// "1988-2000. ALCOM News articles incorporated into: SADC Inland Fisheries News"
				// so I should have used scopeNoteDoubleDateRegex (see field above in this class) to check for
				// them, and then properly separate the right values
				// Also, Intervals are outputted with a "/" separator and not the "-" used in the
				// vocbench. This needs also to be changed in the code below

				Matcher matcher = Pattern.compile(scopeNoteDateRegex).matcher(scopeNoteValue);

				if (matcher.matches()) {

					String initDate = matcher.group(1);
					String endDate = matcher.group(2);

					rdfModel.addTriple(convertedConcept, DCMIMetadataTerms.Res.ISSUED,
							rdfModel.createLiteral(initDate, XmlSchema.Res.DATE));

					if (matcher.group(2) != null) {
						rdfModel.addTriple(convertedConcept, DCMIMetadataTerms.Res.DATE,
								rdfModel.createLiteral(initDate + "/" + endDate, XmlSchema.Res.DATE));
					}

					// in this case a scope note is asserted with only the remaining free-text info
					if (matcher.group(3) != null && !matcher.group(3).equals("") ) {
						rdfModel.addTriple(convertedConcept, SKOS.Res.SCOPENOTE,
								rdfModel.createLiteral(matcher.group(3), value.getLanguage()));
					}

				}

				// if scope note does not match, the whole scope note from vocbench is taken
				else
					rdfModel.addTriple(convertedConcept, SKOS.Res.SCOPENOTE,
							rdfModel.createLiteral(scopeNoteValue, value.getLanguage()));
			} else
				throw new IllegalArgumentException("check for hasScopeNote values of: " + ind);
		}

		/*
		 * convertObjectProperty(ind, convertedConcept, AOSVocabulary.isPublishedBy,
		 * DCMIMetadataTerms.Res.PUBLISHER, ResType.instance);
		 */
	}

	protected void addConcept(OWLNamedClass cls, ARTURIResource superConcept) throws ModelUpdateException {
		// TODO WARNING! there are also concepts pointed by series and journals vocabularies, but coming from
		// other namespaces, such as for corporate bodies. For the moment, corporate are included in the
		// SERIALS (prev JAD) namespace. Another one will be created later
		if (vocabulary == Vocabulary.journals)
			model.addInstance(getJournalResourceNameFromAuthResource(cls).getURI(), BIBO.Res.JOURNAL);
		else if (vocabulary == Vocabulary.series)
			model.addInstance(getSeriesResourceNameFromAuthResource(cls).getURI(), BIBO.Res.SERIES);
		else
			model.addInstance(getCorporateBodyNameFromAuthResource(cls).getURI(), FOAF.Res.ORGANIZATION);
	}
	
	protected void addBroaderRelationship(OWLNamedClass cls, ARTURIResource superConcept)
			throws ModelUpdateException, ModelAccessException {
		ARTURIResource instanceURI; 
		if (vocabulary == Vocabulary.journals) {
			instanceURI = model.createURIResource(getJournalResourceNameFromAuthResource(cls).getURI());
			model.addType(instanceURI, BIBO.Res.JOURNAL);
		}
		else if (vocabulary == Vocabulary.series) {
			instanceURI = model.createURIResource(getSeriesResourceNameFromAuthResource(cls).getURI());
			model.addType(instanceURI, BIBO.Res.SERIES);
		}
		else {
			instanceURI = model.createURIResource(getCorporateBodyNameFromAuthResource(cls).getURI());
			model.addType(instanceURI, FOAF.Res.ORGANIZATION);
		}
			
		
		
	}

	// - dedicated transformation for series and journals, which collapses their namespace into the JAD
	// serials one
	// - transformation of localname from i_ to c_ for resources from other fao namespaces
	// - get the URI as it is for foreign resources
	protected ARTURIResource convertResourceURI(RDFResource owlInstance) {

		String namespace = owlInstance.getNamespace();
		if (namespace.equals(AUTHCON_JOURNALS.defNamespace))
			return getJournalResourceNameFromAuthResource(owlInstance);
		if (namespace.equals(AUTHCON_SERIES.defNamespace))
			return getSeriesResourceNameFromAuthResource(owlInstance);
		if (namespace.equals(AUTHCON_CORPORATEBODIES.defNamespace))
			return getCorporateBodyNameFromAuthResource(owlInstance);
		if (namespace.startsWith("http://aims.fao.org"))
			return rdfModel.createURIResource(owlInstance.getNamespace()
					+ owlInstance.getLocalName().replace("i_", "c_"));
		return rdfModel.createURIResource(owlInstance.getURI());

	}

	protected ARTURIResource getJournalResourceNameFromAuthResource(RDFResource resource) {
		String conceptLocalName = resource.getLocalName().replace("c_", "j_").replace("i_", "j_");
		return rdfModel.createURIResource(JAD.defNamespace + conceptLocalName);
	}

	protected ARTURIResource getSeriesResourceNameFromAuthResource(RDFResource resource) {
		String conceptLocalName = resource.getLocalName().replace("c_", "s_").replace("i_", "s_");
		return rdfModel.createURIResource(JAD.defNamespace + conceptLocalName);
	}

	protected ARTURIResource getCorporateBodyNameFromAuthResource(RDFResource resource) {
		String conceptLocalName = resource.getLocalName().replace("c_", "cb_").replace("i_", "cb_");
		return rdfModel.createURIResource(JAD.defNamespace + conceptLocalName);
	}

	@Override
	protected FAORDFConversionInfo getConversionInfo() {
		return new JADConversionInfo(protOWLModel, model);
	}

	public static void main(String[] args) throws ModelUpdateException, ModelAccessException, IOException,
			UnsupportedRDFFormatException, ModelCreationException, BadConfigurationException,
			ConversionException {
		if (args.length < 3) {
			System.out
					.println("usage:\n"
							+ AGROVOCProtDB2SKOSXLConverter.class.getName()
							+ " <ProtegeDBConfigFile> <owlARTConfig> <vocabulary> <conversionOutputFile> [discardRedundants [forceTypedConversions]]");
			return;
		}

		logger.info("SETTING UP CONVERSION:\n");

		// PROTEGE PROJECT LOADING
		ProtegeDBModelLoader loader = new ProtegeDBModelLoader();
		Project protProject = loader.loadProtegeProject(args[0]);

		// OWL ART SKOSXLMODEL LOADING
		OWLModel owlModel = ModelLoader.loadModel(args[1], OWLModel.class);

		Vocabulary vocabulary = Vocabulary.valueOf(args[2]);

		String ntFile = args[3];

		boolean discardRedundants = true;
		if (args.length > 4) {
			discardRedundants = Boolean.parseBoolean(args[4]);
		}
		logger.info("discardRedundants: " + discardRedundants);

		JADProtDB2BIBOFOAFConverter converter = new JADProtDB2BIBOFOAFConverter(protProject, owlModel,
				vocabulary, discardRedundants);

		String rdfFile = ntFile.replace(".nt", ".rdf");
		File ntOutputFile = new File(ntFile);
		File rdfOutputFile = new File(rdfFile);
		Date d1 = new Date();
		System.out.println("CONVERSION STARTED AT : " + d1);
		converter.convert();
		Date d2 = new Date();
		System.out.println("CONVERSION ENDED AT : " + d2);
		System.out.println("TOTAL TIME FOR CONVERSION: " + RunConversion.getTimeDifference(d2, d1));

		// **************
		// MAPPINGS
		// **************
		/*
		 * System.out.println("Adding 50 triples linking to EUROVOC: " + RunConversion.getTimeDifference(new
		 * Date(), d1));
		 * 
		 * 
		 * AddTriples.addTriples(skosXLModel, new File("Input/eurovoc_links.txt"));
		 * System.out.println("EUROVOC skos:matches ADDED: " + RunConversion.getTimeDifference(new Date(),
		 * d1));
		 */

		// **************
		// EXPORTS
		// **************
		converter.saveConversion(ntOutputFile);
		System.out.println("model exported to NT file: " + RunConversion.getTimeDifference(new Date(), d1));
		converter.saveConversion(rdfOutputFile);
		System.out.println("model exported to RDF/XML file: "
				+ RunConversion.getTimeDifference(new Date(), d1));

		System.out.println("TOTAL TIME : " + RunConversion.getTimeDifference(new Date(), d1));
	}



}

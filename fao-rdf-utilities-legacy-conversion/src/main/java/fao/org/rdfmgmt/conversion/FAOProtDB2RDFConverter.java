package fao.org.rdfmgmt.conversion;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.XmlSchema;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.stanford.smi.protege.model.Project;
import edu.stanford.smi.protegex.owl.model.OWLDatatypeProperty;
import edu.stanford.smi.protegex.owl.model.OWLIndividual;
import edu.stanford.smi.protegex.owl.model.OWLNamedClass;
import edu.stanford.smi.protegex.owl.model.OWLObjectProperty;
import edu.stanford.smi.protegex.owl.model.OWLProperty;
import edu.stanford.smi.protegex.owl.model.RDFProperty;
import edu.stanford.smi.protegex.owl.model.RDFResource;
import edu.stanford.smi.protegex.owl.model.RDFSLiteral;
import edu.stanford.smi.protegex.owl.model.RDFSNames;
import fao.org.rdfmgmt.RunConversion;
import fao.org.rdfmgmt.vocabularies.AGROVOC;

/**
 * 
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * @author Sachit Rajbhandari <Sachit.Rajbhandari@fao.org>
 */
public abstract class FAOProtDB2RDFConverter {

	protected boolean discardRedundants;
	protected boolean forceTypedConversions;
	protected boolean getOnlyPublished;
	public static OWLProperty rdfsComment;
	public static OWLProperty rdfsLabel;

	public static enum ResType {
		label, instance
	};

	protected static Logger logger = LoggerFactory.getLogger(FAOProtDB2RDFConverter.class);

	Project protegeProject;
	protected edu.stanford.smi.protegex.owl.model.OWLModel protOWLModel;
	protected RDFModel rdfModel;
	protected HashSet<OWLNamedClass> convertedConcepts;
	FAORDFConversionInfo conversionInfo;

	/**
	 * @param protegeProject
	 * @param rdfModel
	 * @param discardRedundants
	 *            if <code>true</code> the conversion discards redundant triples (e.g. the inverse triples,
	 *            which in the original Protege are always explicit bothways
	 * @param forceTypedConversions
	 *            if <code>true</code> datatype values which are non conformant to their standard formats are
	 *            automatically converted (when a correction is available). If <code>false</code>, an
	 *            exception is thrown instead, and the conversion is stopped
	 * @param getOnlyPublished
	 *            if <code>true</code>, only the concepts which have a "published" status, are to be converted
	 */
	public FAOProtDB2RDFConverter(Project protegeProject, RDFModel rdfModel, boolean discardRedundants,
			boolean forceTypedConversions, boolean getOnlyPublished) {

		this.getOnlyPublished = getOnlyPublished;
		this.discardRedundants = discardRedundants;
		this.forceTypedConversions = forceTypedConversions;

		this.protegeProject = protegeProject;
		this.rdfModel = rdfModel;
		protOWLModel = (edu.stanford.smi.protegex.owl.model.OWLModel) protegeProject.getKnowledgeBase();

		AOSVocabulary.initialize(protOWLModel);

		convertedConcepts = new HashSet<OWLNamedClass>();

		logger.info("cdomainConcept: " + AOSVocabulary.c_domain_concept);

		rdfsLabel = protOWLModel.getOWLProperty(RDFSNames.Slot.LABEL);
		rdfsComment = protOWLModel.getOWLProperty(RDFSNames.Slot.COMMENT);

		// logger.info("hasLexicalization: " +
		// hasLexicalization.getBrowserText());
	}

	/**
	 * 
	 * 
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 * @throws ConversionException
	 */
	public void convert() throws ModelUpdateException, ModelAccessException, ConversionException {

		AOSConversionResourceLists.initialize(protOWLModel, discardRedundants);

		conversionInfo = getConversionInfo();

		// **************************************
		// COUNTING ALL CONCEPTS
		// *************************************

		Date d0 = new Date();
		int totalConcepts = countTotalConcepts();

		// **************************************
		// CONCEPTS CONVERSION
		// *************************************

		// does a first round for rootConcepts, since they need to be reported
		// as topConcepts for the above
		// scheme. Root concepts are usually depicted in VOCBENCH as children of
		// some metaconcept (which is
		// however not technically a metaconcept but just their superconcept
		Collection<OWLNamedClass> rootConcepts = getRootConcepts();

		int count = rootConcepts.size();
		// recursive descent along the tree, converting all concepts
		for (OWLNamedClass cls : rootConcepts) {
			System.err.println("root concept: " + cls);
			count = convertConcept(cls, NodeFilters.NONE, 0, count, totalConcepts, d0);
		}
	}

	protected abstract FAORDFConversionInfo getConversionInfo();

	/**
	 * @param ind
	 *            the instance representation of a concept in the AGROVOC OWL Vocabulary (see AGROVOC OWL
	 *            Vocabulary for details)
	 * @param convertedConcept
	 *            the representation of the same concept in the target API
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 * @throws ConversionException
	 */
	@SuppressWarnings("unchecked")
	public void convertInstanceDescription(OWLIndividual ind, ARTURIResource convertedConcept)
			throws ModelUpdateException, ModelAccessException, ConversionException {

		// **************************************
		// CONCEPT PROPERTIES CONVERSION
		// *************************************

		// System.out.print("|its properties...");

		convertHasRelatedConcept(ind, convertedConcept);

		// System.out.println("conceptToConcept list: " +
		// AOSConversionResourceLists.conceptToConcept);
		for (OWLObjectProperty prop : AOSConversionResourceLists.conceptToConcept) {
			convertObjectProperty(ind, convertedConcept, prop, ResType.instance);
		}

		// System.out.println("conceptToPlainLiterals list: " +
		// AOSConversionResourceLists.conceptToPlainLiterals);
		for (OWLDatatypeProperty prop : AOSConversionResourceLists.conceptToPlainLiterals) {
			convertDatatypePlainLiteralLanguageProperty(ind, convertedConcept, prop);
		}

		// System.out.println("conceptToTypeStrings list: " +
		// AOSConversionResourceLists.conceptToTypeStrings);
		for (OWLDatatypeProperty prop : AOSConversionResourceLists.conceptToTypeStrings) {
			// System.out.println("converting prop: " + prop + " for subject: "
			// + convertedConcept +
			// " from Protege: " + ind);
			convertDatatypeStringTypedProperty(ind, convertedConcept, prop);
		}

		// System.out.println("conceptToTypeDates list: " +
		// AOSConversionResourceLists.conceptToTypeDates);
		for (OWLDatatypeProperty prop : AOSConversionResourceLists.conceptToTypeDates) {
			convertDatatypeDateTypedProperty(ind, convertedConcept, prop);
		}

		convertExceptionProperties(ind, convertedConcept);

		// **************************************
		// ENTITY ANNOTATIONS CONVERSION
		// *************************************

		// System.out.print("|its entity annotations...");

		// HAS DEFINITION CONVERSION
		convertDefinitions(ind, convertedConcept);

		// HAS IMAGE CONVERSION
		convertImages(ind, convertedConcept);

		// **************************************
		// LEXICALIZATIONS CONVERSION
		// *************************************

		// System.out.print("|its lexicalizations...");

		Collection<OWLIndividual> lexicalizations = ind.getPropertyValues(AOSVocabulary.hasLexicalization);
		for (OWLIndividual lexicalization : lexicalizations) {

			// LEXICALIZATION PROPERTIES CONVERSION

			String status = (String) lexicalization.getPropertyValue(AOSVocabulary.hasStatus);

			if (status != null && getOnlyPublished && !status.equals("Published")) {
				logger.info(lexicalization + " is a non published lexicalization (status=" + status
						+ "), not exporting its content");
			} else {
				ARTURIResource skosLex = convertLexicalization(lexicalization, convertedConcept);

				if (skosLex != null) { // see contract for convertLexicalization
										// method

					for (OWLObjectProperty prop : AOSConversionResourceLists.lexicalizationToLexicalization) {
						convertObjectProperty(lexicalization, skosLex, prop, ResType.label);
					}

					for (OWLDatatypeProperty prop : AOSConversionResourceLists.lexicalizationToPlainLiterals) {
						convertDatatypePlainLiteralLanguageProperty(lexicalization, skosLex, prop);
					}

					convertHasCodeAgrovoc(lexicalization, skosLex);

					for (OWLDatatypeProperty prop : AOSConversionResourceLists.lexicalizationToTypeStrings) {
						convertDatatypeStringTypedProperty(lexicalization, skosLex, prop);
					}

					for (OWLDatatypeProperty prop : AOSConversionResourceLists.lexicalizationToTypeDates) {
						convertDatatypeDateTypedProperty(lexicalization, skosLex, prop);
					}

					for (OWLDatatypeProperty prop : AOSConversionResourceLists.lexicalizationToTypeInts) {
						convertDatatypeINTTypedProperty(lexicalization, skosLex, prop);
					}

				}
			}
		}

		// System.out.println("|concept converted");

	}

	/**
	 * this method is related to the conversion of properties which cannot be easily managed through the
	 * standard methods available in this generic conversion abstract class.<br/>
	 * This method will be usually implemented in the most specific implementing class for conversion to
	 * handle in a dedicated way these conversion of these properties and of their values
	 * 
	 * @throws ModelUpdateException
	 */
	public abstract void convertExceptionProperties(OWLIndividual ind, ARTURIResource convertedConcept)
			throws ModelUpdateException;

	/**
	 * this method is specifically tailored for the {@link AOSVocabulary#hasRelatedConcept} property, which
	 * can be reassigned specifically for SKOS and OWL (in case, it can be further changed on the third layer
	 * of conversion, i.e., the specific vocabulary conversion class)
	 * 
	 * @param ind
	 * @param convertedConcept
	 * @throws ModelUpdateException
	 */
	abstract protected void convertHasRelatedConcept(OWLIndividual ind, ARTURIResource convertedConcept)
			throws ModelUpdateException;

	protected abstract void convertDefinitions(OWLIndividual ind, ARTURIResource convertedConcept)
			throws ModelUpdateException, ConversionException;

	protected abstract void convertImages(OWLIndividual ind, ARTURIResource convertedConcept)
			throws ModelUpdateException, ConversionException;

	protected void convertHasCodeAgrovoc(OWLIndividual protegeSubject, ARTURIResource convertedSubject)
			throws ModelUpdateException, ConversionException {
		Collection<?> values = protegeSubject.getPropertyValues(AOSVocabulary.hasCodeAgrovoc);
		for (Object value : values) {
			rdfModel.addTriple(convertedSubject, SKOS.Res.NOTATION,
					rdfModel.createLiteral(value.toString(), AGROVOC.Res.AGROVOC_CODE));
		}
	}

	/**
	 * Contract for this method is that, beside converting the lexicalform of the lexicalization , the method
	 * returns the lexicalization itself if it is being reified in the target vocabulary; null otherwise (in
	 * this case, the conversion routine does not try to convert properties of the lexicalization object, as
	 * only the literalform of the lexicalization is retained in the target vocabulary
	 * 
	 * @param ind
	 * @param convertedConcept
	 * @return null if the lexicalization needs not to be reified in the target vocabulary
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	protected abstract ARTURIResource convertLexicalization(OWLIndividual ind, ARTURIResource convertedConcept)
			throws ModelUpdateException, ModelAccessException;

	/**
	 * @param cls
	 * @param superConcept
	 *            if equals {@link NodeFilters#NONE} then this is a root concept (take appropriate actions)
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	protected abstract void addConcept(OWLNamedClass cls, ARTURIResource superConcept)
			throws ModelUpdateException, ModelAccessException;
	
	/**
	 * 
	 * @param cls
	 * @param superConcept
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	protected abstract void addBroaderRelationship(OWLNamedClass cls, ARTURIResource superConcept)
			throws ModelUpdateException, ModelAccessException;
	

	void convertDatatypePlainLiteralLanguageProperty(RDFResource owlSubject, ARTURIResource skosSubject,
			RDFProperty predicate) throws ModelUpdateException {
		convertDatatypePlainLiteralLanguageProperty(owlSubject, skosSubject, predicate,
				conversionInfo.getMappedProperty(predicate));
	}

	@SuppressWarnings("unchecked")
	void convertDatatypePlainLiteralLanguageProperty(RDFResource owlSubject, ARTURIResource skosSubject,
			RDFProperty predicate, ARTURIResource targetProperty) throws ModelUpdateException {
		Collection<?> values = owlSubject.getPropertyValues(predicate);
		for (Object obj : values) {
			if (obj instanceof RDFSLiteral) {
				RDFSLiteral value = (RDFSLiteral) obj;
				rdfModel.addTriple(skosSubject, targetProperty,
						rdfModel.createLiteral(value.getString(), value.getLanguage()));
			}
			// made for cases where errounsly a datatype property points to a
			// resource
			else if (obj instanceof OWLIndividual) {
				OWLIndividual value = (OWLIndividual) obj;
				Collection<RDFSLiteral> labels = value.getLabels();
				for (RDFSLiteral label : labels) {
					rdfModel.addTriple(skosSubject, targetProperty,
							rdfModel.createLiteral(label.getString(), label.getLanguage()));
				}

			} else if (obj instanceof String) {
				rdfModel.addTriple(skosSubject, targetProperty, rdfModel.createLiteral((String) obj));
			} else {
				logger.error("predicate: " + predicate.getURI() + "   subject: " + owlSubject.getURI());
				logger.error("Not converted: " + obj + "type: " + obj.getClass());
			}
		}
	}

	// NOT SURE IF VALUE IS A DATE!!!
	@SuppressWarnings("unchecked")
	void convertDatatypeDateTypedProperty(RDFResource protegeSubject, ARTURIResource convertedSubject,
			RDFProperty predicate) throws ModelUpdateException {
		Collection<String> values = protegeSubject.getPropertyValues(predicate);
		for (String value : values) {
			String fixedvalue = value.replace(" ", "T").concat("Z");
			rdfModel.addTriple(convertedSubject, conversionInfo.getMappedProperty(predicate),
					rdfModel.createLiteral(fixedvalue, XmlSchema.Res.DATETIME));
		}
	}

	void convertDatatypeStringTypedProperty(RDFResource protegeSubject, ARTURIResource convertedSubject,
			RDFProperty predicate) throws ModelUpdateException, ConversionException {
		convertDatatypeStringTypedProperty(protegeSubject, convertedSubject, predicate,
				conversionInfo.getMappedProperty(predicate));
	}

	@SuppressWarnings("unchecked")
	void convertDatatypeStringTypedProperty(RDFResource protegeSubject, ARTURIResource convertedSubject,
			RDFProperty predicate, ARTURIResource targetProperty) throws ModelUpdateException,
			ConversionException {
		try {
			Collection<String> values = protegeSubject.getPropertyValues(predicate);
			for (String value : values) {
				rdfModel.addTriple(convertedSubject, targetProperty,
						rdfModel.createLiteral(value.toString(), XmlSchema.Res.STRING));
			}
		} catch (ClassCastException e) {
			if (forceTypedConversions) {
				logger.warn("exception when converting value of predicate: " + predicate + " for subject: "
						+ convertedSubject + " | forcing conversion to desired format");
				Collection<Object> values = protegeSubject.getPropertyValues(predicate);
				for (Object value : values) {
					rdfModel.addTriple(convertedSubject, targetProperty,
							rdfModel.createLiteral(value.toString(), XmlSchema.Res.STRING));
				}
			} else
				throw new ConversionException("exception when converting value of predicate: " + predicate
						+ " for subject: " + convertedSubject + "| " + e.getMessage(), e);
		}
	}

	void convertDatatypeINTTypedProperty(RDFResource protegeSubject, ARTURIResource convertedSubject,
			RDFProperty predicate) throws ModelUpdateException, ConversionException {
		convertDatatypeINTTypedProperty(protegeSubject, convertedSubject, predicate,
				conversionInfo.getMappedProperty(predicate));
	}

	@SuppressWarnings("unchecked")
	void convertDatatypeINTTypedProperty(RDFResource protegeSubject, ARTURIResource convertedSubject,
			RDFProperty predicate, ARTURIResource targetProperty) throws ModelUpdateException,
			ConversionException {
		try {
			Collection<Integer> values = protegeSubject.getPropertyValues(predicate);
			for (Integer value : values) {
				rdfModel.addTriple(convertedSubject, targetProperty,
						rdfModel.createLiteral(value.toString(), XmlSchema.Res.INT));
			}
		} catch (ClassCastException e) {
			if (forceTypedConversions) {
				logger.warn("exception when converting value of predicate: " + predicate + " for subject: "
						+ convertedSubject + " | forcing conversion to desired format");
				Collection<Object> values = protegeSubject.getPropertyValues(predicate);
				for (Object value : values) {
					rdfModel.addTriple(convertedSubject, targetProperty,
							rdfModel.createLiteral(value.toString(), XmlSchema.Res.STRING));
				}
			} else
				throw new ConversionException("exception when converting value of predicate: " + predicate
						+ " for subject: " + convertedSubject + "| " + e.getMessage(), e);
		}
	}

	/**
	 * conversion from/to object properties. The <code>resType</code> argument specifies if the destination
	 * resources involved in the conversion are SKOS-XL labels (label) or skos concepts (instance) and thus
	 * drives the conversion of their URI.<br/>
	 * This method is invoked when a property from the AGROVOC OWL vocabulary is being converted into a
	 * different property in the target model
	 * 
	 * @param owlSubject
	 *            the subject of the triples in the Protege OWL Model
	 * @param skosSubject
	 *            the subject of the triples in the SKOS Conversion
	 * @param owlPredicate
	 *            the predicate of the triples in the Protege OWL Model
	 * @param skosPredicate
	 *            the predicate of the triples in the SKOS Conversion
	 * @param resType
	 *            specifies if the destination resources involved in the conversion are lexicalizations
	 *            (label) or concepts (instance) and thus drives the conversion of their URI
	 * @throws ModelUpdateException
	 */
	@SuppressWarnings("unchecked")
	void convertObjectProperty(RDFResource owlSubject, ARTURIResource skosSubject, RDFProperty owlPredicate,
			ARTURIResource skosPredicate, ResType resType) throws ModelUpdateException {
		Collection<RDFResource> values = owlSubject.getPropertyValues(owlPredicate);
		for (RDFResource value : values) {
			ARTURIResource object = null;
			if (resType == ResType.label)
				object = getSKOSXLLabelFromOWLCNOUN(value);
			else if (resType == ResType.instance)
				object = convertResourceURI(value);
			rdfModel.addTriple(skosSubject, skosPredicate, object);
		}
	}

	/**
	 * as for
	 * {@link #convertObjectProperty(RDFResource, ARTURIResource, RDFProperty, ARTURIResource, ResType)} with
	 * the <code>predicate</code> being passed as <code>owlPredicate</code> of that method, and this same
	 * predicate being converted to SKOS-XL API resource, with the same URI. <br/>
	 * This method is being used when a property is left as it is in the new vocabulary.
	 * 
	 * @param owlSubject
	 *            the subject of the triples in the Protege OWL Model
	 * @param skosSubject
	 *            the subject of the triples in the SKOS Conversion
	 * @param predicate
	 *            the predicate
	 * @param resType
	 *            specifies if the destination resources involved in the conversion are SKOS-XL labels (label)
	 *            or skos concepts (instance) and thus drives the conversion of their URI
	 * @throws ModelUpdateException
	 */
	void convertObjectProperty(RDFResource owlSubject, ARTURIResource skosSubject, RDFProperty predicate,
			ResType resType) throws ModelUpdateException {
		convertObjectProperty(owlSubject, skosSubject, predicate,
				conversionInfo.getMappedProperty(predicate), resType);
	}

	@SuppressWarnings("unchecked")
	public void countConcept(OWLNamedClass cls) throws ModelUpdateException, ModelAccessException {
		Collection<OWLNamedClass> subConcepts = cls.getSubclasses(false);
		for (OWLNamedClass subCls : subConcepts) {
			if (!convertedConcepts.contains(subCls)) {
				convertedConcepts.add(subCls);
				countConcept(subCls);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public int countTotalConcepts() throws ModelUpdateException, ModelAccessException {
		Collection<OWLNamedClass> rootConcepts = AOSVocabulary.c_domain_concept.getSubclasses(false);
		System.out.println("TOTAL TOP CONCEPTS FOUND : " + rootConcepts.size());
		// recursive descent along the tree, converting all concepts
		convertedConcepts.addAll(rootConcepts);
		for (OWLNamedClass cls : rootConcepts) {
			countConcept(cls);
		}
		int totalCount = convertedConcepts.size();
		System.out.println("TOTAL CONCEPTS FOUND : " + totalCount);
		convertedConcepts.clear();
		return totalCount;
	}

	@SuppressWarnings("unchecked")
	public int convertConcept(OWLNamedClass cls, ARTURIResource superCls, int level, int count,
			int totalCount, Date d0) throws ModelUpdateException, ModelAccessException, ConversionException {
		if (!convertedConcepts.contains(cls)) {
			convertedConcepts.add(cls);
			ARTURIResource convertedConcept = convertResourceURI(cls);
			count++;

			// getting the instance description
			Collection<OWLIndividual> instances = cls.getInstances(false);
			Date d1 = new Date();
			if (instances.size() != 1)
				throw new IllegalStateException(
						"no domain class in the OWL AGROVOC Model should have other than one instance, while: "
								+ cls + " has these instances: " + instances);
			OWLIndividual inst = instances.iterator().next();

			String status = (String) inst.getPropertyValue(AOSVocabulary.hasStatus);

			if (status != null && getOnlyPublished && !status.equals("Published")) {
				logger.info(cls + " is a non published concept (status=" + status
						+ "), not exporting its content");
			} else {

				addConcept(cls, superCls);
				convertInstanceDescription(inst, convertedConcept);

				// start of logging code
				Date d2 = new Date();
				String timediff = RunConversion.getTimeDifference(d2, d1);

				String tabs = "";
				for (int i = 0; i < level; i++)
					tabs += "|\t";
				tabs += "|--";
				level++;
				String format = "%5d of %5d - in: %s - ET: %s :: %-80s\n";
				System.out.format(format, count, totalCount, timediff,
						RunConversion.getTimeDifference(d2, d0), tabs + "[" + cls.getURI() + "]");
				// end of logging code
			}

			// children are explored, no matter if the concept is deprecated or
			// not (but only one time, see
			// outer if statement)
			Collection<OWLNamedClass> subConcepts = cls.getSubclasses(false);
			for (OWLNamedClass subCls : subConcepts) {
				count = convertConcept(subCls, convertedConcept, level, count, totalCount, d0);
			}
		} else {
			// the concept has been already visited, so we are here in case of multiple inheritance
			// the concept must not be converted again, though the linking to the further father need to be
			// exported
			addBroaderRelationship(cls, superCls);
		}

		return count;
	}

	public void saveConversion(File outputFile) throws IOException, ModelAccessException,
			UnsupportedRDFFormatException {
		rdfModel.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);
	}

	@SuppressWarnings("unchecked")
	protected Collection<OWLNamedClass> getRootConcepts() throws ModelUpdateException, ModelAccessException {
		Collection<OWLNamedClass> rootConcepts = AOSVocabulary.c_domain_concept.getSubclasses(false);
		return rootConcepts;
	}

	protected abstract ARTURIResource convertResourceURI(RDFResource owlInstance);

	protected ARTURIResource getSKOSXLLabelFromOWLCNOUN(RDFResource owlInstance) {
		// String conceptLocalName = owlInstance.getLocalName().replace("i_", "xl_");
		// this one below replaces the one above as we may be sure that the string to be modified starts with
		// the "i_" element, while running a replace would run into the risk of having more than one match, as
		// in the hindi case, where i_hi_... is transformed into xl_hxl_...
		String xLabelLocalName = "xl_" + owlInstance.getLocalName().substring(2);
		return rdfModel.createURIResource(rdfModel.getDefaultNamespace() + xLabelLocalName);
	}

	protected ARTURIResource convertDefinitionName(RDFResource owlInstance) {
		String conceptLocalName = owlInstance.getLocalName().replace("i_", "c_");
		return rdfModel.createURIResource(rdfModel.getDefaultNamespace() + conceptLocalName);
	}

}

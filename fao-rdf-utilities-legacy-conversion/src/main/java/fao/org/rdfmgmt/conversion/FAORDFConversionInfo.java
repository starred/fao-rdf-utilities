package fao.org.rdfmgmt.conversion;

import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.RDFModel;

import java.util.Collection;
import java.util.HashMap;

import edu.stanford.smi.protegex.owl.model.OWLModel;
import edu.stanford.smi.protegex.owl.model.RDFProperty;
import fao.org.rdfmgmt.vocabularies.AGRONTOLOGY;
import fao.org.rdfmgmt.vocabularies.AOS;

/**
 * abstract class preparing the structures for the properties which need to be mapped in the various Protege
 * --> standard RDF conversions
 * <p>
 * Note that:
 * <ul>
 * <li>mappedProperties is a special structure containing properties which need to be mapped to different
 * properties in the destination vocabulary. Properties in mappedProperties are automatically left out from
 * standard conversion.</li>
 * <li>
 * Some properties in {@link AOSConversionResourceLists} should be removed if they need to be translated in an
 * exceptional way. For these properties, use the {@link Collection#remove(Object)} method to remove them from
 * the proper conversion list in the {@link #initialize()} implementation of this class, and then handle them
 * properly in the
 * {@link FAOProtDB2RDFConverter#convertExceptionProperties(edu.stanford.smi.protegex.owl.model.OWLIndividual, ARTURIResource)}
 * method</li>
 * </ul>
 * </p>
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 */
public abstract class FAORDFConversionInfo {

	// this is an hashmap telling how to convert given properties
	public HashMap<RDFProperty, ARTURIResource> mappedProperties;

	OWLModel owlModel;
	RDFModel rdfModel;

	public FAORDFConversionInfo(OWLModel owlModel, RDFModel rdfModel) {
		mappedProperties = new HashMap<RDFProperty, ARTURIResource>();
		this.owlModel = owlModel;
		this.rdfModel = rdfModel;
		initialize();
	}

	protected abstract void initialize();

	/**
	 * finds a specific replacement for the property from the list of mapped properties, otherwise, just
	 * reuses the same property it found. If AOS namespace is being used, it is replaced with the AGRONTOLOGY
	 * one
	 * 
	 * @param sourceProp
	 * @return
	 */
	ARTURIResource getMappedProperty(RDFProperty sourceProp) {
		ARTURIResource mappedProp = mappedProperties.get(sourceProp);
		if (mappedProp != null)
			return mappedProp;
		else
			return rdfModel.createURIResource(sourceProp.getURI().replace(AOS.defNamespace,
					AGRONTOLOGY.NAMESPACE));
	}

}

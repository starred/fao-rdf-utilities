package fao.org.rdfmgmt.conversion;

import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.VocabUtilities;
import edu.stanford.smi.protegex.owl.model.OWLModel;
import fao.org.rdfmgmt.vocabularies.DCMIMetadataTerms;

public class SKOSXLGenericConversionInfo extends FAORDFConversionInfo {

	public SKOSXLGenericConversionInfo(OWLModel owlModel, RDFModel rdfModel) {
		super(owlModel, rdfModel);
	}

	@Override
	protected void initialize() {
		// no need to initialize any special mapping for generic SKOSXL conversion
		// if in need, put things like:
		mappedProperties.put(AOSVocabulary.hasDateCreated, DCMIMetadataTerms.Res.CREATED);
		mappedProperties.put(AOSVocabulary.hasDateLastUpdated, DCMIMetadataTerms.Res.MODIFIED);
		mappedProperties.put(AOSVocabulary.hasScopeNote, SKOS.Res.SCOPENOTE);
		mappedProperties.put(AOSVocabulary.hasEditorialNote, SKOS.Res.EDITORIALNOTE);
		
		mappedProperties.put(AOSVocabulary.hasStatus, VocabUtilities.nodeFactory.createURIResource("http://art.uniroma2.it/ontologies/vocbench#hasStatus"));
		
		mappedProperties.put(AOSVocabulary.takenFromSource, VocabUtilities.nodeFactory.createURIResource("http://art.uniroma2.it/ontologies/vocbench#hasSource"));
		mappedProperties.put(AOSVocabulary.hasSourceLink, VocabUtilities.nodeFactory.createURIResource("http://art.uniroma2.it/ontologies/vocbench#hasLink"));		
		mappedProperties.put(AOSVocabulary.hasImageSource, VocabUtilities.nodeFactory.createURIResource("http://art.uniroma2.it/ontologies/vocbench#hasSource"));
		mappedProperties.put(AOSVocabulary.hasImageLink, VocabUtilities.nodeFactory.createURIResource("http://art.uniroma2.it/ontologies/vocbench#hasLink"));
		
		
		// in case you want to remove a property
		// AOSConversionResourceLists.conceptToTypeDates.remove(AOSVocabulary.hasDateCreated);
		AOSConversionResourceLists.lexicalizationToLexicalization.remove(AOSVocabulary.hasTranslation);
		AOSConversionResourceLists.lexicalizationToTypeStrings.remove(AOSVocabulary.hasCodeAgrovoc);
		
	}

	
}

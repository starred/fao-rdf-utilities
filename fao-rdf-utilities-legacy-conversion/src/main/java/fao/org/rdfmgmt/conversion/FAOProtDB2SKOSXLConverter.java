package fao.org.rdfmgmt.conversion;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.SKOS;

import java.util.Collection;

import edu.stanford.smi.protege.model.Project;
import edu.stanford.smi.protegex.owl.model.OWLIndividual;
import edu.stanford.smi.protegex.owl.model.OWLNamedClass;
import edu.stanford.smi.protegex.owl.model.RDFSLiteral;
import fao.org.rdfmgmt.vocabularies.FOAF;

/**
 * this class extends the {@link FAOProtDB2RDFConverter} class to specifically map the definitions and
 * lexicalizations of the FAO legacy OWL Model into standard SKOSXL
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public abstract class FAOProtDB2SKOSXLConverter extends FAOProtDB2RDFConverter {

	protected boolean skosLabelsMaterialization;
	protected SKOSXLModel skosxlModel;

	/**
	 * @param protegeProject
	 * @param skosXLModel
	 * @param discardRedundants
	 *            if <code>true</code> the conversion discards redundant triples (e.g. the inverse triples,
	 *            which in the original Protege are always explicit bothways
	 * @param skosLabelsMaterialization
	 *            this parameter is separated from the <code>discardRedundants</code> parameter passed to the
	 *            conversion
	 * @param forceTypedConversions
	 *            if <code>true</code> datatype values which are non conformant to their standard formats are
	 *            automatically converted (when a correction is available). If <code>false</code>, an
	 *            exception is thrown instead, and the conversion is stopped
	 * @param getOnlyPublished
	 *            if <code>true</code>, only the concepts which have a "published" status, are to be converted
	 */
	public FAOProtDB2SKOSXLConverter(Project protegeProject, SKOSXLModel skosXLModel,
			boolean discardRedundants, boolean skosLabelsMaterialization, boolean forceTypedConversions,
			boolean getOnlyPublished) {
		super(protegeProject, skosXLModel, discardRedundants, forceTypedConversions, getOnlyPublished);
		this.skosxlModel = skosXLModel;
		this.skosLabelsMaterialization = skosLabelsMaterialization;
	}

	protected void convertHasRelatedConcept(OWLIndividual ind, ARTURIResource convertedConcept)
			throws ModelUpdateException {
		convertObjectProperty(ind, convertedConcept, AOSVocabulary.hasRelatedConcept, SKOS.Res.RELATED,
				ResType.instance);
	}

	protected void convertDefinitions(OWLIndividual ind, ARTURIResource skosConcept)
			throws ModelUpdateException, ConversionException {
		@SuppressWarnings("unchecked")
		Collection<OWLIndividual> hasDefinitions = ind.getPropertyValues(AOSVocabulary.hasDefinition);
		for (OWLIndividual definition : hasDefinitions) {
			ARTURIResource newDef = convertDefinitionName(definition);

			// adding the hasDefinition triple with modified name
			rdfModel.addTriple(skosConcept, SKOS.Res.DEFINITION, newDef);

			// LABELS
			@SuppressWarnings("unchecked")
			Collection<RDFSLiteral> labels = definition.getLabels();

			for (RDFSLiteral label : labels) {
				skosxlModel.addTriple(newDef, RDF.Res.VALUE,
						skosxlModel.createLiteral(label.getString(), label.getLanguage()));
			}

			// OTHER PROPERTIES OF DEFINITION
			convertDatatypeStringTypedProperty(definition, newDef, AOSVocabulary.takenFromSource);
			convertDatatypeStringTypedProperty(definition, newDef, AOSVocabulary.hasSourceLink);
			convertDatatypeDateTypedProperty(definition, newDef, AOSVocabulary.hasDateCreated);
			convertDatatypeDateTypedProperty(definition, newDef, AOSVocabulary.hasDateLastUpdated);
		}
	}

	protected void convertImages(OWLIndividual ind, ARTURIResource skosConcept) throws ModelUpdateException,
			ConversionException {
		@SuppressWarnings("unchecked")
		Collection<OWLIndividual> hasImages = ind.getPropertyValues(AOSVocabulary.hasImage);
		for (OWLIndividual image : hasImages) {
			ARTURIResource newImage = convertDefinitionName(image);

			// adding the hasDefinition triple with modified name
			rdfModel.addTriple(skosConcept, FOAF.Res.DEPICTION, newImage);
			rdfModel.addTriple(newImage, RDF.Res.TYPE, FOAF.Res.IMAGE);

			// LABELS
			@SuppressWarnings("unchecked")
			Collection<RDFSLiteral> labels = image.getLabels();

			for (RDFSLiteral label : labels) {
				skosxlModel.addTriple(newImage, RDF.Res.VALUE,
						skosxlModel.createLiteral(label.getString(), label.getLanguage()));
			}
			// COMMENTS

			// TODO modified to add multiple comments in different languages - pms if (comments.size() !=1)
			// throw new IllegalStateException(
			// "there should be only one comment per c_definition!, while here we have: " + comments);
			@SuppressWarnings("unchecked")
			Collection<RDFSLiteral> comments = image.getComments();
			for (RDFSLiteral comment : comments) {
				skosxlModel.addComment(newImage, comment.getString(), comment.getLanguage());
			}

			// OTHER PROPERTIES OF DEFINITION
			convertDatatypeStringTypedProperty(image, newImage, AOSVocabulary.hasImageSource);
			convertDatatypeStringTypedProperty(image, newImage, AOSVocabulary.hasImageLink);
			convertDatatypeDateTypedProperty(image, newImage, AOSVocabulary.hasDateCreated);
			convertDatatypeDateTypedProperty(image, newImage, AOSVocabulary.hasDateLastUpdated);
		}
	}

	protected ARTURIResource convertLexicalization(OWLIndividual lexicalization,
			ARTURIResource convertedConcept) throws ModelUpdateException, ModelAccessException {
		ARTURIResource skosLex = getSKOSXLLabelFromOWLCNOUN(lexicalization);

		// LABELS
		@SuppressWarnings("unchecked")
		Collection<RDFSLiteral> labels = lexicalization.getLabels();
		if (labels.size() != 1)
			throw new IllegalStateException(
					"there should be exactly one label per Lexicalization!, while here we have: " + labels);

		RDFSLiteral label = labels.iterator().next();
		skosxlModel.addXLabel(skosLex.getURI(), label.getString(), label.getLanguage());

		// DETERMINING IF IT IS A PREFLABEL OR ALTLABEL

		@SuppressWarnings("unchecked")
		Collection<Boolean> values = lexicalization.getPropertyValues(AOSVocabulary.isMainLabel);
		if (values.size() > 1) {
			throw new IllegalStateException(
					"there should be only one isMainLabel value per Lexicalization!, while here we have: "
							+ values);
		} else {
			boolean main;
			if (values.size() == 0)
				main = false;
			else {
				Boolean value = values.iterator().next();
				main = value.booleanValue();
			}

			if (main) {
				skosxlModel.setPrefXLabel(convertedConcept, skosLex, false);
				if (skosLabelsMaterialization)
					// skosxlModel.setPrefLabel(convertedConcept, label.getString(), label.getLanguage());
					// better to avoid SKOSModel prefLabel model verification, which would drop conversion
					// performance. Model is surely consistent
					skosxlModel.addTriple(convertedConcept, SKOS.Res.PREFLABEL,
							skosxlModel.createLiteral(label.getString(), label.getLanguage()));
			} else {
				skosxlModel.addAltXLabel(convertedConcept, skosLex);
				if (skosLabelsMaterialization)
					skosxlModel.addAltLabel(convertedConcept, label.getString(), label.getLanguage());
			}
		}

		return skosLex;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fao.org.owl2skos.conversion.FAOProtDB2RDFConverter#addConcept(edu.stanford.smi.protegex.owl.model.
	 * OWLNamedClass, it.uniroma2.art.owlart.model.ARTURIResource)
	 * 
	 * {@link SKOSXLModel} knows how to handle the NodeFilters.NONE case (see contract of this method)
	 */
	protected void addConcept(OWLNamedClass cls, ARTURIResource superConcept) throws ModelUpdateException,
			ModelAccessException {
		// does not use convertResourceURI() and a simple getURI() because OWLNamedClasses already have the
		// right URI; it's instances being converted
		skosxlModel.addConcept(cls.getURI(), superConcept);

		if (!discardRedundants && !superConcept.equals(NodeFilters.NONE)) {
			skosxlModel.addNarrowerConcept(superConcept, skosxlModel.createURIResource(cls.getURI()));
		}
	}

	protected void addBroaderRelationship(OWLNamedClass cls, ARTURIResource superConcept)
			throws ModelUpdateException, ModelAccessException {
		// does not use convertResourceURI() and a simple getURI() because OWLNamedClasses already have the
		// right URI; it's instances being converted
		ARTURIResource owlartConcept = skosxlModel.createURIResource(cls.getURI());
		skosxlModel.addTriple(owlartConcept, SKOS.Res.BROADER, superConcept);

		if (!discardRedundants && !superConcept.equals(NodeFilters.NONE)) {
			skosxlModel.addNarrowerConcept(superConcept, owlartConcept);
		}
	}

	@Override
	protected FAORDFConversionInfo getConversionInfo() {
		return new SKOSXLGenericConversionInfo(protOWLModel, skosxlModel);
	}

}

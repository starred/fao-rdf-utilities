package fao.org.rdfmgmt.conversion.jad;

import it.uniroma2.art.owlart.models.RDFModel;
import edu.stanford.smi.protegex.owl.model.OWLModel;
import fao.org.rdfmgmt.conversion.AOSConversionResourceLists;
import fao.org.rdfmgmt.conversion.AOSVocabulary;
import fao.org.rdfmgmt.conversion.FAORDFConversionInfo;
import fao.org.rdfmgmt.vocabularies.AGMES;
import fao.org.rdfmgmt.vocabularies.DCMIMetadataTerms;

/**
 * see comments on {@link FAORDFConversionInfo} for info about <code>mappedProperties</code> and how to handle
 * exceptional conversions
 * 
 * @author Armando Stellato <a href="mailto:stellato@info.uniroma2.it">stellato@info.uniroma2.it</a>
 * 
 */
public class JADConversionInfo extends FAORDFConversionInfo {

	public JADConversionInfo(OWLModel owlModel, RDFModel rdfModel) {
		super(owlModel, rdfModel);
	}

	@Override
	public void initialize() {

		// PUBLISHER is handled through the double conversion taking publishers' data from the vocbench
		// so, when launched for extracting journals/series, it just generates the URI, then a second run
		// extracts the publisher's info
		mappedProperties.put(AOSVocabulary.isPublishedBy, DCMIMetadataTerms.Res.PUBLISHER);
		
		// mappedProperties.put(AOSVocabulary.hasIssn, BIBO.Res.ISSN);
		// this one is managed through method: convertExceptionProperties, as its values need to be modified
		// mappedProperties.put(AOSVocabulary.hasEIssn, BIBO.Res.EISSN);
		mappedProperties.put(AOSVocabulary.hasCallNumber, AGMES.Res.AVAILABILITYNUMBER);

		// these properties are editorial notes of vocbench which can be taken away from the LOD publishing
		// version
		AOSConversionResourceLists.conceptToTypeDates.remove(AOSVocabulary.hasDateCreated);
		AOSConversionResourceLists.conceptToTypeDates.remove(AOSVocabulary.hasDateLastUpdated);
		AOSConversionResourceLists.conceptToTypeStrings.remove(AOSVocabulary.hasStatus);

		// the following properties are handled in an exceptional way in the JADProtDB2FOAFConverter
		AOSConversionResourceLists.conceptToTypeStrings.remove(AOSVocabulary.hasIssn);
		AOSConversionResourceLists.conceptToPlainLiterals.remove(AOSVocabulary.hasScopeNote);
	}

}

package fao.org.rdfmgmt.conversion.jad;

import java.io.File;
import java.io.IOException;

import fao.org.rdfmgmt.owlart.ModelLoader;
import fao.org.rdfmgmt.vocabularies.AUTHCON_CORPORATEBODIES;
import fao.org.rdfmgmt.vocabularies.BIBO;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.vocabulary.OWL;

/**
 * Simple utility class to finalize the output of a JAD conversion according to the last agreements about JAD
 * conversion, that is:
 * <ul>
 * <li>No URI for publishers, just blank nodes</li>
 * <li>sameAs statements with respect to old vocbench URIs</li>
 * <li>flattening bibo:Series to bibo:Journals, which is wrong but it makes coherence with the existing
 * entropy in the rest of AGRIS data</li>
 * </ul>
 * 
 * @author Armando Stellato <a href="mailto:stellato@info.uniroma2.it">stellato@info.uniroma2.it</a>
 * 
 */
public class JADBIBOFOAFFinalizer {

	public static void main(String[] args) throws IOException, ModelCreationException,
			BadConfigurationException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException {
		// OWL ART SKOSXLMODEL LOADING
		OWLModel owlModel = ModelLoader.loadModel(args[0], OWLModel.class);
		File inputFile = new File(args[1]);
		owlModel.addRDF(inputFile, null, RDFFormat.guessRDFFormatFromFile(inputFile));

		// replacing publishers URIs with blank nodes
		ARTStatementIterator statit = owlModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
				NodeFilters.ANY, false, NodeFilters.MAINGRAPH);
		while (statit.streamOpen()) {
			ARTStatement stat = statit.getNext();
						
			ARTURIResource subj = stat.getSubject().asURIResource();						
			if (subj.getLocalName().startsWith("cb")) {
				owlModel.deleteStatement(stat);
				owlModel.addTriple(owlModel.createBNode(subj.getLocalName()), stat.getPredicate(), stat.getObject());
			}
			
			ARTNode obj = stat.getObject();
			if (obj.isURIResource() && obj.asURIResource().getLocalName().startsWith("cb")) {
				owlModel.deleteStatement(stat);
				String localName = obj.asURIResource().getLocalName();
				owlModel.addTriple(stat.getSubject(), stat.getPredicate(), owlModel.createBNode(localName));
				//adding sameAs between blank node and final || IN OGNI CASO NON E' QUI, MA SULLE PUBLICATONS!  
				//owlModel.addTriple(owlModel.createBNode(localName), OWL.Res.SAMEAS, owlModel.createURIResource(AUTHCON_CORPORATEBODIES.defNamespace + localName.replace("cb_", "c_")));
			}
				
		}
		statit.close();

		// removing Serials
		statit = owlModel.listStatements(NodeFilters.ANY, NodeFilters.ANY, BIBO.Res.SERIES, false, NodeFilters.MAINGRAPH);
		while (statit.streamOpen()) {
			ARTStatement stat = statit.getNext();
			owlModel.deleteStatement(stat);
			owlModel.addTriple(stat.getSubject(), stat.getPredicate(), BIBO.Res.JOURNAL);
		}
		statit.close();

		
		String outputFileName = "finalized_jad";
		owlModel.writeRDF(new File(outputFileName+".nt"), RDFFormat.NTRIPLES, NodeFilters.MAINGRAPH);
		owlModel.writeRDF(new File(outputFileName+".rdf"), RDFFormat.RDFXML_ABBREV, NodeFilters.MAINGRAPH);		
		
	}
}

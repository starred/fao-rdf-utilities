package fao.org.rdfmgmt.protege;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.stanford.smi.protege.model.Project;
import edu.stanford.smi.protegex.owl.model.OWLProperty;
import edu.stanford.smi.protegex.owl.model.RDFSNames;
import fao.org.rdfmgmt.conversion.AOSVocabulary;

/**
 * Used to inspect the model content of an ontology (classes, properties)
 * The implementation is still empty, but I leave it here in case we need it in the future.
 * We can even use it as a template for other files needing to load data from ProtegeDB
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class ProtegeDBOntologyInspector {

	public static OWLProperty rdfsComment;
	public static OWLProperty rdfsLabel;

	public static enum ResType {
		label, instance
	};

	protected static Logger logger = LoggerFactory.getLogger(ProtegeDBOntologyInspector.class);

	Project protegeProject;
	edu.stanford.smi.protegex.owl.model.OWLModel protOWLModel;

	public ProtegeDBOntologyInspector(Project protegeProject) {
		this.protegeProject = protegeProject;
		protOWLModel = (edu.stanford.smi.protegex.owl.model.OWLModel) protegeProject.getKnowledgeBase();

		AOSVocabulary.initialize(protOWLModel);

		logger.info("cdomainConcept: " + AOSVocabulary.c_domain_concept);

		rdfsLabel = protOWLModel.getOWLProperty(RDFSNames.Slot.LABEL);
		rdfsComment = protOWLModel.getOWLProperty(RDFSNames.Slot.COMMENT);

		// logger.info("hasLexicalization: " + hasLexicalization.getBrowserText());
	}

	/**
	 * 
	 * @param args
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ModelCreationException
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 * @throws UnsupportedRDFFormatException
	 * @throws BadConfigurationException
	 * @throws UnsupportedModelConfigurationException
	 * @throws UnloadableModelConfigurationException
	 */
	public static void main(String[] args) throws IOException, ClassNotFoundException,
			InstantiationException, IllegalAccessException, ModelCreationException, ModelUpdateException,
			ModelAccessException, UnsupportedRDFFormatException, BadConfigurationException,
			UnsupportedModelConfigurationException, UnloadableModelConfigurationException {
		if (args.length < 2) {
			System.out.println("usage:\n" + ProtegeDBOntologyInspector.class.getName()
					+ " <ProtegeDBConfigFile> config");
			return;
		}

		// PROTEGE PROJECT LOADING
		ProtegeModelLoader loader = new ProtegeModelLoader();
		Project protProject = loader.loadProtegeProject(args[0]);
		ProtegeDBOntologyInspector inspector = new ProtegeDBOntologyInspector(protProject);
	}

}

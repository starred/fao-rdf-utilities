package fao.org.allegro.test;

import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;

import com.franz.agraph.repository.AGCatalog;
import com.franz.agraph.repository.AGRepository;
import com.franz.agraph.repository.AGRepositoryConnection;
import com.franz.agraph.repository.AGServer;

public class AllegroAccessViaSesame2Testing {
	static private final String SERVER_URL = "http://202.73.13.50:55820";
    static private final String CATALOG_ID = "Root";
    static private final String REPOSITORY_ID = "agrovoc";
    static private final String USERNAME = "fao";
    static private final String PASSWORD = "faop@55";
    public static AGRepositoryConnection example1(boolean close) throws Exception {
    	 System.out.println("\nStarting example1().");
         AGServer server = new AGServer(SERVER_URL, USERNAME, PASSWORD);
         System.out.println("Available catalogs: " + server.listCatalogs());
         AGCatalog catalog = server.getRootCatalog();
         System.out.println("Available repositories in catalog " + 
                 (catalog.getCatalogName()) + ": " + 
                 catalog.listRepositories());
         AGRepository myRepository = catalog.createRepository(REPOSITORY_ID);
         System.out.println("Got a repository.");
         myRepository.initialize();
         System.out.println("Initialized repository.");
         System.out.println("Repository is writable? " + myRepository.isWritable());
         AGRepositoryConnection conn = myRepository.getConnection();
         conn.setAutoCommit(true);
         return conn;
    }
    
    public static void main(String[] args) throws Exception {
    	AGRepositoryConnection conn = AllegroAccessViaSesame2Testing.example1(false);
    	String queryString = "SELECT  ?p ?o  WHERE {ns:c_15903 ?p ?o .}";
    	conn.setNamespace("ns", "http://aims.fao.org/aos/agrovoc/");
    	TupleQuery tupleQuery = conn.prepareTupleQuery(QueryLanguage.SPARQL, queryString);
        TupleQueryResult result = tupleQuery.evaluate();
        
            while (result.hasNext()) {
                BindingSet bindingSet = result.next();
               
                Value p = bindingSet.getValue("p");
                Value o = bindingSet.getValue("o");
                System.out.format("%s %s\n", p, o);
            }
            result.close();
            conn.close();
        
	}
}


